


# Basic structure of LIDO

## LIDO document and LIDO record

The basic structure of LIDO is determined by the XML representation. Any data represented in XML requires a single outer element, enclosing all further data. With regard to the tree-like structure of XML, this outermost element is referred to as the *root* element. The root element is where all machine processing of XML data starts and ends. Either the LIDO wrapper <[lido:lidoWrap](https://lido-schema.org/schema/latest/lido.html#lidoWrap)> or the LIDO element <[lido:lido](https://lido-schema.org/schema/latest/lido.html#lido)> can be used as the root element for an entire <abbr title="In LIDO an XML instance provided as a whole unit">LIDO document</abbr>. A document with the root &lt;lido:lido&gt; can contain only one <abbr title="In LIDO an XML instance for the description of a single record">LIDO record</abbr>, while a document with the root &lt;lido:lidoWrap&gt; can contain one or more &lt;lido:lido&gt; elements i.e. one ore more <abbr title="In LIDO an XML instance for the description of a single record">LIDO records</abbr>. Both forms are valid with respect to the LIDO XML schema.

![Figure 3: Two views of the root-and-branch metaphor used in describing nested structures in LIDO](img/figure_root-branches.png)

## Top level elements

In the <[lido:lido](https://lido-schema.org/schema/latest/lido.html#lido)> element six elements are nested at the top. Following is an outline of these elements with short descriptions.

The &lt;lido:lido&gt; element fully encloses a metadata record. The following list contains the six elements that can occur at the upper level of a LIDO record. Three of these are mandatory elements:

- [LIDO Metadata Record Identifier](https://lido-schema.org/schema/latest/lido.html#lidoRecID) [required]{.important}
- [Published Object Identifier](https://lido-schema.org/schema/latest/lido.html#objectPublishedID)
- [Category](https://lido-schema.org/schema/latest/lido.html#category)
- [Application Profile](https://lido-schema.org/schema/latest/lido.html#applicationProfile) [new&nbsp;in&nbsp;LIDO&nbsp;v1.1]{.green}
- [Descriptive Metadata](https://lido-schema.org/schema/latest/lido.html#descriptiveMetadata) [required]{.important}
- [Administrative Metadata](https://lido-schema.org/schema/latest/lido.html#administrativeMetadata) [required]{.important}

The LIDO top level elements in XML syntax:

```xml
<lido:lido>
    <lido:lidoRecID lido:type="{...}">[...]</lido:lidoRecID>
    <lido:objectPublishedID>[...]</lido:objectPublishedID>
    <lido:category>[...]</lido:category>
    <lido:applicationProfile lido:type="{...}">[...]</lido:applicationProfile>
    <lido:descriptiveMetadata xml:lang="{...}">[...]</lido:descriptiveMetadata>
    <lido:administrativeMetadata xml:lang="{...}">[...]</lido:administrativeMetadata>
</lido:lido>
```

### LIDO Metadata Record Identifier

**Element name:** <[lido:lidoRecID](https://lido-schema.org/schema/latest/lido.html#lidoRecID)>

**Note:** This mandatory element serves to distinguish an individual LIDO record from any other record that may occur in a database, data repository, or any other aggregation of machine-processable records. The LIDO Metadata Record Identifier is preferably composed of an identifier for the contributor and a record identification in the (local) system of the contributor. It is not required to be persistent, which means that it can be obsoleted when new versions of a LIDO record become available. Any subsequent version of a LIDO record should be distinguishable at least by using the <[lido:recordMetadataDate](https://lido-schema.org/schema/latest/lido.html#recordMetadataDate)> element. For identifiers guaranteed to be persistent see the <[lido:objectPublishedID](https://lido-schema.org/schema/latest/lido.html#objectPublishedID)> described below. However, some aggregators will expect this identifier to be persistent throughout all updates.

**Example for "URI":** In the following example the LIDO Metadata Record Identifier is composed of the ISIL (International Standard Identifier for Libraries and Related Organisations) of the contributor and the local record ID. The mandatory *type* attribute has a value from the LIDO Terminology for "URI", and the source is provided as ISIL of the contributor.

> **LIDO Metadata Record Identifier:** ld.zdb-services.de/resource/organisations/DE-Mb112/lido-obj00154983

***Type:*** URI | ***Source:*** ld.zdb-services.de/resource/organisations/DE-Mb112

```xml
<lido:lidoRecID
    lido:type="http://terminology.lido-schema.org/lido00099"
    lido:source="ld.zdb-services.de/resource/organisations/DE-Mb112">
    ld.zdb-services.de/resource/organisations/DE/DE-Mb112/lido-obj00154983
</lido:lidoRecID>
```

**Example for "Local identifier":** In the following example the LIDO Metadata Record Identifier is composed of the ISIL of the contributor (data provider) and the local record ID. The mandatory *lido:type* attribute has a value from the LIDO Terminology for "Local identifier", and the source is provided as free text.

> **LIDO Metadata Record Identifier:** DE-Mb112/lido-obj00154983 ***Type:*** Local identifier | ***Source:*** Deutsches Dokumentationszentrum für Kunstgeschichte - Bildarchiv Foto Marburg

```xml
<lido:lidoRecID
    lido:type="http://terminology.lido-schema.org/lido00100"
    lido:source="Deutsches Dokumentationszentrum für Kunstgeschichte - Bildarchiv Foto Marburg">
    DE-Mb112/lido-obj00154983
</lido:lidoRecID>
```

### Published Object Identifier

**Element name:** <[lido:objectPublishedID](https://lido-schema.org/schema/latest/lido.html#objectPublishedID)>

**Note:** This element contains a public entry for the described object or work in another information system or authority file. This identifier should always be a dereferenceable URI so that it can be used as a key to associated information. Repeatable for identifiers from more than one source. If there are multiple published identifiers, then the attribute [lido:pref](http://lido-schema.org/schema/latest/lido.html#pref) should mark the one preferred by the provider of the LIDO record.

**Example:** In the following example the Published Object Identifier is marked as the preferred one by the data provider. The mandatory *lido:type* attribute has a value from the LIDO Terminology for "URI", and the *source* is provided as free-text.

> **Published Object Identifier:** <https://d-nb.info/gnd/4074156-4>
***Preference:*** Preferred | ***Type:*** URI | ***Source:*** Deutsche Nationalbibliothek: Gemeinsame Normdatei. URL: <https://d-nb.info/gnd> [2022-09-23]

```xml
<lido:objectPublishedID
    lido:pref="http://terminology.lido-schema.org/lido00169"
    lido:type="http://terminology.lido-schema.org/lido00099"
    lido:source="Deutsche Nationalbibliothek: Gemeinsame Normdatei. URL: http://d-nb.info/gnd [2022-09-23]">
    http://d-nb.info/gnd/4074156-4
</lido:objectPublishedID>
```

### Category

**Element name:** <[lido:category](https://lido-schema.org/schema/latest/lido.html#category)>

**Note:** Category is a content element for grouping LIDO records together by very broad classes of objects or works. Initially intended to be populated with high-level classes of the CIDOC CRM, particularly sub-classes of [E18 Physical Thing](https://cidoc-crm.org/html/cidoc_crm_v7.1.1_with_translations.html#E18), there is now a [Category Vocabulary](http://terminology.lido-schema.org/lido00092 ) available from the LIDO Terminology for use in this element. Note that a &lt;lido:category&gt; statement must be logically and semantically consistent with what is said in the more specific &lt;lido:objectWorkType&gt; and &lt;lido:classification&gt; elements.

**Example:** In the following example, the category statement is fully expressed by the LIDO Terminology identifier, ["lido00096"](http://terminology.lido-schema.org/lido00096). The term in &lt;skos:prefLabel&gt; is given here both for human readability and for use by processing systems that are not prepared for fetching data using a vocabulary URI.

> **Category:** Human-made object ***Preference:*** Preferred

```xml
<lido:category>
    <skos:Concept rdf:about="http://terminology.lido-schema.org/lido00096">
        <skos:prefLabel xml:lang="en">Human-made object</skos:prefLabel>
    </skos:Concept>
</lido:category>
```

### Application Profile

**Element name:** <[lido:applicationProfile](https://lido-schema.org/schema/latest/lido.html#applicationProfile)>

**Note:** The LIDO schema may not cover all metadata needs of the user community. To accommodate specialized user requirements, the LIDO schema can be complemented with application profiles. Whenever an application profile has been used in the preparation of a LIDO record, the <[lido:applicationProfile](https://lido-schema.org/schema/latest/lido.html#applicationProfile)> element should carry the public identifier (URI or equivalent) of the respective document(s). For further information on LIDO profiles, see section [*3.4.3 Application Profiles*](#application-profiles) and <https://cidoc.mini.icom.museum/working-groups/lido/lido-overview/profiles/>.

**Example:** In the following example the Application Profile Element contains the URI of the XML Schema Definition Document of the [LIDO Application Profile Painting and Sculpture](https://doi.org/10.11588/arthistoricum.1026).

> **Application Profile:** <https://lido-schema.org/profiles/v1.1/lido-v1.1-profile-paintingandsculpture-v1.0.xsd> ***Type:*** URI

```xml
<lido:applicationProfile
    lido:type="http://terminology.lido-schema.org/lido00099">
    https://lido-schema.org/profiles/v1.1/lido-v1.1-profile-paintingandsculpture-v1.0.xsd
</lido:applicationProfile>
```

### Descriptive Metadata

**Element name:** <[lido:descriptiveMetadata](https://lido-schema.org/schema/latest/lido.html#descriptiveMetadata)>

**Note:** Descriptive metadata are intended to facilitate the discovery and identification of objects or works. They are most visible to end-users, guiding them through the physical or virtual shelves and enabling them to find what they need. Descriptive metadata comprise information on object properties, such as its type, title, physical features, or participation in an event. Note, that the use of &lt;lido:descriptiveMetadata&gt; provided with an xml:lang attribute is mandatory.

![Figure 4: Descriptive Metadata and its immediate sub-elements](img/figure_descriptiveMetadata.png)

### Administrative Metadata

**Element name:** <[lido:administrativeMetadata](https://lido-schema.org/schema/latest/lido.html#administrativeMetadata)>

**Note:** Administrative metadata are intended to facilitate the management of the data provided, particularly holding information about rights and restrictions on the access or use of the described object or work, the provided record or any supplied resource. Note, that the use of &lt;lido:administrativeMetadata&gt; provided with an xml:lang attribute is mandatory.

![Figure 5: Administrative Metadata and its immediate sub-elements](img/figure_administrativeMetadata.png)


## Element nesting

Element nesting means that elements contain other elements that contain still other elements, and so forth. A nested structure of so called parent elements and child elements is a basic feature of XML. As with CDWA Lite, LIDO consists of wrapper and set elements that only serve to structure the data, and data elements for the actual data content. In this Primer document, the wrapper and set elements are referred to as **structural elements**, whereas the elements actually holding data are called **content elements**. Since a complete LIDO record can have a dozen or more nesting levels, some distinctions between kinds of elements are made in the LIDO element descriptions.

The following figure shows the nesting of LIDO structural and content elements with a corresponding example for the description of materials and techniques, used in the production of an object. The [Object Materials/Techniques Wrapper](https://lido-schema.org/schema/latest/lido.html#objectMaterialsTechWrap) contains the [Object Materials/Techniques Set](https://lido-schema.org/schema/latest/lido.html#objectMaterialsTechSet), enclosing the [Display Materials/Techniques](https://lido-schema.org/schema/latest/lido.html#displayMaterialsTech) element and the set for [Materials/Techniques](https://lido-schema.org/schema/latest/lido.html#materialsTech), holding the index elements.

![Figure 6: Nesting of structural and content elements](img/figure_nesting.png)

**Example:** Following is an XML code snippet containing the elements depicted in figure 5.

```xml
 <lido:objectMaterialsTechWrap>
    <lido:objectMaterialsTechSet>
        <lido:displayMaterialsTech xml:lang="{...}">[...]</lido:displayMaterialsTech>
        <lido:materialsTech>
            <lido:termMaterialsTech lido:type="{...}">
                <skos:Concept rdf:about="{...}"></skos:Concept>
            </lido:termMaterialsTech>
        </lido:materialsTech>
    </lido:objectMaterialsTechSet>
</lido:objectMaterialsTechWrap>
```

### Structural elements

LIDO defines structural elements for grouping more specific elements together. A structural element is either a **wrapper** which is non-repeatable, or a repeatable **set**. Usually, wrappers are on a higher nesting level relative to set elements, the latter being typically enclosed by a wrapper. However, wrapper elements may also occur as sub-elements of wrappers as well as sets.

#### Wrapper elements

**Note:** In the [LIDO Schema Documentation](https://lido-schema.org/schema/latest/lido.html), an element is introduced as "A wrapper ..." if it structures the data in a LIDO record and if it is not repeatable.

**Features:** Wrappers are not repeatable, have no attributes, except for a language attribute that can be given to administrative and descriptive metadata. A wrapper itself does not hold any content data.

**Example:** <[lido:classificationWrap](https://lido-schema.org/schema/latest/lido.html#objectMaterialsTechWrap)> is a wrapper for the index element <[lido:classification](https://lido-schema.org/schema/latest/lido.html#classification)>, and is contained by the wrapper element <[lido:objectClassificationWrap](https://lido-schema.org/schema/latest/lido.html#objectClassificationWrap)>.

```xml
<lido:classificationWrap>
    <lido:classification lido:type="{...}">
        <skos:Concept rdf:about="{...}"></skos:Concept>
    </lido:classification>
</lido:classificationWrap>
```

#### Set elements

**Note:** In the [LIDO Schema documentation](https://lido-schema.org/schema/latest/lido.html), an element is introduced as "A set ..." if it structures the data in a LIDO record and if it is repeatable.

**Features:** In contrast to LIDO wrappers, sets are repeatable and can have attributes. Like wrappers, they do not hold any content data.

**Example:** <[lido:measurementsSet](https://lido-schema.org/schema/latest/lido.html#measurementsSet)> is a set element that encloses three index elements for object dimensions (&lt;lido:measurementType&gt;, &lt;lido:measurementUnit&gt;, &lt;lido:measurementValue&gt;), all of which are mandatory if &lt;lido:measurementsSet&gt; is used. The set is repeatable within its parent element so that it can accommodate data for different types of dimensions, such as height, width, weight, etc. The following XML code snippet shows the usage of measurement types "height" and "width".

```xml
<lido:objectMeasurementsSet>
    <lido:displayObjectMeasurements xml:lang="en">height x width [...]</lido:displayObjectMeasurements>
    <lido:objectMeasurements>
        <lido:measurementsSet>
            <lido:measurementType>
                <skos:Concept rdf:about="{...}">
                    <skos:prefLabel xml:lang="en">
                        height
                    </skos:prefLabel>
                </skos:Concept>
            </lido:measurementType>
            <lido:measurementUnit>
                <skos:Concept rdf:about="{...}"></skos:Concept>
            </lido:measurementUnit>
            <lido:measurementValue>[...]</lido:measurementValue>
        </lido:measurementsSet>
        <lido:measurementsSet>
            <lido:measurementType>
                <skos:Concept rdf:about="{...}">
                    <skos:prefLabel xml:lang="en">
                        width
                    </skos:prefLabel>
                </skos:Concept>
            </lido:measurementType>
            <lido:measurementUnit>
                <skos:Concept rdf:about="{...}"></skos:Concept>
            </lido:measurementUnit>
            <lido:measurementValue>[...]</lido:measurementValue>
        </lido:measurementsSet>
    </lido:objectMeasurements>
</lido:objectMeasurementsSet>
```

### Content elements

The LIDO element descriptions distinguish between the following kinds of content elements:

1. Text elements for free-text notes
2. Display elements, a kind of text elements
3. Index elements holding indexing terms
4. Identifier elements for all kinds of entities

#### Text elements

**Note:** In the [LIDO Schema Documentation](https://lido-schema.org/schema/latest/lido.html), an element is introduced as "A text element ..." if it is intended to present human-readable notes or explanations, populated with free-text data values. Most of the text elements are used for display and source information, or for more detailed textual descriptions of the object or work. Note that exactly one &lt;lido:appellationValue&gt; per language should be marked as the preferred one.

**Features:** Defined by [*textComplexType*](https://lido-schema.org/schema/latest/lido.html#textComplexType), a text element can have attributes, is repeatable (under conditions), and has free text (xs:string) as its data value.

**Example:** <[lido:appellationValue](https://lido-schema.org/schema/latest/lido.html#appellationValue)> is a text element contained by several elements for naming entities. This element is required if an element for naming entities is used, e.g., <[lido:nameActorSet](https://lido-schema.org/schema/latest/lido.html#nameActorSet)>.

> **Creator:** Leonardo da Vinci ***Preference:*** Preferred

```xml
<lido:nameActorSet>
    <lido:appellationValue
        lido:pref="http://terminology.lido-schema.org/lido00169"
        xml:lang="en">
        Leonardo da Vinci
    </lido:appellationValue>
</lido:nameActorSet>
```

#### Display elements

**Note:** In the [LIDO Schema Documentation](https://lido-schema.org/schema/latest/lido.html), an element is introduced as "A display element ..." if it contains a human-readable rendering of what is stated in the associated index element. For an account of LIDO display elements and more detailed information see section [*4.3.3 Display and index elements*](#display-and-index-elements).

**Features:** Defined by [*textComplexType*](https://lido-schema.org/schema/latest/lido.html#textComplexType), a display element can have attributes, is repeatable only for different languages, and has free text (xs:string) as its data value.

**Example:** <[lido:displayMaterialsTech](https://lido-schema.org/schema/latest/lido.html#displayMaterialsTech)> is a textual description of the associated index terms in <[lido:termMaterialsTech](https://lido-schema.org/schema/latest/lido.html#termMaterialsTech)> contained, e.g., by the <[lido:objectMaterialsTechSet](https://lido-schema.org/schema/latest/lido.html#objectMaterialsTechSet)> element.

> **Materials/Techniques:** Oil on poplar panel

```xml
<lido:objectMaterialsTechSet>
    <lido:displayMaterialsTech xml:lang="en">
        Oil on poplar panel
    </lido:displayMaterialsTech>
</lido:objectMaterialsTechSet>
```

#### Index elements

**Note:** In the [LIDO Schema Documentation](https://lido-schema.org/schema/latest/lido.html), an element is introduced as "An index element ..." if it refers to general concepts for indexing and retrieval. Index elements provide sub-elements for information on the indexing concept, including its URI or local identifier, and the natural language term or terms. For more information on display and index elements see section [*4.4.3 Display and index elements*](#display-and-index-elements) below.

**Features:** Defined by [*conceptComplexType*](https://lido-schema.org/schema/latest/lido.html#conceptComplexType), an index element can have attributes, may be repeatable, and is populated with elements containing controlled data values.

**Example:** <[lido:termMaterialsTech](https://lido-schema.org/schema/latest/lido.html#termMaterialsTech)> is an index element to provide descriptors for materials, e.g., medium or support, and techniques, from controlled vocabularies.

> **Materials/Techniques:** Oil paint ***Type:*** Medium ***Preference:*** Display

```xml
<!-- Medium -->
<lido:termMaterialsTech
    lido:type="http://terminology.lido-schema.org/lido00513">
    <skos:Concept
        rdf:about="http://vocab.getty.edu/aat/300015050">
        <skos:prefLabel xml:lang="en">
            oil paint (paint)
        </skos:prefLabel>
    </skos:Concept>
    <!-- Display label -->
    <lido:term
        lido:pref="http://terminology.lido-schema.org/lido00526"
        xml:lang="en">
        oil paint
    </lido:term>
</lido:termMaterialsTech>
```

#### Identifier elements

**Note:** In the [LIDO Schema Documentation](https://lido-schema.org/schema/latest/lido.html), an element is introduced as "An identifier ..." if it is intended to hold values for IRIs, URIs, or local identifiers. There are [thirteen identifier elements](https://lido-schema.org/schema/latest/lido.html#identifierComplexType) defined in LIDO, particularly for named entities, such as <[lido:actorID]()>, administrative data, such as <[lido:recordID](https://lido-schema.org/schema/latest/lido.html#recordID)>, or contextual information, such as <[lido:resourceID](https://lido-schema.org/schema/latest/lido.html#resourceID)>. Note that the [*lido:type*](https://lido-schema.org/schema/latest/lido.html#type) attribute is required for identifiers. Terms for the type attribute are provided in the LIDO [Identifier Type-Vocabulary](http://terminology.lido-schema.org/lido00098). It is strongly recommended to use an HTTP URI identifier from a LOD vocabulary for the type attribute.

**Features:** Defined by [*identifierComplexType*](https://lido-schema.org/schema/latest/lido.html#identifierComplexType), an identifier element can have attributes, is repeatable (under conditions), and is populated preferably with URIs. If local identifiers must be used, then use of the source attribute is necessary for making the identifier unambiguous.

**Example:** <[lido:actorID](https://lido-schema.org/schema/latest/lido.html#actorID)> holds an identifier for an actor.

> **Actor Identifier:** http://viaf.org/viaf/24604287 ***Type:*** URI

```xml
<lido:actor>
    <lido:actorID
        lido:type="http://terminology.lido-schema.org/lido00099">
        http://viaf.org/viaf/24604287
    </lido:actorID>
</lido:actor>
```

### Display and index elements

Following CDWA/CCO, LIDO defines elements to allow for recording distinct display information alongside the index entries intended for retrieval. A **display** element presents information in natural language, intended to be easily readable. An **index** element is designed to hold controlled values or descriptors from formal languages, such as thesauri or classification systems, assigned for information retrieval. Compared to index elements, the free-text display elements allow for a less rigid expression of information including subtleties, uncertainties, or fuzziness. In contrast, value-controlled index elements support consistent matching with user queries and reliable feedback for query refinement.

Portals can use the information given in display and/or index elements in various ways. If the display element is lacking an associated index element, then free text is the only source for search terms, forsaking the chance for more reliable search results. Where the display element is not used, portal designers will typically present the index terms in some order, perhaps linking them to a new or modified query. If the index terms are from a controlled vocabulary, then search portals may construct hyperlinks for semantically enhanced search and navigation, for example by offering a broader or narrower search focus, or by retrieving definitions and other additional information from the thesaurus or authority file (sometimes referred to as 'exploring the knowledge graph').

A total of **13 display elements** are available in LIDO Version 1.1. The list below shows all of these elements within their sets and examples of the paired use of display elements with their corresponding index element.

Of **33 index elements** in total, 12 are derived from [*conceptMixedComplexType*](https://lido-schema.org/schema/latest/lido.html#conceptMixedComplexType). Elements declared this way can either contain plain text, or sub-elements with structured information, but not both simultaneously. These elements offer a fallback solution for data providers that can only supply textual keywords for indexing.

> For further information see [Cataloging Cultural Objects](https://vraweb.org/wp-content/uploads/2020/04/CatalogingCulturalObjectsFullv2.pdf), chapter "Display and Indexing", pp. 24–25. Examples and cataloging rules are provided in the respective chapter describing a category, e.g., Measurements, pp. 109–120.

The following table lists the 13 display elements and shows values for the display element as well as the corresponding index element or elements, if applicable for the painting *Mona Lisa*.

**Table 2: List of display elements with examples for display and index data**

| Display element              | Example for display data                                            | Example for index data |
| ---------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| <[lido:displayActor](http://lido-schema.org/schema/latest/lido.html#displayActor)>             | Leonardo da Vinci   |  <[lido:actor](https://lido-schema.org/schema/v1.1/lido-v1.1.html#actor_actorInRoleComplexType)> [*Person*](http://terminology.lido-schema.org/lido00163): [**Leonardo da Vinci**](http://vocab.getty.edu/ulan/500010879)    |
| <[lido:displayActorInRole](https://lido-schema.org/schema/latest/lido.html#displayActorInRole)>        | Painter: Leonardo da Vinci | <[lido:roleActor](https://lido-schema.org/schema/latest/lido.html#roleActor)> [**painter**](http://vocab.getty.edu/aat/300025136)<br><[lido:actor](https://lido-schema.org/schema/v1.1/lido-v1.1.html#actor_actorInRoleComplexType)> [*Person*](http://terminology.lido-schema.org/lido00163): [**Leonardo da Vinci**](http://vocab.getty.edu/ulan/500010879)              |                  |
| <[lido:displayDate](https://lido-schema.org/schema/latest/lido.html#displayDate)>               |Between 1503/1519                              |  <[lido:earliestDate](https://lido-schema.org/schema/latest/lido.html#earliestDate)> *[Estimated date](http://terminology.lido-schema.org/lido00529)*: **1503**<br> <[lido:latestDate](https://lido-schema.org/schema/latest/lido.html#latestDate)> *[Estimated date](http://terminology.lido-schema.org/lido00529)*: **1519**                                         |
| <[lido:displayEdition](https://lido-schema.org/schema/latest/lido.html#displayEdition)>            | Not applicable|  Not applicable                                                            |
| <[lido:displayEvent](https://lido-schema.org/schema/latest/lido.html#displayEvent)>              | Production                                       | <[lido:eventType](https://lido-schema.org/schema/latest/lido.html#eventType)> [**Production**](http://vocab.getty.edu/aat/300069682)                                                            |
| <[lido:displayMaterialsTech](https://lido-schema.org/schema/latest/lido.html#displayMaterialsTech)>      | Oil on poplar panel                         | <[lido:termMaterialsTech](https://lido-schema.org/schema/latest/lido.html#termMaterialsTech)> [*Medium*](http://terminology.lido-schema.org/lido00513): [**oil paint**](http://vocab.getty.edu/aat/300015050)<br/> [*Support*](http://terminology.lido-schema.org/lido00514): [**poplar**](http://vocab.getty.edu/aat/300012363); [**panel**](http://vocab.getty.edu/aat/300014657);<br>[*Technique*](http://terminology.lido-schema.org/lido00131): [**panel painting**](http://vocab.getty.edu/aat/300178675); [**sfumato**](http://vocab.getty.edu/aat/300053421)                                         |
| <[lido:displayObject](https://lido-schema.org/schema/latest/lido.html#displayObject)>            | Mona Lisa | <[lido:subjectObject](https://lido-schema.org/schema/latest/lido.html#subjectObject)> <[lido:object](https://lido-schema.org/schema/latest/lido.html#object)> <[lido:objectID]()> [*URI*](http://terminology.lido-schema.org/lido00099): [**Mona Lisa**](http://www.wikidata.org/entity/Q12418)
| <[lido:displayObjectMeasurements](https://lido-schema.org/schema/latest/lido.html#displayObjectMeasurements)> | Height x width: 77 x 53 cm | <[lido:measurementType](https://lido-schema.org/schema/latest/lido.html#measurementType)> [**height**](http://www.wikidata.org/wiki/Q208826)<br/><[lido:measurementUnit](https://lido-schema.org/schema/latest/lido.html#measurementUnit)> [**metre**](http://www.wikidata.org/entity/Q11573)<br/><[lido:measurementValue](https://lido-schema.org/schema/latest/lido.html#measurementValue)>**0.79**                     |
| <[lido:displayPlace](https://lido-schema.org/schema/latest/lido.html#displayPlace)>              | Florence                                       |  [City](http://www.wikidata.org/entity/Q515): [Florence](http://www.wikidata.org/entity/Q2044)                                                            |
| <[lido:displayRelatedWork](https://lido-schema.org/schema/latest/lido.html#displayRelatedWork)>       |Leonardo da Vinci, Study for Mona Lisa, drawing      |<[lido:objectID]()> [*Local identifier*](http://terminology.lido-schema.org/lido00100): **08012138** <[lido:relatedWorkRelType](https://lido-schema.org/schema/latest/lido.html#relatedWorkRelType)> *[has preparatory study](http://terminology.lido-schema.org/lido00259)*: [**Study for Mona Lisa, drawing**](http://foto.biblhertz.it/obj08012138) |
| <[lido:displayRepository](https://lido-schema.org/schema/latest/lido.html#displayRepository)>        | Musée du Louvre, Département des Peintures | <[lido:repositoryName](http://lido-schema.org/schema/latest/lido.html#repositoryName)> <[lido:legalBodyID](https://lido-schema.org/schema/latest/lido.html#legalBodyID)> [*URI*](http://terminology.lido-schema.org/lido00099): [**Department of Paintings of the Louvre**](http://www.wikidata.org/entity/Q3044768)                                 |
| <[lido:displayState](https://lido-schema.org/schema/latest/lido.html#displayState)>             | Not applicable|Not applicable                                                              |
| <[lido:displaySubject](https://lido-schema.org/schema/latest/lido.html#displaySubject)>          | Half-length portrait of Lisa del Giocondo against the backdrop of a landscape | <[lido:subject](https://lido-schema.org/schema/latest/lido.html#subject)> [*Description*](http://terminology.lido-schema.org/lido00525) <[lido:subjectConcept](https://lido-schema.org/schema/latest/lido.html#subjectConcept)>  [**woman**](http://vocab.getty.edu/aat/300025943); [**half figure**](http://vocab.getty.edu/aat/300047469) <br/> <[lido:extentSubject](https://lido-schema.org/schema/latest/lido.html#extentSubject)> [**backgound**](http://vocab.getty.edu/aat/300056369) <[lido:subjectConcept](https://lido-schema.org/schema/latest/lido.html#subjectConcept)> [**landscape**](http://vocab.getty.edu/aat/300008626)  <br/><[lido:subject](https://lido-schema.org/schema/latest/lido.html#subject)> [*Identification*](http://terminology.lido-schema.org/lido00136) <[lido:subjectActor](https://lido-schema.org/schema/latest/lido.html#subjectActor)> [**Lisa del Giocondo**](http://viaf.org/viaf/50517148)

## Information areas and groups

Descriptive and administrative metadata in LIDO are organized into a total of seven structural elements, called **information areas**. This arrangement reflects basic information about content descriptions of cultural objects, rather than formal or technical requirements. Four of these areas are descriptive, while the other three hold administrative data.

Each information area in LIDO is a non-repeatable wrapper and a direct sub-element of the topmost descriptive or administrative metadata element. Being structural elements, the information areas do not hold any content data themselves. The following figure shows the seven areas, listed by their sequence in a LIDO record.

The direct sub-elements of the information areas are the main building blocks for structuring data content. These are called **information groups** in LIDO. Note that the information areas and information groups are introduced to provide a quick synopsis of major elements. Information areas and groups are not declared as such in the LIDO schema.

![Figure 7: Overview of the seven information areas in LIDO](img/figure_information-areas.png)

<br />

![Figure 8: Overview of information groups in LIDO](img/figure_information-groups.png)

<br />
**Table 3: Information areas and information groups**

| Information areas            | Information groups                                            |
| ----------------------------------------------- | ------------------------------------------------------------ |
|**1. Object Classification**<br><[lido:objectClassificationWrap](https://lido-schema.org/schema/latest/lido.html#objectClassificationWrap)>             | **1.** [Object/Work Type](https://lido-schema.org/schema/latest/lido.html#objectWorkTypeWrap)<br> **2.** [Classification](https://lido-schema.org/schema/latest/lido.html#classificationWrap)  |
|**2. Object Identification**<br><[lido:objectIdentificationWrap](https://lido-schema.org/schema/latest/lido.html#objectIdentificationWrap)>   | **3.** [Title](https://lido-schema.org/schema/latest/lido.html#titleWrap)<br>**4.** [Inscriptions](https://lido-schema.org/schema/latest/lido.html#inscriptionsWrap)<br>**5.** [Repository/Location](https://lido-schema.org/schema/latest/lido.html#repositoryWrap)<br>**6.** [State/Edition](https://lido-schema.org/schema/latest/lido.html#displayStateEditionWrap)<br>**7.** [Object Description](https://lido-schema.org/schema/latest/lido.html#objectDescriptionWrap)<br>**8.** [Object Measurements](https://lido-schema.org/schema/latest/lido.html#objectMeasurementsWrap)<br>**9.** [Object/MaterialsTechniques](https://lido-schema.org/schema/latest/lido.html#objectMaterialsTechWrap)  |                  |
|**3. Event**<br><[lido:eventWrap](https://lido-schema.org/schema/latest/lido.html#eventWrap)>               |**10.** [Event Set](https://lido-schema.org/schema/latest/lido.html#eventSet)                            |       |
|**4. Object Relation**<br><[lido:objectRelationWap](https://lido-schema.org/schema/latest/lido.html#objectRelationWrap)>  | **11.** [Subject](https://lido-schema.org/schema/latest/lido.html#subjectWrap)<br>**12.** [Related Works](https://lido-schema.org/schema/latest/lido.html#relatedWorksWrap)
|**5. Rights for Work**<br><[lido:rightsWorkWrap](https://lido-schema.org/schema/latest/lido.html#rightsWorkWrap)>   | **13.** [Rights for Work Set](https://lido-schema.org/schema/latest/lido.html#rightsWorkSet)
|**6. Record**<br><[lido:recordWrap](http://lido-schema.org/schema/latest/lido.html#recordWrap)>      | **14.** [Record Metadata Information Set](https://lido-schema.org/schema/latest/lido.html#recordInfoSet)                             |
|**7. Resource**<br><[lido:resourceWrap](http://lido-schema.org/schema/latest/lido.html#resourceWrap)>    | **15.** [Resource Set](https://lido-schema.org/schema/latest/lido.html#resourceSet) |

### Descriptive Metadata

[**Descriptive Metadata**](https://lido-schema.org/schema/latest/lido.html#descriptiveMetadata) in LIDO hold information about the properties of the described object itself, whether it is analog or digital. These data are essential for searching, finding, and identifying the object in collections or portals. Descriptive information about an object or work includes the following four information areas:

1. **Object Classification**
2. **Object Identification**
3. **Event**
4. **Object Relation**

#### Object Classification

The [**Object Classification Wrapper**](https://lido-schema.org/schema/latest/lido.html#objectClassificationWrap) groups objects together on the basis of shared characteristics for discovery. It contains the information groups for [Object/Work Type](https://lido-schema.org/schema/latest/lido.html#objectWorkTypeWrap) and [Classification](https://lido-schema.org/schema/latest/lido.html#classificationWrap) with the index elements <[lido:objectWorkType](https://lido-schema.org/schema/latest/lido.html#objectWorkType)> and <[lido:classification](https://lido-schema.org/schema/latest/lido.html#classification)>. While the former describes the object or work type as specific as possible, the latter groups objects together by broad categories, particularly for browsing or facet filtering. For information on the index elements <[lido:objectWorkType](https://lido-schema.org/schema/latest/lido.html#objectWorkType)> and <[lido:classification](https://lido-schema.org/schema/latest/lido.html#classification)> see sections [*5.1 Mandatory elements*](#mandatory-elements) and [*5.2 Recommended elements*](#recommended-elements).

#### Object Identification

The [**Object Identification Wrapper**](https://lido-schema.org/schema/latest/lido.html#objectIdentificationWrap) contains identifying information about an item, including its title, repository information, a textual object description, and description of the physical appearance. A minimum of information in this area is necessary to identify the unique item described in the LIDO record, distinguishing it from all similar ones in a collection, a local database, or an aggregation portal. For information on <[lido:titleSet](https://lido-schema.org/schema/latest/lido.html#titleSet)>, <[lido:objectMeasurements](https://lido-schema.org/schema/latest/lido.html#objectMeasurements)>, and <[lido:materialsTech](https://lido-schema.org/schema/latest/lido.html#materialsTech)> see sections [*5.1 Mandatory elements*](#mandatory-elements) and [*5.2 Recommended elements*](#recommended-elements).

#### Event

The [**Event Wrapper**](https://lido-schema.org/schema/latest/lido.html#eventWrap) contains the repeatable <[lido:eventSet](https://lido-schema.org/schema/latest/lido.html#eventSet)> and enables the description of various events the object or work has been involved in or is associated with in some way. The &lt;lido:eventSet&gt; itself may be equally repeated in order to record the history of an object and its provenance as a chain of custodial events.
An event-centric cataloging opens up ways to establish a network of contextual entities that may be associated with the event, usually actors, places, and dates. The LIDO elements <[lido:eventActor](https://lido-schema.org/schema/latest/lido.html#eventActor)>, <[lido:eventPlace](https://lido-schema.org/schema/latest/lido.html#eventPlace)>, and <[lido:eventDate](https://lido-schema.org/schema/latest/lido.html#eventDate)> reflect the high-level entities of the CIDOC CRM model, [E39 Actor](https://cidoc-crm.org/html/cidoc_crm_v7.1.1_with_translations.html#E39), [E53 Place](https://cidoc-crm.org/html/cidoc_crm_v7.1.1_with_translations.html#E53), and [E52 Time-Span](https://cidoc-crm.org/html/cidoc_crm_v7.1.1_with_translations.html#E52), respectively. For information on indexing events see section [*5.2 Recommended elements*](#recommended-elements).

#### Object Relation

The [**Object Relation Wrapper**](https://lido-schema.org/schema/latest/lido.html#objectRelationWrap) comprises information on the topics of the object in focus, i.e., what it is about, and possible relations to other objects or works. It contains the two information groups <[lido:subjectWrap](https://lido-schema.org/schema/latest/lido.html#subjectWrap)> and <[lido:relatedWorksWrap](https://lido-schema.org/schema/latest/lido.html#relatedWorksWrap)>. For information on the index elements <[lido:subject](https://lido-schema.org/schema/latest/lido.html#subject)> and <[lido:relatedWork](https://lido-schema.org/schema/latest/lido.html#relatedWork)> see sections [*5.1 Mandatory elements*](#mandatory-elements) and [*5.2 Recommended elements*](#recommended-elements).

### Administrative Metadata

[**Administrative Metadata**](https://lido-schema.org/schema/latest/lido.html#administrativeMetadata) in LIDO relate to the management of the described record or resource, and associated usage or intellectual property rights. This includes information about the rights holder, preservation and archiving processes the record or resource has gone through, as well as technical characterstics, e.g., for decoding or rendering the digital representation.

Information in the Administrative Metadata part of the LIDO schema is divided into three information areas, each of which has its dedicated wrapper element.

1. **Rights for Work**
2. **Record**
3. **Resource**

#### Rights for Work

The [**Rights for Work Wrapper**](https://lido-schema.org/schema/latest/lido.html#rightsWorkWrap) contains information about intellectual property and usage rights associated with the described object or work. "Work" here refers to the object at hand as either a distinct intellectual or artistic creation such as a human-made artifact, or as a natural object such as a plant or a mineral specimen. Being an object in its own right, it is distinct from any surrogate resource which is to be described in its own wrapper. The rights associated with the described object or work are documented in <[lido:rightsWorkSet](https://lido-schema.org/schema/latest/lido.html#rightsWorkSet)>.

**Note on Rights Type**

The <[lido:rightsType](https://lido-schema.org/schema/latest/lido.html#rightsType)> element allows for indexing rights pertaining to each of the above information areas, i.e., the object or work, the record, and the resource. LIDO allows two distinct usages of this element: a [generic type](http://terminology.lido-schema.org/lido00920) statement designating the kind of right in question, e.g., copyright or performance right, and [specific rights information](http://terminology.lido-schema.org/lido00921), such as a particular license. The values are provided by the LIDO [Rights Type Type-Vocabulary](http://terminology.lido-schema.org/rightsType_type).

**Example:** Following is an example of how to record a generic rights type.

```xml
<lido:rightsWorkWrap>
    <lido:rightsWorkSet>
        <!-- "Generic rights type" -->
        <lido:rightsType
            lido:type="http://terminology.lido-schema.org/lido00920">
            <skos:Concept
                rdf:about="http://vocab.getty.edu/aat/300055598">
                <skos:prefLabel xml:lang="en">
                    copyright
                </skos:prefLabel>
        </lido:rightsType>
    </lido:rightsWorkSet>
</lido:rightsWorkWrap>
```

**Example:** Following is an example of how to record specific rights information.

```xml
<lido:rightsWorkWrap>
    <lido:rightsWorkSet>
        <!-- "Specific rights information" -->
        <lido:rightsType
            lido:type="http://terminology.lido-schema.org/lido00921">
            <skos:Concept
                rdf:about="https://creativecommons.org/publicdomain/mark/1.0/">
                <skos:prefLabel xml:lang="en">
                    Public Domain Mark 1.0
                </skos:prefLabel>
            </skos:Concept>
        </lido:rightsType>
    </lido:rightsWorkSet>
</lido:rightsWorkWrap>
```

#### Record

The [**Record Wrapper**](http://lido-schema.org/schema/latest/lido.html#recordWrap) contains information about this very LIDO record. It encloses three mandatory elements: <[lido:recordID](https://lido-schema.org/schema/latest/lido.html#recordID)> relates to identifiers from systems where the description was produced, <[lido:recordType](https://lido-schema.org/schema/latest/lido.html#recordType)> indicates the description level chosen for the object, and <[lido:recordSource](https://lido-schema.org/schema/latest/lido.html#recordSource)> holds data about the producer of this record. Further elements relate  the identity of the originator, how the LIDO record can be referenced in databases or metadata repositories, and what rights are claimed by the originator for using this record. The rights associated with the record are documented in <[lido:recordRights](https://lido-schema.org/schema/latest/lido.html#recordRights)>.

Please note: Rights specifications in <[lido:recordRights](https://lido-schema.org/schema/latest/lido.html#recordRights)> refer to all elements of the current LIDO record. It is possible to specify different rights for individual object description texts in <[lido:descriptiveNoteValue](https://lido-schema.org/schema/latest/lido.html#descriptiveNoteValue)> using the element <[lido:objectDescriptionRights](https://lido-schema.org/schema/latest/lido.html#objectDescriptionRights)> in the corresponding <[lido:objectDescriptionSet](https://lido-schema.org/schema/latest/lido.html#objectDescriptionSet)>. Rights specifications in <[lido:objectDescriptionRights](https://lido-schema.org/schema/latest/lido.html#objectDescriptionRights)> overwrite the rights specifications in <[lido:recordRights](https://lido-schema.org/schema/latest/lido.html#recordRights)>.

#### Resource

The [**Resource Wrapper**](http://lido-schema.org/schema/latest/lido.html#resourceWrap) contains information about the digital resource or resources that serve as a surrogate for the object or work. Typically this consists of one or more digital representations such as digital images, video or audio files, or 3D models for online access. Information includes the type of the resource and its digital format, as well as its source and rights. Note that the resource element does not apply to items that are considered objects/works in their own right, but only to surrogates of such. The rights associated with the resource are documented in <[lido:rightsResource](https://lido-schema.org/schema/latest/lido.html#rightsResource)>.

> For further information about metadata see NISO (2017): [Understanding Metadata: What is Metadata, and What is it For?: A Primer](https://www.niso.org/publications/understanding-metadata-2017)

## Element structure

Some content elements in LIDO follow a fixed structural pattern: A LIDO <abbr title="In LIDO a repeatable structural element">set</abbr>, e.g., <[lido:subjectActor]()>, comprises a <abbr title="In LIDO a free-text element for human-readable descriptions">display element</abbr> followed by elements holding indexing information. This structure applies particularly to named entities for which an identifier may be used, such as actor, event, object, and place (see [*identifierComplexType*](https://lido-schema.org/schema/latest/lido.html#identifierComplexType)). This pattern is also followed for, e.g., collection information, descriptions of materials and techniques, or measurements. For more information on display and index elements see section [*4.3.3 Display and index elements*](#display-and-index-elements).

### Overview of the structure and the elements

The following figure shows the element structure, and an example for the description of an actor as subject of the described object or work. The sequence of the sub-elements of <[lido:actor](https://lido-schema.org/schema/latest/lido.html#actor_actorSetComplexType)> is determined by the LIDO specification for [*actorComplexType*](https://lido-schema.org/schema/latest/lido.html#actorComplexType).

![Figure 9: Element structure for a named entity](img/figure_element-structure.jpg)

**Example:** The following XML code snippet includes display and index elements to record identifying information about a person and its "[Cultural affiliation](http://terminology.lido-schema.org/lido01027)" as a type for <[lido:nationalityActor](https://lido-schema.org/schema/latest/lido.html#nationalityActor)>.

```xml
<lido:subjectActor>
    <lido:displayActor xml:lang="{...}">
        [...]
    </lido:displayActor>
    <!-- Person -->
    <lido:actor
        lido:type="http://terminology.lido-schema.org/lido00163">
        <lido:actorID lido:type="{...}">
            [...]
        </lido:actorID>
        <lido:nameActorSet>
            <!-- Preferred name -->
            <lido:appellationValue
                lido:pref="http://terminology.lido-schema.org/lido00169"
                xml:lang="{...}">
                [...]
            </lido:appellationValue>
        </lido:nameActorSet>
        <!-- Cultural affiliation -->
        <lido:nationalityActor
            lido:type="http://terminology.lido-schema.org/lido01027">
            <skos:Concept rdf:about="{...}"></skos:Concept>
        </lido:nationalityActor>
    </lido:actor>
</lido:subjectActor>
```

### Mandatory sequence of elements

The LIDO XML schema prescribes a sequence for elements occurring in elements defined by XML complex types. This sequence must always be observed. Validating parsers will detect any violation of mandatory sequences and will refuse to recognize the XML record as valid with respect to the LIDO schema definition.

Note, that only elements explicitly marked as *required* are mandatory to use in a LIDO record. For the description of an actor, for instance, it is required and sufficient to use the <[lido:nameActorSet](https://lido-schema.org/schema/latest/lido.html#nameActorSet)> element to get a valid LIDO record. If more information on the actor is given, such as a person's nationality or vital dates, the prescribed sequence of [*actorComplexType*](https://lido-schema.org/schema/latest/lido.html#actorComplexType) must be observed to satisfy the LIDO schema definition.

![Figure 10: Example sequence in LIDO actorComplexType](img/figure_sequence.jpg)
