rm util/primer.md;
cat primer.txt | while read LINE; do echo "\n" | cat "$LINE" -  >> util/primer.md ; done
pandoc util/primer.md -o primer.html -s --toc -c util/style.css --template util/template.html -H util/headerincludes.html -N