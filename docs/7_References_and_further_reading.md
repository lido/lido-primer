
# References and further reading

## LIDO

- LIDO XML Schema Definition. [XSD](<http://lido-schema.org/schema/latest/lido.xsd>)
- LIDO Documentation. [HTML](<http://lido-schema.org/schema/latest/lido.html>)
- LIDO Terminology Recommendation. [HTML](<http://lido-schema.org/documents/terminology-recommendation.html>)
- LIDO Terminology. [Website](<http://terminology.lido-schema.org>)
- LIDO Terminology LOD Interface Documentation. [Website](<https://cidoc.mini.icom.museum/working-groups/lido/lido-overview/lido-terminology/>)
- LIDO Examples. [Website](https://lido-schema.org/examples)
- LIDO Application Profiles [Website](https://cidoc.mini.icom.museum/working-groups/lido/lido-overview/profiles/)
- [LIDO Application Profile Painting and Sculpture] Knaus, Gudrun, Kailus, Angela und Stein, Regine: LIDO-Handbuch für die Erfassung und Publikation von Metadaten zu kulturellen Objekten: Band 2: Malerei und Skulptur, herausgegeben von Deutsches Dokumentationszentrum für Kunstgeschichte – Bildarchiv Foto Marburg und Christian Bracht, Heidelberg: arthistoricum.net, 2022 (LIDO-Handbuch, Band 2). [Website/PDF](https://doi.org/10.11588/arthistoricum.1026)
- [LIDO home] LIDO Website. [Website](<https://lido-schema.org>)

## General introductions

- Baca, Murtha (ed.) (2002). Introduction to Art Image Access: Issues, Tools, Standards, and Strategies. [Online Edition](https://www.getty.edu/research/publications/electronic_publications/intro_aia/index.html)
- Baca, Murtha (ed.) (2016). Introduction to Metadata (Third Edition). [Online Edition](https://www.getty.edu/publications/intrometadata/)
- Harpring, Patricia (2010). Introduction to Controlled Vocabularies: Terminology for Art, Architecture, and Other Cultural Works. [Online Edition](https://www.getty.edu/research/publications/electronic_publications/intro_controlled_vocab/index.html)
- Hyvönen, Eero (2012). Publishing and Using Cultural Heritage Linked Data on the Semantic Web. Morgan & Claypool.
- Joudrey, Daniel;  Taylor, Arlene G. (2018). The Organization of Information. Fourth edition. Libraries Unlimited.
- Jannidis, Fotis; Kohle, Hubertus; Rehbein, Malte (Hg.) (2017). Digital humanities. Eine Einführung. J. B. Metzler.
- Lubas, Rebecca; Jackson, Amy; Schneider, Ingrid (2013). The Metadata Manual. A practical workbook. Elsevier Science.
- Riley, Jenn (2017). Understanding Metadata: What is Metadata, and What is it For? A Primer Publication of the National Information Standards Organization. [PDF](https://groups.niso.org/higherlogic/ws/public/download/17446/Understanding%20Metadata.pdf)
- Zeng, Lei Marcia; Qin, Jan (2022). Metadata. Third edition. London: Facet Publishing.

## Metadata standards and guidelines

- [CDWA-Lite] J. Paul Getty Trust/ARTstor (2006). CDWA Lite. XML Schema Content for Contributing Records via the OAI Harvesting Protocol. [Website](https://www.getty.edu/research/publications/electronic_publications/cdwa/cdwalite.html)
- [CRM home] CIDOC Conceptual Reference Model (CRM). [Website](https://www.cidoc-crm.org/)
- [CRM 2006–] The International Committee for Documentation of the International Council of Museums (ICOM-CIDOC). [Versions of the CIDOC-CRM](https://cidoc-crm.org/versions-of-the-cidoc-crm)
- [CRM 2014] ISO 21127:2014. Information and documentation – A reference ontology for the interchange of cultural heritage information. [Reference](https://www.iso.org/standard/57832.html)
- [CRM 2014] Oldman, Dominic and CRM Labs (2014). The CIDOC Conceptual Reference Model (CIDOC-CRM): Primer. [PDF](https://www.cidoc-crm.org/sites/default/files/CRMPrimer_v1.1_1.pdf)
- [EDM home] Europeana Data Model. [Website](https://pro.europeana.eu/page/edm-documentation)
- [EDM 2013] Europenana Data Model Primer (2013). Editor: Antoine Isaac. [PDF](https://pro.europeana.eu/files/Europeana_Professional/Share_your_data/Technical_requirements/EDM_Documentation/EDM_Primer_130714.pdf)
- [EDM 2017] Definition of the Europeana Data Model v5.2.8. (2017). Europeana Foundation. [PDF](https://pro.europeana.eu/files/Europeana_Professional/Share_your_data/Technical_requirements/EDM_Documentation//EDM_Definition_v5.2.8_102017.pdf)
- [IFLA-FRBRoo] Bekiari, Chryssoula; Doerr, Martin; Le Boeuf, Patrick; Riva, Pat (2015). Definition of FRBRoo: A Conceptual Model for Bibliographic Information in Object-Oriented Formalism. [PDF](https://repository.ifla.org/bitstream/123456789/659/1/frbroo_v_2.4.pdf)
- [IFLA-LRM] Riva, Pat; Le Bœuf, Patrick; Žumer, Maja (2017). IFLA Library Reference Model: A Conceptual Model for Bibliographic Information. [PDF](https://repository.ifla.org/bitstream/123456789/40/1/ifla-lrm-august-2017_rev201712.pdf)
- [LinkedArt] Linked Art: Networking Digital Collections and Scholarship (2019–). Research Network funded by the UK Arts and Humanities Research Council (AHRC). [Website](https://linked.art/)
-[Spectrum] Collections Trust (2022). Spectrum 5.1. [Website](https://collectionstrust.org.uk/spectrum/)

## Content standards and guidelines

- [CCO 2006] Cataloging Cultural Objects: A Guide to Describing Cultural Works and Their Images. Visual Resources Association. Edited by Murtha Baca et al. (2006). American Library Association, Chicago. [PDF](https://static1.squarespace.com/static/64839bcb007bd907faf40da2/t/648682731ae85c00320c7d13/1686536835929/CatalogingCulturalObjectsFullv2.pdf)
- [CDWA] Categories for the Description of Works of Art (2009–). Edited by Murtha Baca and Patricia Harpring. Last revised 2022. [Online Edition](https://www.getty.edu/research/publications/electronic_publications/cdwa/index.html)
- [GVP] Getty Vocabulary Program. Getty Resarch Institute. [Website](https://www.getty.edu/research/tools/vocabularies/)
- [GVP 2014–] Training Materials. [Website](https://www.getty.edu/research/tools/vocabularies/training.html)
- [GVP 2020] CONA and Subject Access for Art Works. Overview of the CONA Depicted Subject. Patricia Harpring (2020). [PDF](https://www.getty.edu/research/tools/vocabularies/cona_and_subject_access.pdf)
- [GVP 2022] Cataloging Works at Museums and Special Collections: Documentation, Indexing, Access with CDWA, CCO, and the Getty Vocabularies. Patricia Harpring (Revised February 2022). [PDF](https://www.getty.edu/research/tools/vocabularies/cco_cdwa_for_museums.pdf)
- [GVP 2022] Utilizing Getty Vocabularies for Research and Retrieval. Patricia Harpring et al. (May 2022). [PDF](https://www.getty.edu/research/tools/vocabularies/vocabs_in_the_field.pdf) (9 MB, 71pp)
- [GVP 2022] Introduction and Application: Getty Vocabularies, CDWA, and CCO. Workshop (June 2022). [PDF](https://www.getty.edu/research/tools/vocabularies/cdwa_vocab_workshop.pdf) (13 MB, 166pp)

## Value vocabularies

- [AAT] Art & Architecture Thesaurus® Online. J. Paul Getty Trust, Getty Research Institutes, made available under the ODC Attribution License. [Website](https://www.getty.edu/research/tools/vocabularies/aat/index.html)
- [GeoNames]  GeoNames – open source geographical database. [Website](https://www.geonames.org/)
- [GND] Gemeinsame Normdatei (Integrated Authority File). Deutsche Nationalbibliothek. [Website](https://www.dnb.de/EN/Professionell/Standardisierung/GND/gnd_node.html). Access via [lobid.gnd](https://lobid.org/gnd) (German)
- [Iconclass] Iconclass. Third version of the online Iconclass browser. [Website](https://iconclass.org/)
- [LCSH] LOC Library of Congress Subject Headings. [Website](https://id.loc.gov/authorities/subjects.html)
- [TGN] Getty Thesaurus of Geographic Names® Online. J. Paul Getty Trust, Getty Research Institutes, made available under the ODC Attribution License. [Website](https://www.getty.edu/research/tools/vocabularies/tgn/index.html)
- [ULAN] Union List of Artist Names® Online. J. Paul Getty Trust, Getty Research Institutes, made available under the ODC Attribution License. [Website](https://www.getty.edu/research/tools/vocabularies/ulan/)
- [VIAF] VIAF: Virtual International Authority File. Hosted by OCLC. [Website](https://viaf.org/)
- [Wikidata] Wikimedia Foundation. Wikidata – the free knowledge base. [Website](https://www.wikidata.org/wiki/Wikidata:Main_Page)

## Linked Data and Semantic Web

- Heath, Tom; Bizer, Christian [2011]. Linked Data: Evolving the Web into a Global Data Space. [Online Edition 1.0.](http://linkeddatabook.com/editions/1.0/)
- European Commission (2013). Einführung in Linked Data. Open Data Support. [PDF](https://data.europa.eu/sites/default/files/d2.1.2_training_module_1.2_introduction_to_linked_data_de_edp.pdf) (in German)
- Europeana (2017). Linked Open Data. [Website](https://pro.europeana.eu/page/linked-open-data)
- FAIR Princples (2016). GO FAIR Initiative. [Website](https://www.go-fair.org/fair-principles/)
- UCLA Library [2022]. Semantic Web and Linked Data. (Last Updated Dec 12, 2022). [Home](https://guides.library.ucla.edu/semantic-web)
- W3C (2014). Linked Data Platform Best Practices and Guidelines. [Website](https://www.w3.org/TR/ldp-bp/)
- W3C (2015–). Semantic Web. [Website](https://www.w3.org/standards/semanticweb/)

## Application profiles

- Dublin Core Metadata Initiative (2009). Guidelines for Dublin Core™ Application Profiles. [Website](https://www.dublincore.org/specifications/dublin-core/profile-guidelines/)
- LOC Program for Cooperative Cataloging (2019). PCC Task Group on Metadata Application Profiles. [Website](https://www.loc.gov/aba/pcc/taskgroup/Metadata-Application-Profiles.html)
- UCLA Library [2022]. Best Practices, Standards, and Metadata Application Profiles (MAPs). (Last Updated Dec 12, 2022). [Website section](https://guides.library.ucla.edu/semantic-web/bestpractices)

## Formal languages and specifications

**XML and associated technologies**

- The Extensible Markup Language (XML) Version 1.0, W3C Recommendation 26 November 2008. [Website](https://www.w3.org/TR/xml/)
- The W3C XML Schema Definition Language (XSD) Version 1.1 Part 1: Structures. W3C Recommendation 5 April 2012. [Website](https://www.w3.org/TR/xmlschema11-1/)
- The XML Path Language (XPath) Version 3.1, W3C Recommendation 21 March 2017. [Website](https://www.w3.org/TR/xpath-31/)
- XSL Transformations (XSLT) Version 3.0, W3C Recommendation 8 June 2017. [Website](https://www.w3.org/TR/xslt-30/)
- The Schematron language website by Rick Jelliffe, 1999–2021. [Website](https://www.schematron.com/)

**RDF and associated technologies**

- The Resource Desciption Framework (RDF) Version 1.1 Concepts and Abstract Syntax. W3C Recommendation 25 February 2014. [Website](https://www.w3.org/TR/rdf11-concepts/)
- RDF Schema (RDFS) Version 1.1. W3C Recommendation 25 February 2014. [Website](https://www.w3.org/TR/rdf-schema/)
- The OWL 2 Web Ontology Language Document Overview (Second Edition). W3C Recommendation 11 December 2012. [Website](https://www.w3.org/TR/owl2-overview/)
- The SPARQL Query Language for RDF Version 1.1 Overview, W3C Recommendation 21 March 2013. [Website](https://www.w3.org/TR/sparql11-overview/)

**Schemas and protocols**

- The Simple Knowledge Organization System (SKOS) Reference, W3C Recommendation 18 August 2009. [Website](https://www.w3.org/TR/skos-reference/)
- The PROV Data Model (PROV-DM), W3C Recommendation 13 April 2013. [Website](https://www.w3.org/TR/prov-dm/)
- The Geography Markup Language (GML) Encoding Standard Version 3.2. Open Geospatial Consortium Inc., 2016. [PDF](https://portal.ogc.org/files/?artifact_id=74183&version=2)
- The OAI Protocol for Metadata Harvesting (OAI-PMH) Version 2.0. Open Archives Initiative 2002, Rev 2015. [Website](http://www.openarchives.org/OAI/openarchivesprotocol.html)
- The Linked Data Platform (Rules for HTTP operations) Version 1.0, W3C Recommendation 26 February 2015. [Website](https://www.w3.org/TR/ldp/)
