# LIDO Primer

*Dies ist die Seite, die beim Anklicken des Geamttitels (links oben in der Kopfzeile) erscheint.*

*Bitte erst dann aktualisieren, wenn die Struktur praktisch fertig steht. Sonst muss hier immer wieder nachgearbeitet werden.*

(BF 2024-09-11: Ich vermute, dass das Inhaltsverzeichnis hier unten nicht für die Erzeugung des Primers verwendet wird, da die aktuelle Primer-Inhaltsstruktur anders aussieht.)

******

This document is a guide for those who wish to familiarise themselves with the LIDO metadata schema, its background, intent, structure, and application.

## [1 Introduction](1_Introduction.md)

* [1.1 Scope of this document](1_Introduction.md#11-scope-of-this-document)
* [1.2 How to read this_document](1_Introduction.md#12-how-to-read-this-document)

## [2 About LIDO](2_About_LIDO.md)

* [2.1 What is LIDO?](2_About_LIDO.md#21-what-is-lido)
* [2.2 Why LIDO?](2_About_LIDO.md#22-why-lido)
* [2.3 Background](2_About_LIDO.md#23-background-of-lido)
* [2.4 Related standards](2_About_LIDO.md#24-related-standards)

## [3 LIDO design principles](3_Design_principles.md)

* [3.1 Choice of technologies](3_Design_principles.md#31-choice-of-technologies)
* [3.2 Supporting Web principles](3_Design_principles.md#32-supporting-web-principles)
* [3.3 Enabling adaptability](3_Design_principles.md#33-enabling-adaptability)

## [4 The basic structure of LIDO](4_Basic_structure_of_LIDO.md)

* [4.1 Top leve of LIDO](4_Basic_structure_of_LIDO.md#41-top-level-of-lido)
* [4.2.Information areas](4_Basic_structure_of_LIDO.md#42-information-areas)
* [4.3 Element nesting](4_Basic_structure_of_LIDO.md#43-element-nesting)
* [4.4 Element structure](4_Basic_structure_of_LIDO.md#44-element-structure)

## [5 Basic elements and attributes](5_Basic_elements_and_attributes.md)

* [5.1 Manddatory and recommended elements](5_Basic_elements_and_attributes.md#51-mandatory-and-recommended-elements)
* [5.2 Display and index elements](5_Basic_elements_and_attributes.md#52-display-and-index-elements)
* [5.3 Elements from other namespaces](5_Basic_elements_and_attributes.md#53-elements-from-other-namespaces)
* [5.4 Attributes in LIDO](5_Basic_elements_and_attributes.md#54-attributes-in-lido)

## [6 Checking metadata quality](6_Checking_metadata_quality.md)

## [7 References and further reading](7_References_and_further_reading.md)
