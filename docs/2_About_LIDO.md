

# About LIDO

## What is LIDO?

LIDO is a **metadata schema**, formally defined in the **XML schema** language. It enables a representation of data about a variety of objects of material culture, originating in heterogenous cataloging or collection management systems. The multitude of objects cover a wide range of objects including natural objects, works of fine art, items from decorative arts, archaeology, technology, life sciences, and other domains. Below, the term "cultural objects" refers to all these kinds of objects, following the [UNESCO definition](https://uis.unesco.org/en/glossary-term/cultural-heritage) of cultural heritage.

Among other features, LIDO is able to represent contextual information as events occurring in the lifetime of an object. This approach is highly flexible, easy to follow, and consistent with internationally agreed recommendations for museum documentation (see sections [*2.3 Background*](#background) and [*2.4 Related standards*](#related-standards)). Events can also connect an object description to the wider knowledge sphere, using [Linked Data](https://www.w3.org/standards/semanticweb/data) principles.

A single object or work, or a group of objects, is described in a **LIDO record** using XML syntax. A **LIDO document** can contain one or several LIDO records. The representation of the LIDO schema in LIDO XML documents is referred to as the **LIDO format**.

There are many use cases for a LIDO based data exchange and transfer in provider/consumer scenarios. LIDO was initially designed as a data **harvesting standard** and still serves this function in heritage institutions where metadata is often made available through the [OAI Protocol for Metadata Harvesting](https://www.openarchives.org/pmh/) (OAI-PMH). Harvesting refers to the process of collecting and updating metadata by aggregation portals.

Next to descriptive metadata, a LIDO record contains administrative metadata that may include rights information and links to resource representations. Aggregation portals can use these links for guiding the user to media items (such as facsimiles, images, audio or video files, or 3D models) representing the collection object described in the metadata record. While accessing and obtaining such media items may be the actual goal of interacting with a portal, the metadata record is essential for making them findable, comprehensible, and to aid in their discovery.

## Why LIDO?

LIDO is designed to allow for object descriptions at different levels of granularity or specificity. Few elements are considered mandatory in order to facilitate the recording of objects where detailed information is not available, while a great number of possible elements enables a fine-grained account of an object. The latter supports, for instance, a deeper exploration of data, possibly encouraging and facilitating research questions and the reuse of research data. By improving interoperability through the support of Linked Data principles and by providing data via interfaces or APIs, resources and research data become more visible and accessible.

The LIDO XML schema is designed to

- allow the description of objects or works, including non-artistic and natural objects, to the level desired;
- allow a data provider to decide how extensive the metadata sets they provide in a particular reuse context are, i.e., they can vary in different scenarios;
- enable the provision of metadata about digital surrogates representing the object in focus;
- provide links back to records in their home context, i.e., on an organization's website;
- enable the display of (detailed) information about an object in a reader-friendly form, while the record content is equally well prepared for retrieval;
- support the identification of an associated entity such as an actor, an event, a place, or a concept, by allowing for references to authority files and controlled vocabularies;
- provide full support for multilingual records, either at the structural element level or at the individual term or appellation element level, or both;
- distinguish between the identifiers of the documented object, the web page containing a description of the object, its online digital surrogates and the LIDO record itself.

Representing metadata in LIDO enables efficient processing for public information portals where collection objects can be discovered, searched, viewed and contextualised.

## Background

LIDO builds upon several pre-existing standards and guidelines for the description of cultural objects and metadata exchange. The LIDO schema integrates and extends CDWA Lite, is underpinned by the CIDOC Conceptual Reference Model (CIDOC CRM), and incorporates some information units from Spectrum definitions.

### CDWA Lite

[CDWA Lite](https://www.getty.edu/research/publications/electronic_publications/cdwa/cdwalite.pdf) is an XML schema derived from the [Categories for the Description of Works of Art](https://www.getty.edu/research/publications/electronic_publications/cdwa/) (CDWA), following the data content standard [Cataloging Cultural Objects](https://www.vraweb.org/cco?rq=cataloging%20cultural%20objects) (CCO), published by the [J. Paul Getty Trust](https://www.getty.edu/about/) and [ARTstor](https://www.artstor.org/). It is intended as a low-barrier way for institutions to contribute their collection metadata to union catalogs using the Open Archives Initiatives Protocol for Metadata Harvesting (OAI/PMH).

CDWA Lite defines 96 elements and sub-elements, most of which are integrated in LIDO. Note that the CDWA Lite schema is replaced by LIDO Version 1.0 since 2010.

The most important extension to CDWA Lite is the introduction of the <[lido:event](https://lido-schema.org/schema/latest/lido.html#event)> element, reflecting the event-centric approach taken by the CIDOC CRM mentioned below. The Event element not only permits to describe the object history as such, but also the entities (like actors and places) associated with each single event.

There is a [Metadata Standards Crosswalk](https://www.getty.edu/research/publications/electronic_publications/intrometadata/crosswalks.html) provided by the Getty Vocabulary Program. It departs from the full CDWA standard, comprising a mapping of a partial list of the elements for 15 standards from the museum, library and archive sectors, including CDWA Lite.

> For more information on the schema, see the publication  [CDWA Lite](https://www.getty.edu/research/publications/electronic_publications/cdwa/cdwalite.html) of the Getty Research Institute. For more information on the standard, see the [CDWA Homepage](https://www.getty.edu/research/publications/electronic_publications/cdwa/) and [CDWA and Other Metadata Standards](https://www.getty.edu/research/publications/electronic_publications/cdwa/moreinfo.html).

### CIDOC CRM

The [CIDOC Conceptual Reference Model](http://www.cidoc-crm.org/) is a formal ontology, published as ISO standard 21127 in 2006 (21127:2006), revised in 2014 (21127:2014). The CIDOC CRM provides definitions and a formal structure for describing foundational concepts and relationships used in cultural heritage documentation. It is intended to be a common language for domain experts and implementers when formulating requirements for information systems, and it is also regarded as an example for good practice of conceptual modeling. Development and maintenance work is carried out by the [CIDOC CRM Special Interest Group (SIG)](https://cidoc.mini.icom.museum/working-groups/crm-special-interest-group/), continuously active for more than two decades.

It was the CIDOC CRM event-centric modeling that inspired LIDO to introduce an Event element in addition to elements taken from CDWA Lite. Some of the CIDOC CRM classes and its definitions were considered in or aligned with the LIDO Terminology, particularly terms for the <[lido:category](https://lido-schema.org/schema/latest/lido.html#category)> and <[lido:eventType](https://lido-schema.org/schema/latest/lido.html#eventType)> elements. A mapping between [LIDO 1.0 and the CIDOC CRM version 6.0](https://cidoc-crm.org/Resources/mappings-between-lido-v1.0-and-cidoc-crm-v6.0-in-x3ml-ver.1) has been prepared in 2017, using the [X3ML](https://www.ics.forth.gr/isl/x3ml-toolkit) mapping definition language.

> For more information on the model and versions of the standard see [CIDOC CRM Home](http://www.cidoc-crm.org/).

### Spectrum

[Spectrum](https://collectionstrust.org.uk/spectrum/) is a collections management standard provided by the UK [Collections Trust](https://collectionstrust.org.uk/) charity. It describes activities, known as "procedures" in Spectrum, commonly encountered in managing museum collections. The standard also specifies [Information requirements](https://collectionstrust.org.uk/spectrum/information-requirements/) to be applied to procedures, for instance, [Object information groups](https://collectionstrust.org.uk/spectrum/information-requirements/object-information-groups/), further containing "Units of information" for documentation purposes, e.g., [Object production information](https://collectionstrust.org.uk/resource/object-production-information/). The Spectrum version 5.0 was published in September 2017, including 21 procedures, nine of which are considered "Primary procedures".

In LIDO, some of the information units defined in Spectrum have been considered, particulary from the [Object collection information](https://collectionstrust.org.uk/resource/object-collection-information/). There is also a list of mappings available in [Mapping Spectrum to other standards](https://collectionstrust.org.uk/spectrum/mapping-spectrum/), including a mapping from [Spectrum 5.0 to LIDO Version 1.0](https://collectionstrust.org.uk/resource/mapping-of-lido-to-spectrum-5-0/).

> For more information on Spectrum see [Introduction to Spectrum 5.0](https://collectionstrust.org.uk/spectrum/spectrum-5/) by Collectios Trust.

## Related standards

LIDO builds upon several standards, specifications, and guidelines. A structured view of relevant background information offers the following distinction:

- Formal models as reference frameworks for **data integration**, notably the [CIDOC Conceptual Reference Model](http://www.cidoc-crm.org/) (CIDOC CRM), the [IFLA Library Reference Model](https://www.ifla.org/publications/node/11412) (IFLA LRM), and [FRBRoo](https://repository.ifla.org/handle/123456789/659), a conceptual model for bibliographic information in object-oriented formalism.
- Rules for **data content** include cataloging guidelines, specifically [Categories for the Description of Works of Art](http://www.getty.edu/research/publications/electronic_publications/cdwa/index.html) (CDWA) and [Cataloging Cultural Objects](https://www.vraweb.org/cco?rq=cataloging%20cultural%20objects): A Guide to Describing Cultural Works and Their Images (CCO).
- Controlled vocabularies for **data values**, such as authority files like the [Union List of Artist Names](https://www.getty.edu/research/tools/vocabularies/ulan/) (ULAN) or thesauri like the [Art & Architecture Thesaurus](http://www.getty.edu/research/tools/vocabularies/aat/) (AAT).

![Figure 1: Related standards](img/figure_related-standards.png)

> For more related standards see the list of [Standards and guidelines](https://cidoc.mini.icom.museum/standards/cidoc-standards-guidelines/) provided by the CIDOC community, and the overview in [CDWA and Other Metadata Standards](https://www.getty.edu/research/publications/electronic_publications/cdwa/moreinfo.html) as well as the [Metadata Standards Crosswalk](https://www.getty.edu/research/publications/electronic_publications/intrometadata/crosswalks.html), both maintained by the Getty Vocabulary Program.
