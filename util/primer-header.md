---
title: LIDO Primer
subtitle: Lightweight Information Describing Objects (LIDO) Primer
date: 2025-03-17
license: CC BY 4.0
license-url: https://creativecommons.org/licenses/by/4.0/
author:
- Jutta Lindenthal
- Hanna-Lena Meiners
- Detlev Balzer
editor: CIDOC LIDO Working Group
editor-url: https://cidoc.mini.icom.museum/working-groups/lido/
contributor:
- Barbara Fichtl
- Simon Sendler
- Regine Stein
- LIDO-DE Working Group
---
