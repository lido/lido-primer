# LIDO Primer

Dies sind die Markdown-Dateien und zugehörige Bilder zum Erzeugen der Veröffentlichungsfassung mit MkDocs

## Dependencies

- Pandoc

## Local Build

First, make sure that there is no existing file `util/primer.md`.

Then, compile the chapters into a single markdown file:

`cat primer.txt | while read LINE; do echo "\n" | cat "$LINE" -  >> util/primer.md ; done`

Finally, call Pandoc to transform the markdown into the final primer.html

`pandoc util/primer.md -o primer.html -s --toc -c util/style.css --template util/template.html -H util/headerincludes.html`

This repository is optimized for the creation of the primer.html in a CI/CD-environment. Due to this, the included
JavaScript will not work locally on a clean clone. To fix this, change the content of `util/headerincludes.html` from

`<script src="navigation.js" charset="utf-8" defer></script>`

to

`<script src="util/navigation.js" charset="utf-8" defer></script>`

and rerun Pandoc as above.
