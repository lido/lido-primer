
# Checking metadata quality

Quality issues can affect processing, integration, and usefulness of a metadata record. Quality checks should therefore be organized according to different levels of compliance:

- Syntactic validity with respect to the schema definition
- Observation of additional formal constraints
- Observation of content rules and recommendations

## Syntactic validation

Reliable processing of XML records requires that the record structure follows the model definition. The LIDO record schema is expressed in the [W3C XML Schema Definition Language (XSD)](https://www.w3.org/TR/xmlschema11-1/), one of several formal grammars for specifying XML data structures. An XML record is said to be syntactically valid if (and only if) it meets all constraints laid down in the formal schema definition.

Since all further quality checks rely on the syntactic validity of a LIDO record, **XSD validation is the essential first step in quality assurance**. Software tools for checking XML records against XSD are widely available. In the case of the LIDO schema, such validators will also perform checks against the Geographical Markup Language (GML) definition used in LIDO for location coordinates. Note that the GML schema is many times larger than the LIDO schema, so that precautions against overuse of machine resources may have to be taken when high quantities of individual LIDO records need to be validated.

## Schematron

The schema definition of LIDO v1.1 contains embedded [Schematron](https://www.schematron.com/) rules that constrain certain aspects of the definition which cannot be expressed through the XML Schema Definition Language (XSD). A valid LIDO v1.1 document has therefore to validate against the structural definition contained in the XSD Schema while also complying to the Schematron assertions.

Schematron validates these constraints through rules implemented as W3C [XSL Transformations](https://www.w3.org/TR/xslt-30/). In other words, Schematron provides a workflow in which a transformation style sheet, when applied to a LIDO v1.1 document, will result in a report document (as [Schematron Validation Report Language, SVRL](https://www.schematron.com/document/3427.html)) about compliance to these rules.

Validation of Schema embedded Schematron rules is supported by several tools. Please refer to the respective documentation. For pre-built Schematron validation style sheets and technical documentation refer to <https://gitlab.gwdg.de/lido/development/-/tree/develop/1.1/schematron>.

## Further measures

Wherever feasible, additional checks should be developed for the detection of repeatedly (or even occasionally) occuring quality issues. In many cases, such checks can be implemented as sets of Schematron rules apart from those described above, as self-contained XSLT or XQuery scripts, or as tools written in any  programming language with support for XML technology.

Useful areas for such quality checks include:

- Compliance with the [LIDO Terminology Recommendation](https://lido-schema.org/documents/terminology-recommendation.html). These specify which sections of the LIDO Terminology or from other vocabularies can be applied in particular element or attribute contexts. For example, the [*pref*](http://lido-schema.org/schema/latest/lido.html#pref) attribute of a <[lido:term](https://lido-schema.org/schema/latest/lido.html#term)> element can only contain a URI from the LIDO [Preference Vocabulary](http://terminology.lido-schema.org/lido00168 ), and a statement in <[lido:objectWorkType](http://lido-schema.org/schema/latest/lido.html#objectWorkType)> should only be expressed with concepts from the AAT [Objects Facet](http://vocab.getty.edu/hier/aat/300264092).
- Appropriate usage of elements and attributes for specific object types. For example, a [Natural object](http://terminology.lido-schema.org/lido00817) cannot have a [Production](http://terminology.lido-schema.org/lido00007) event.
- Appropriate usage of textual elements. For example, object titles in the <[lido:titleSet](https://lido-schema.org/schema/latest/lido.html#titleSet)> element should consist of more than one word, not only of numbers, etc.
- Detection of contradictory statements. For example, the timespan of a [Collection](http://terminology.lido-schema.org/lido00010) event cannot precede that of the [Designing](http://terminology.lido-schema.org/lido00224) of the item.
- Improper use of type attributes. A <[lido:measurementType](https://lido-schema.org/schema/latest/lido.html#measurementType)> "height" is incompatible with a <[lido:measurementUnit](https://lido-schema.org/schema/latest/lido.html#measurementUnit)> "kilogram".
