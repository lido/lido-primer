

# Basic elements and attributes

This chapter introduces some of the most important content elements in LIDO, including elements from other namespaces, and gives an account of the attributes used for some of the LIDO elements. A brief explanation of the role of complex types in XML schema definitions and an enumeration of the complex types defined for the LIDO schema is appended.

## Mandatory elements

### Overview

Six content elements are declared mandatory in LIDO as a minimum requirement for a LIDO-compliant record. These elements are a subset of the core categories of <abbr title="Categories for the Description of Works of Art">CDWA</abbr>, and are considered necessary to unambiguously identify an object or work. The motivation for selecting only a small number of mandatory elements was to offer a low threshold for transforming existing data to LIDO. Having only few restrictions also leaves room for adapting the LIDO schema to different requirements. The convenience of easy data transformation, however, has to be balanced against the risk of accepting poor metadata that often leads to unsatisfactory search results. Therefore, some elements in addition to the minimum set are strongly recommended to be used in the object description.
Note that the elements [Descriptive Metadata](https://lido-schema.org/schema/latest/lido.html#descriptiveMetadata) and [Administrative Metadata](https://lido-schema.org/schema/latest/lido.html#administrativeMetadata) must be present in any valid LIDO record. Besides, there are more mandatory elements, depending on the use of other elements, see below.

The following six content elements are mandatory, listed in the sequence of appearance in a LIDO record:

1. **[LIDO Metadata Record Identifier](https://lido-schema.org/schema/latest/lido.html#lidoRecID)**
2. **[Object/Work Type](https://lido-schema.org/schema/latest/lido.html#objectWorkType)**
3. **[Title Set](https://lido-schema.org/schema/latest/lido.html#titleSet)**
4. **[Record Identifier](https://lido-schema.org/schema/latest/lido.html#recordID)**
5. **[Record Type](https://lido-schema.org/schema/latest/lido.html#recordType)**
6. **[Record Source](https://lido-schema.org/schema/latest/lido.html#recordSource)**

The mandatory elements must be contained in a LIDO record. If any of these are missing, a validating XML processor will reject the LIDO record as invalid. Note, that this validation concerns the syntactical conformance to the LIDO schema only;  it does not refer to the element contents, whether they contain data, and if this is semantically correct or not.

Therefore, the following example is a syntactically valid, yet semantically useless LIDO record.

```xml
<lido:lido>
    <lido:lidoRecID lido:type="xxx"></lido:lidoRecID>
    <lido:descriptiveMetadata xml:lang="xxx">
        <lido:objectClassificationWrap>
            <lido:objectWorkTypeWrap>
                <lido:objectWorkType></lido:objectWorkType>
            </lido:objectWorkTypeWrap>
        </lido:objectClassificationWrap>
        <lido:objectIdentificationWrap>
            <lido:titleWrap>
                <lido:titleSet>
                    <lido:appellationValue>
                    </lido:appellationValue>
                </lido:titleSet>
            </lido:titleWrap>
        </lido:objectIdentificationWrap>
    </lido:descriptiveMetadata>
    <lido:administrativeMetadata xml:lang="xxx">
        <lido:recordWrap>
            <lido:recordID lido:type="xxx"></lido:recordID>
            <lido:recordType></lido:recordType>
            <lido:recordSource></lido:recordSource>
        </lido:recordWrap>
    </lido:administrativeMetadata>
</lido:lido>
```

Note that more elements can become mandatory, depending on the use of a super-element (a *parent element* in XML terminology) or a sub-element (*child element* in XML), respectively. For example, <[lido:eventType](https://lido-schema.org/schema/latest/lido.html#eventType)> is mandatory if the super-element <[lido:event](https://lido-schema.org/schema/latest/lido.html#event)> is used; <[lido:titleWrap](https://lido-schema.org/schema/latest/lido.html#titleWrap)> is mandatory if the sub-element <[lido:titleSet](https://lido-schema.org/schema/latest/lido.html#titleSet)> is used.

**List of elements that are mandatory when the super-element (parent-element in XML) is used, listed in alphabetical order:**

- <[lido:actor](https://lido-schema.org/schema/latest/lido.html#actor)> if <[lido:actorInRole](https://lido-schema.org/schema/latest/lido.html#actorInRole)> is used
- <[lido:appellationValue](https://lido-schema.org/schema/latest/lido.html#appellationValue)> if a named entity or a title is described
- <[lido:eventType](https://lido-schema.org/schema/latest/lido.html#eventType)> if <[lido:event](https://lido-schema.org/schema/latest/lido.html#event)> is used
- <[lido:linkResource](https://lido-schema.org/schema/latest/lido.html#linkResource)> if <[lido:resourceRepresentation](https://lido-schema.org/schema/latest/lido.html#resourceRepresentation)> is used
- <[lido:measurementType](https://lido-schema.org/schema/latest/lido.html#measurementType)> if <[lido:measurementsSet](https://lido-schema.org/schema/latest/lido.html#measurementsSet)> or <[lido:resourceMeasurementsSet](https://lido-schema.org/schema/latest/lido.html#resourceMeasurementsSet)> is used
- <[lido:measurementUnit](https://lido-schema.org/schema/latest/lido.html#measurementUnit)> if <[lido:measurementsSet](https://lido-schema.org/schema/latest/lido.html#measurementsSet)> or <[lido:resourceMeasurementsSet](https://lido-schema.org/schema/latest/lido.html#resourceMeasurementsSet)> is used
- <[lido:measurementValue](https://lido-schema.org/schema/latest/lido.html#measurementValue)> if <[lido:measurementsSet](https://lido-schema.org/schema/latest/lido.html#measurementsSet)> or <[lido:resourceMeasurementsSet](https://lido-schema.org/schema/latest/lido.html#resourceMeasurementsSet)> is used
- <[lido:nameActorSet](https://lido-schema.org/schema/latest/lido.html#nameActorSet)> if <[lido:actor](https://lido-schema.org/schema/latest/lido.html#actor)> is used

**List of elements that are mandatory depending on the use of a sub-element (child-element in XML), listed in alphabetical order:**

- <[lido:classificationWrap](https://lido-schema.org/schema/latest/lido.html#classificationWrap)> if <[lido:classification](https://lido-schema.org/schema/latest/lido.html#classification)> is used
- <[lido:displayStateEditionWrap](https://lido-schema.org/schema/latest/lido.html#displayStateEditionWrap)> if one of the sub-elements is used
- <[lido:eventWrap](https://lido-schema.org/schema/latest/lido.html#eventWrap)> if <[lido:eventSet](https://lido-schema.org/schema/latest/lido.html#eventSet)> is used
- <[lido:inscriptionsWrap](https://lido-schema.org/schema/latest/lido.html#inscriptionsWrap)> if <[lido:inscriptions](https://lido-schema.org/schema/latest/lido.html#inscriptions)> is used
- <[lido:objectDescriptionWrap](https://lido-schema.org/schema/latest/lido.html#objectDescriptionWrap)> if <[lido:objectDescriptionSet](https://lido-schema.org/schema/latest/lido.html#objectDescriptionSet)> is used
- <[lido:objectMaterialsTechWrap](https://lido-schema.org/schema/latest/lido.html#objectMaterialsTechWrap)> if <[lido:objectMaterialsTechSet](https://lido-schema.org/schema/latest/lido.html#objectMaterialsTechSet)> is used
- <[lido:objectMeasurementsWrap](https://lido-schema.org/schema/latest/lido.html#objectMeasurementsWrap)> if <[lido:objectMeasurementsSet](https://lido-schema.org/schema/latest/lido.html#objectMeasurementsSet)> is used
- <[lido:objectRelationWrap](https://lido-schema.org/schema/latest/lido.html#objectRelationWrap)> if <[lido:subjectSet](https://lido-schema.org/schema/latest/lido.html#subjectSet)> and/or <[lido:relatedWorkSet](https://lido-schema.org/schema/latest/lido.html#relatedWorkSet)> is used
- <[lido:relatedWorksWrap](https://lido-schema.org/schema/latest/lido.html#relatedWorksWrap)> if <[lido:relatedWorkSet](https://lido-schema.org/schema/latest/lido.html#relatedWorkSet)> is used
- <[lido:repositoryWrap](https://lido-schema.org/schema/latest/lido.html#repositoryWrap)> if <[lido:repositorySet](https://lido-schema.org/schema/latest/lido.html#repositorySet)> is used
- <[lido:resourceWrap](https://lido-schema.org/schema/latest/lido.html#resourceWrap)> if <[lido:resourceSet](https://lido-schema.org/schema/latest/lido.html#resourceSet)> is used
- <[lido:rightsWorkWrap](https://lido-schema.org/schema/latest/lido.html#rightsWorkWrap)> if <[lido:rightsWorkSet](https://lido-schema.org/schema/latest/lido.html#rightsWorkSet)> is used
- <[lido:subjectWrap](https://lido-schema.org/schema/latest/lido.html#subjectWrap)> if <[lido:subjectSet](https://lido-schema.org/schema/latest/lido.html#subjectSet)> is used
- <[lido:titleWrap](https://lido-schema.org/schema/latest/lido.html#titleWrap)> if <[lido:titleSet](https://lido-schema.org/schema/latest/lido.html#titleSet)> is used

### LIDO Metadata Record Identifier

**Element name:** <[lido:lidoRecID](https://lido-schema.org/schema/latest/lido.html#lidoRecID)>

**Note:** This mandatory element serves to distinguish an individual LIDO record from any other record that may occur in a database, data repository, or any other aggregation of machine-processable records. The LIDO Metadata Record Identifier is preferably composed of an identifier for the contributor and a record identification in the (local) system of the contributor. It is not required to be persistent. For more detailed information and examples see section [*4.2.1 LIDO Metadata Record Identifier*](#lido-metadata-record-identifier).

### Object/Work Type

**Element name:** <[lido:objectWorkType](https://lido-schema.org/schema/latest/lido.html#objectWorkType)>

**Note:** The designation **object/work** dates from the CDWA Lite category [Object/Work](https://www.getty.edu/research/publications/electronic_publications/cdwa/1object.html), calling attention to the fact that the element embraces not only works of art, but, e.g., human-made everyday objects and natural specimens as well. This element captures the particular kind of an object, what the thing is in itself, considering its form and other intrinsic or defining characteristics. These generic properties are referred to as the ***isness*** of an object. Hence, a good descriptor for the object's type conveys essential features of the object as required for reliable indexing and retrieval.
The type of object at hand should always be indexed using the most specific descriptor available from the indexing vocabulary. Doing so is essential for ensuring a satisfactory level of precision in search results. In addition, there are more advantages when following the principle of the most specific and appropriate index entry (Cutter's rule) if relying on a well-structured knowledge organization system.
Specific descriptors for &lt;lido:objectWorkType&gt; may be found in the [Objects Facet](http://vocab.getty.edu/hier/aat/300264092) hierarchy of the Art & Architecture Thesaurus.

**Example:** In the following example the most specific concept from the Art & Architecture Thesaurus (AAT), "[oil painting](http://vocab.getty.edu/page/aat/300033799)", is used to describe the work type of the *Mona Lisa* by Leonardo da Vinci. This statement is further qualified by the value for the type attribute, "[Object by material or technique](http://terminology.lido-schema.org/lido00789)", taken from the LIDO Terminology and corresponding to the equivalent "[guide term](http://vocab.getty.edu/page/aat/300033701)" in the AAT hierarchy. The values for <[skos:prefLabel](http://www.w3.org/2004/02/skos/core#prefLabel)> and <[skos:altLabel](http://www.w3.org/2004/02/skos/core#altLabel)> are supposed to be automatically taken from the AAT concept. In the case where the labels provided by the authority vocabulary are not well suited for display purposes, a <[lido:term](https://lido-schema.org/schema/latest/lido.html#term)> with preference "[Display label](http://terminology.lido-schema.org/lido00526)" may be added to control how the term will be presented. Terms for the *lido:type* attribute of Object/Work Type are provided by the LIDO [Object/Work Type Type-Vocabulary](http://terminology.lido-schema.org/objectWorkType_type).

> **Object/Work Type:** Oil painting ***Preference:*** Display

```xml
<!--Object by material or technique-->
<lido:objectWorkType
    lido:type="http://terminology.lido-schema.org/lido00789">
    <skos:Concept
        rdf:about="http://vocab.getty.edu/aat/300033799">
        <skos:prefLabel xml:lang="en">
            oil paintings (visual works)
        </skos:prefLabel>
    </skos:Concept>
    <!-- Display label -->
    <lido:term
        lido:pref="http://terminology.lido-schema.org/lido00526"
        xml:lang="en">
        Oil painting
    </lido:term>
</lido:objectWorkType>
```
> See also [Cataloging Cultural Objects](https://vraweb.org/wp-content/uploads/2020/04/CatalogingCulturalObjectsFullv2.pdf): 1.1 About Object Naming, pp. 48–50.

### Title Set

**Element name:** <[lido:titleSet](http://lido-schema.org/schema/latest/lido.html#titleSet)>

**Note:** The Title Set element holds values for the appellation of the object or work, such as a title proper for a work or a name by which the object is known. A title is critical to always have a human-readable text to refer to an object, and making it distinguishable from similar objects in search results. There may exist multiple titles in a given language, as well as titles in different languages. One title has to be marked as the preferred one in each language, if there is more than one title; all other titles are regarded as alternative ones. It is strongly recommended to provide a descriptive, concise title that indicates the most important features to be recognized at a glance.
If no title or name is available, a descriptive one should be constructed based on the object/work type and further characteristics sufficient to select and distinguish the object in information retrieval. For example, "Daucus carota L." as a title may apply to dozens of plant specimens. Enriching the title with information from place and date elements can yield unique object titles such as "Daucus carota L. from Gmünd, Niederösterreich, 2009", or "Seats from [Zea mays L. from Missahoe (Togo)](https://www.deutsche-digitale-bibliothek.de/item/WMSGMGOH7OC4LBHIZ3B4TVSBVMSALBYU), 1914".
Where multiple titles are taken from different sources, the title set has to be repeated for each source. Note that (local) inventory numbers should not be recorded in titles.

**Example:** The following example shows a published title sufficiently descriptive for identification. The type for <[lido:titleSet](https://lido-schema.org/schema/latest/lido.html#titleSet)> is taken from the AAT "[titles (general, names)](http://vocab.getty.edu/hier/aat/300417193)" hierarchy as suggested in the LIDO Terminology Recommendation. The preferred title in this example is the best known one, "Mona Lisa". The published title is marked as an alternative one and useful for search queries. The CDWA guidelines recommend to always provide a descriptive title which in this example is drawn from the CONA record for "[Mona Lisa](http://vocab.getty.edu/page/cona/700000213)".

> **Title:** Mona Lisa  ***Preference:*** Preferred | **Source:** Getty Vocabulary Program, CONA [online], Mona Lisa.<br/>
**Descriptive title:** Portrait of Lisa Gherardini ***Preference:*** Alternative | **Source:** Getty Vocabulary Program, CONA [online], Mona Lisa.<br/>
**Published title:** PORTRAIT DE MONA LISA (1479-1528) ; DITE LA JOCONDE ***Preference:*** Alternative | **Source:** Collections des musées de France (Joconde), Portait de Mona Lisa.

```xml
<!-- Preferred title -->
<lido:titleSet
    lido:pref="http://terminology.lido-schema.org/lido00169">
<!-- Preferred appellation -->
    <lido:appellationValue
        lido:pref="http://terminology.lido-schema.org/lido00169"
        xml:lang="en">
        Mona Lisa
    </lido:appellationValue>
    <lido:appellationValue
        lido:pref="http://terminology.lido-schema.org/lido00169"
        xml:lang="de">
        Mona Lisa
    </lido:appellationValue>
    <lido:sourceAppellation
        xml:lang="en">
        Getty Vocabulary Program, CONA [online], Mona Lisa. URL: http://vocab.getty.edu/page/cona/700000213 [2022-07-04]
    </lido:sourceAppellation>
    <lido:sourceAppellation
        xml:lang="de">
        German National Library, GND, Mona Lisa. URL: https://d-nb.info/gnd/4074156-4 [2022-07-04]
    </lido:sourceAppellation>
</lido:titleSet>
<!-- Alternative title; descriptive title -->
<lido:titleSet
    lido:type="http://vocab.getty.edu/aat/300417199"
    lido:pref="http://terminology.lido-schema.org/lido00170">
    <!-- Alternative appellation -->
    <lido:appellationValue
        lido:pref="http://terminology.lido-schema.org/lido00170"
        xml:lang="en">
        Portrait of Lisa Gherardini
    </lido:appellationValue>
    <lido:sourceAppellation>
        Getty Vocabulary Program, CONA [online], Mona Lisa. URL: http://vocab.getty.edu/page/cona/700000213
    </lido:sourceAppellation>
</lido:titleSet>
<!-- Alternative title; published title -->
<lido:titleSet
    lido:type="http://vocab.getty.edu/aat/300417206"
    lido:pref="http://terminology.lido-schema.org/lido00170">
    <!-- Alternative appellation -->
    <lido:appellationValue
        lido:pref="http://terminology.lido-schema.org/lido00170"
        xml:lang="fr">
        PORTRAIT DE MONA LISA (1479-1528) ; DITE LA JOCONDE
    </lido:appellationValue>
    <lido:sourceAppellation>
        Collections des musées de France (Joconde), Portait de Mona Lisa. URL: https://www.pop.culture.gouv.fr/notice/joconde/000PE025604
    </lido:sourceAppellation>
</lido:titleSet>
```

> See also [Cataloging Cultural Objects](https://vraweb.org/wp-content/uploads/2020/04/CatalogingCulturalObjectsFullv2.pdf): 1.2.2 Rules for Title, pp. 58–69.

### Record Identifier

**Element name:** <[lido:recordID](http://lido-schema.org/schema/latest/lido.html#recordID)>

**Note:** The Record Identifier element is a text string uniquely identifying the record in the contributor's database or other recordkeeping system. It serves as a reference for all communication with the originator concerning the contents of the metadata record.

**Example:** The following example shows the local identifier for the record describing the "[Mona Lisa](https://www.bildindex.de/document/obj00076417)" provided by Deutsches Dokumentationszentrum für Kunstgeschichte - Bildarchiv Foto Marburg.

> **Record Identifier:** obj00076417 ***Type:*** Local identifier | **Source:** Deutsches Dokumentationszentrum für Kunstgeschichte - Bildarchiv Foto Marburg

```xml
<lido:recordID
    lido:source="Deutsches Dokumentationszentrum für Kunstgeschichte - Bildarchiv Foto Marburg"
    lido:type="http://terminology.lido-schema.org/lido00100">
    obj00076417
</lido:recordID>
```

### Record Type

**Element name:** <[lido:recordType](http://lido-schema.org/schema/latest/lido.html#recordType)>

**Note:** The Record Type element indicates the cataloging level selected for the record in question. It represents the logical tier of the <[lido](https://lido-schema.org/schema/latest/lido.html#lido)> object record, whether it refers to a single item, a part thereof, or a group of objects. Objects or works may be described at the following levels of granularity, as recommended in the LIDO [Record Type Vocabulary](http://terminology.lido-schema.org/lido00140):

- [*Item-level records*](http://terminology.lido-schema.org/lido00141) describe one single object or work in a single LIDO record, e.g., an "[Oil painting](https://www.getty.edu/research/publications/electronic_publications/cdwa/examples/02_oil_painting_2ricci.html)"; a description at item-level may also embrace parts of an object, e.g., a "[Sèvres Bowl](https://www.getty.edu/research/publications/electronic_publications/cdwa/examples/14_sevres_bowl.html)" comprising bowl, lid and dish.
- [*Group-level records*](http://terminology.lido-schema.org/lido00453) describe more than one object in a single LIDO record, e.g., a "[Collection of photographs](https://www.getty.edu/research/publications/electronic_publications/cdwa/examples/63_collection_of_photos.html)", a "[Group of drawings](https://www.getty.edu/research/publications/electronic_publications/cdwa/examples/56_group_of_drawings.html)", or a "[Tea service set](https://www.getty.edu/research/publications/electronic_publications/cdwa/examples/102_tea_set.html)".
- [*Component-level records*](http://terminology.lido-schema.org/lido00447) describe parts or components as distinct LIDO records. These records must be linked to the record for the whole, thus establishing a hierarchy of whole and parts; e.g., the "[Dome](https://www.getty.edu/research/publications/electronic_publications/cdwa/examples/120_dome.html)" as a component of Saint Peter's Basilica.
- [*Conceptual-level records*](http://terminology.lido-schema.org/lido01021) are designated to collocate other records. This level allows introducing a record that is merely notional in nature, not describing any real object as an instance. Typical uses are assembling items of a multiple, or for series items under a common concept; e.g., "[Great Wave off Kanagawa](http://vocab.getty.edu/page/cona/700008708)".

**Example:** The following example refers to the cataloging level of an item. The values for the concept URI and the labels are drawn from the LIDO [Record Type Vocabulary](http://terminology.lido-schema.org/lido00140). If desired, a custom display label may be added manually.

> **Record Type:** Item ***Preference:*** Display

```xml
<lido:recordType>
    <skos:Concept
        rdf:about="http://terminology.lido-schema.org/lido00141">
        <skos:prefLabel
            xml:lang="en">
            item-level record
        </skos:prefLabel>
        <skos:prefLabel
            xml:lang="de">
            Einzelobjekt
        </skos:prefLabel>
    </skos:Concept>
    <!-- Display label -->
    <lido:term
        lido:pref="http://terminology.lido-schema.org/lido00526"
        xml:lang="en">
        Item
    </lido:term>
</lido:recordType>
```

> See also Categories for the Description of Works of Art: [1.1. Catalog Level](https://www.getty.edu/research/publications/electronic_publications/cdwa/1object.html#RTFToC2a) in chapter [1. Object/Work](https://www.getty.edu/research/publications/electronic_publications/cdwa/1object.html).

### Record Source

**Element name:** <[lido:recordSource](http://lido-schema.org/schema/latest/lido.html#recordSource)>

**Note:** The Record Source element holds identifying information on the source from which, or where the <[lido](https://lido-schema.org/schema/latest/lido.html#lido)> object record was created. The source is usually the repository, institution or person creating the record in question.

**Example:** The following example shows the source information for the record describing the "[Mona Lisa](https://www.bildindex.de/document/obj00076417)" provided by Deutsches Dokumentationszentrum für Kunstgeschichte - Bildarchiv Foto Marburg.

> **Record Source:** Deutsches Dokumentationszentrum für Kunstgeschichte - Bildarchiv Foto Marburg<br/>
**Legal Body Identifier:** <https://ld.zdb-services.de/resource/organisations/DE-Mb112> ***Type:*** URI | ***Source:*** ISIL (ISO 15511)<br/>
**Appellation Value:** Deutsches Dokumentationszentrum für Kunstgeschichte - Bildarchiv Foto Marburg ***Preference:*** Preferred<br/>
**Legal Body Weblink:** <https://www.uni-marburg.de/de/fotomarburg>

```xml
<lido:recordSource>
    <!-- URI -->
    <lido:legalBodyID
        lido:type="http://terminology.lido-schema.org/lido00099"
        lido:source="ISIL (ISO 15511)">
        https://ld.zdb-services.de/resource/organisations/DE-Mb112
    </lido:legalBodyID>
    <lido:legalBodyName>
        <!-- Preferred label -->
        <lido:appellationValue
            lido:pref="http://terminology.lido-schema.org/lido00169"
            xml:lang="de">
            Deutsches Dokumentationszentrum für Kunstgeschichte - Bildarchiv Foto Marburg
        </lido:appellationValue>
        <lido:appellationValue
            lido:pref="http://terminology.lido-schema.org/lido00169"
            xml:lang="en">
            Foto Marburg Picture Archive
        </lido:appellationValue>
        <lido:sourceAppellation
            xml:lang="en">
            Philipps Universität Marburg. URL: https://www.uni-marburg.de/en/research/research-profile/academics_centres/academic-centers
        </lido:sourceAppellation>
    </lido:legalBodyName>
    <lido:legalBodyWeblink>
        https://www.uni-marburg.de/de/fotomarburg
    </lido:legalBodyWeblink>
</lido:recordSource>
```

### Example: Mandatory elements

The following XML code snippet shows the use of the mandatory elements in the record describing the “[Mona Lisa](https://www.bildindex.de/document/obj00076417)” provided by Deutsches Dokumentationszentrum für Kunstgeschichte - Bildarchiv Foto Marburg. Note that the value for the Object/Work Type is adapted to follow Cutter's rule of using the most specific descriptor. 

```xml
<lido:lido>
    <lido:lidoRecID
        lido:source="ld.zdb-services.de/resource/organisations/DE-Mb112"
        lido:type="http://terminology.lido-schema.org/lido00099">
        ld.zdb-services.de/resource/organisations/DE-Mb112/lido/obj/00076417
    </lido:lidoRecID>
    <lido:descriptiveMetadata xml:lang="en">
        <lido:objectClassificationWrap>
            <lido:objectWorkTypeWrap>
                <lido:objectWorkType>
                    <skos:Concept
                        rdf:about="http://vocab.getty.edu/aat/300033799">
                        <skos:prefLabel
                            xml:lang="en">
                            oil paintings (visual works)
                        </skos:prefLabel>
                    </skos:Concept>
                </lido:objectWorkType>
            </lido:objectWorkTypeWrap>
        </lido:objectClassificationWrap>
        <lido:objectIdentificationWrap>
            <lido:titleWrap>
                <lido:titleSet>
                    <lido:appellationValue
                        lido:pref="http://terminology.lido-schema.org/lido00169"
                        xml:lang="en">
                        Mona Lisa
                    </lido:appellationValue>
                </lido:titleSet>
            </lido:titleWrap>
        </lido:objectIdentificationWrap>
    </lido:descriptiveMetadata>
    <lido:administrativeMetadata
        xml:lang="en">
        <lido:recordWrap>
            <lido:recordID
                lido:source="ld.zdb-services.de/resource/organisations/DE-Mb112"
                lido:type="http://terminology.lido-schema.org/lido00100">
                obj00076417
            </lido:recordID>
            <lido:recordType>
                <skos:Concept
                    rdf:about="http://terminology.lido-schema.org/lido00141">
                    <skos:prefLabel
                        xml:lang="en">
                        Item-level record
                    </skos:prefLabel>
                </skos:Concept>
            </lido:recordType>
            <lido:recordSource>
                <lido:legalBodyID
                    lido:type="http://terminology.lido-schema.org/lido00099">
                    ld.zdb-services.de/resource/organisations/DE-Mb112
                </lido:legalBodyID>
                <lido:legalBodyName>
                    <lido:appellationValue
                        xml:lang="de">
                        Deutsches Dokumentationszentrum für Kunstgeschichte - Bildarchiv Foto Marburg
                    </lido:appellationValue>
                </lido:legalBodyName>
            </lido:recordSource>
        </lido:recordWrap>
    </lido:administrativeMetadata>
</lido:lido>
```

## Recommended elements

### Overview

The mandatory LIDO elements are sufficient to identify an object or work unambiguously, given that metadata elements are applied correctly. In most cases, however, providing the bare minimum of metadata will usually not be enough to enable good retrieval results in terms of findability and object discovery. Compared to LIDO, the CDWA standard defines some more elements as required, marked "core" in the [CDWA Overview of Categories](https://www.getty.edu/research/publications/electronic_publications/cdwa/categories.html). These are, besides information on the creator and creation, particularly metadata for classification and subject matter.

**Overview of mandatory and recommended elements in LIDO**

Mandatory elements are

- [LIDO Metadata Record Identifier](https://lido-schema.org/schema/latest/lido.html#lidoRecID)
- [Object/Work Type](http://lido-schema.org/schema/latest/lido.html#objectWorkType)
- [Title](http://lido-schema.org/schema/latest/lido.html#titleSet)
- [Record ID](http://lido-schema.org/schema/latest/lido.html#recordID)
- [Record Type](http://lido-schema.org/schema/latest/lido.html#recordType)
- [Record Source](http://lido-schema.org/schema/latest/lido.html#recordSource)

It is strongly recommended to provide indexing terms for the following elements. These elements are further described below.

- [Classification](http://lido-schema.org/schema/latest/lido.html#classification)
- [Measurements](http://lido-schema.org/schema/latest/lido.html#measurementsSet)
- [Materials/Techniques](http://lido-schema.org/schema/latest/lido.html#materialsTech)
- [Event](http://lido-schema.org/schema/latest/lido.html#event)
- [Subject](http://lido-schema.org/schema/latest/lido.html#subject)
- [Related Works](http://lido-schema.org/schema/latest/lido.html#relatedWork)

It is strongly recommended to provide information for [Rights for Work](http://lido-schema.org/schema/latest/lido.html#rightsWorkSet), [Rights for Record](http://lido-schema.org/schema/latest/lido.html#recordRights), and [Rights Resource](http://lido-schema.org/schema/latest/lido.html#rightsResource). For more information see section [*4.4.2 Administrative Metadata*](##administrative-metadata-1)".

It is also recommended to provide information for the following elements:

- [Inscriptions](http://lido-schema.org/schema/latest/lido.html#inscriptions)
- [Repository](http://lido-schema.org/schema/latest/lido.html#repositorySet)
- [Description](http://lido-schema.org/schema/latest/lido.html#objectDescriptionSet)
- [Record Metadata](http://lido-schema.org/schema/latest/lido.html#recordInfoSet)
- [Resource Representation](http://lido-schema.org/schema/latest/lido.html#resourceRepresentation)

In addition to the mandatory and recommended elements above, data producers are encouraged to utilize further schema elements in the LIDO record based on the following considerations:

- the nature of the items being described;
- the availability of data in the organization’s collections management system;
- the requirements of the target portal.

### Classification

**Element name:** <[lido:classification](https://lido-schema.org/schema/latest/lido.html#classification)>

**Note:** Classification assigns an object to one or more classes from a shared class scheme. Like <[lido:objectWorkType](http://lido-schema.org/schema/latest/lido.html#objectWorkType)>, the Classification element is used for grouping similar objects so that they can be retrieved in a single search operation. Unlike Object/WorkType which classifies the object at the most specific level suitable, the classification element aggregates objects on the basis of broad categories. For example, a work of type "oil painting" can be classified under "paintings", a "bas-relief" would be assigned to a class "sculptures", and a "cathedral" should be grouped into a category "architecture".

Classes can be used to implement browsing facilities in digital environments. Examples are the [Europeana Themes](https://www.europeana.eu/en/themes) or the access to the Louvre collections entitled [Explore the collections](https://collections.louvre.fr/en/). Such categories may also give a first impression of what can be expected to be found in the database and thus serve as a useful starting point for discovery in a Web portal.

An object can be assigned several classification terms from different classification schemes, depending on the aspect or point of view. The schemes may vary in the division criteria used to arrange the classes, such as by period, location, or status of property. An example is the categorization of the *Mona Lisa* in Wikipedia under [16th-century portraits](https://en.wikipedia.org/wiki/Category:16th-century_portraits), [Portrait paintings in the Louvre](https://en.wikipedia.org/wiki/Category:Portrait_paintings_in_the_Louvre), and [Stolen works of art](https://en.wikipedia.org/wiki/Category:Stolen_works_of_art).

Classification categories can refer to different aspects under which the object is viewed. Two common aspects are the *genre or form* represented by the object, reflecting its *isness* at a  broad level, and the *thematic context* the object is related to, such as a "subject category" or a domain-specific grouping. For example, a farmhouse like the [Olson House](https://d-nb.info/gnd/7843738-6) could be classified as a "building" by object genre and assigned to "agriculture" as subject category; for a religious building like the [Holy Name of Jesus Cathedral](https://d-nb.info/gnd/1173755292) the subject category might be "religion". Drawings of plants may be categorized by theme "botanics". These different aspects of categorization can be distinguished by using the *lido:type* attribute for <[lido:classification](https://lido-schema.org/schema/latest/lido.html#classification)>, for instance, "[Object genre](http://terminology.lido-schema.org/lido00853)" or "[Subject category](http://terminology.lido-schema.org/lido00932)", provided by the LIDO [Classification Type-Vocabulary](http://terminology.lido-schema.org/lido00088). The usage of classification for natural objects or for special collection domains is subject to further development.

While suitable schemes for broad classification are widely used in the library community, none of these has yet been released as an open data resource as demanded the FAIR principles. This also applies to classification schemes proposed for the museum community. As long as an overarching classification system that is appropriate for cross-domain metadata aggregations is lacking, the LIDO classification element should be used according to the recommendations given in the [CDWA](https://www.getty.edu/research/publications/electronic_publications/cdwa/2classification.html) guidelines.

**Example:** The following example refers to the *Mona Lisa* painting. In this particular case, classification is not very illuminating because there are no different aspects to consider. Nevertheless, it serves to illustrate the usage of different vocabularies for the different types of classification. The French term "peinture" is added here corresponding to the term being indexed and displayed displayed as "Category" in the [Louvre record](https://collections.louvre.fr/en/ark:/53355/cl010062370). The term "[peinture](https://collections.louvre.fr/en/recherche?nCategory%5B0%5D=40)" is used there to link to the search results for this index term in the "Louvre collections". Note that the URI for the Dewey Class in the following XML code snippet does not resolve. 

> **Classification:** paintings (visual works) ***Type:*** Object genre ***Preference:*** Display<br />
**Classification:** Painting and paintings ***Type:*** Subject category ***Preference:*** Preferred | **Notation:** 750

```xml
<lido:classificationWrap>
    <!-- Object genre -->
    <lido:classification
        lido:type="http://terminology.lido-schema.org/lido00853">
        <skos:Concept
            rdf:about="http://vocab.getty.edu/aat/300033618">
            <skos:prefLabel
                xml:lang="en">
                paintings (visual works)
            </skos:prefLabel>
            <skos:prefLabel
                xml:lang="de">
                Gemälde
            </skos:prefLabel>
            <skos:prefLabel
                xml:lang="fr">
                peintures (oeuvres visuelles)
            </skos:prefLabel>
        </skos:Concept>
        <lido:term
            lido:addedSearchTerm="yes"
            xml:lang="fr">
            peinture
        </lido:term>
        <!-- Display label -->
        <lido:term
            lido:pref="http://terminology.lido-schema.org/lido00526"
            xml:lang="fr">
            peinture
        </lido:term>
    </lido:classification>
    <!-- Subject category -->
        <lido:classification
            lido:type="http://terminology.lido-schema.org/lido00932">
        <!-- This URI does not resolve -->
            <skos:Concept
                rdf:about="http://dewey.info/class/750">
                <skos:prefLabel
                    xml:lang="en">
                    Painting and paintings
                </skos:prefLabel>
                <skos:prefLabel
                    xml:lang="de">
                    Malerei und Gemälde
                </skos:prefLabel>
            <skos:notation>750</skos:notation>
        </skos:Concept>
    </lido:classification>
</lido:classificationWrap>
```

### Measurements

**Element name:** <[lido:measurementsSet](https://lido-schema.org/schema/latest/lido.html#measurementsSet)>

**Note:** Measurements Set contains information about the dimensions of the object, comprising the measurement type, such as height or width, the corresponding unit, and the measured value. LIDO provides two elements: <[lido:objectMeasurementsSet](https://lido-schema.org/schema/latest/lido.html#objectMeasurementsSet)> is used to catalog the dimensions of the object at hand, whereas <[lido:eventObjectMeasurements](https://lido-schema.org/schema/latest/lido.html#eventObjectMeasurements)> refers to the measurements with respect to the event in focus, for instance, the decreased dimensions after the removal of a part.

**Example:** The following example describes the dimensions of the [*Mona Lisa*](https://collections.louvre.fr/en/ark:/53355/cl010062370) in the Louvre. The Measurements Set for "width" is omitted in the XML snippet for brevity. Added is information on the extent, i.e., the part of the object to which the dimensions apply, here the wood panel. Also added is the shape of the panel.

> **Display Object Measurements:** Height x width: 0.79 x 0.53 m<br />
**Type:** height  **Unit:** meter **Value:** 0.79<br />
**Type:** width   **Unit:** meter **Value:** 0.53<br />
**Extent Measurements:** wood panel<br />
**Shape Measurements:** rectangular<br />

```xml
<lido:objectMeasurementsSet>
    <lido:displayObjectMeasurements
        xml:lang="en">
        Height x width: 0.79 x 0.53 m
    </lido:displayObjectMeasurements>
    <lido:objectMeasurements>
        <lido:measurementsSet>
            <lido:measurementType>
                <skos:Concept
                    rdf:about="http://www.wikidata.org/wiki/Q208826">
                    <skos:prefLabel
                        xml:lang="en">
                        height
                    </skos:prefLabel>
                </skos:Concept>
                </lido:measurementType>
            <lido:measurementUnit>
                <skos:Concept
                    rdf:about="http://www.wikidata.org/wiki/Q11573">
                    <skos:prefLabel
                        xml:lang="en">
                        meter
                    </skos:prefLabel>
                </skos:Concept>
            </lido:measurementUnit>
            <lido:measurementValue>
                0.79
            </lido:measurementValue>
        </lido:measurementsSet>
        <lido:extentMeasurements>
            <skos:Concept
                rdf:about="http://vocab.getty.edu/aat/300014657">
                <skos:prefLabel
                    xml:lang="en">
                    panel (wood by form)
                </skos:prefLabel>
                <skos:altLabel
                    xml:lang="en">
                    wood panel
                </skos:altLabel>
            </skos:Concept>
        </lido:extentMeasurements>
        <lido:shapeMeasurements>
            <skos:Concept
                rdf:about="http://vocab.getty.edu/aat/300263831">
                <skos:prefLabel
                    xml:lang="en">
                    rectangular
                </skos:prefLabel>
            </skos:Concept>
        </lido:shapeMeasurements>
    </lido:objectMeasurements>
</lido:objectMeasurementsSet>
```

### Materials/Techniques

**Element name:** <[lido:materialsTech](https://lido-schema.org/schema/latest/lido.html#materialsTech)>

**Note:** Materials/Techniques contains information about the substances, such as the medium or support, and the techniques or implements, either incorporated in the object in focus, or used in the production or modification of the object. LIDO provides two elements, <[lido:objectMaterialsTechSet](https://lido-schema.org/schema/latest/lido.html#objectMaterialsTechSet)> and <[lido:eventMaterialsTech](https://lido-schema.org/schema/latest/lido.html#eventMaterialsTech)>, to catalog materials and techniques either as found in the object, or as used in the context of an event, respectively.

**Example:** The following example describes materials and techniques found in the *Mona Lisa* painting. The <[lido:extentMaterialsTech](https://lido-schema.org/schema/latest/lido.html#extentMaterialsTech)> element is applied to describe the part of the painting where a certain technique was utilized.

> **Display Materials/Techniques:** Oil on poplar panel<br />
**Materials/Techniques:** oil paint ***Type:*** Medium ***Preference:*** Display<br />
**Materials/Techniques:** poplar wood ***Type:*** Support ***Preference:*** Display<br />
**Materials/Techniques:** panel painting ***Type:*** Technique ***Preference:*** Display<br />
**Materials/Techniques:** sfumato ***Type:*** Technique ***Preference:*** Display | **Extent:** landscape ***Preference:*** Display

```xml
<lido:objectMaterialsTechSet>
    <lido:displayMaterialsTech
        xml:lang="en">
        Oil on poplar panel
    </lido:displayMaterialsTech>
    <lido:materialsTech>
        <!-- Medium -->
        <lido:termMaterialsTech
            lido:type="http://terminology.lido-schema.org/lido00513">
            <skos:Concept
                rdf:about="http://vocab.getty.edu/aat/300015050">
                <skos:prefLabel
                    xml:lang="en">
                    oil paint (paint)
                </skos:prefLabel>
            </skos:Concept>
            <lido:term
                lido:addedSearchTerm="yes"
                xml:lang="de">
                Ölfarbe
            </lido:term>
            <!-- Display label -->
            <lido:term
                lido:pref="http://terminology.lido-schema.org/lido00526"
                xml:lang="en">
                oil paint
            </lido:term>
        </lido:termMaterialsTech>
        <!-- Support -->
        <lido:termMaterialsTech
            lido:type="http://terminology.lido-schema.org/lido00514">
            <skos:Concept
                rdf:about="http://vocab.getty.edu/aat/300012363">
                <skos:prefLabel
                    xml:lang="en">
                    poplar (wood)
                </skos:prefLabel>
            </skos:Concept>
            <!--Display label -->
            <lido:term
                lido:pref="http://terminology.lido-schema.org/lido00526"
                xml:lang="en">
                poplar wood
            </lido:term>
        </lido:termMaterialsTech>
        <!-- Technique -->
        <lido:termMaterialsTech
            lido:type="http://terminology.lido-schema.org/lido00131">
            <skos:Concept
                rdf:about="http://vocab.getty.edu/aat/300178675">
                <skos:prefLabel
                    xml:lang="en">
                    panel painting (image-making)
                </skos:prefLabel>
            </skos:Concept>
            <!-- Display label -->
            <lido:term
                lido:pref="http://terminology.lido-schema.org/lido00526"
                xml:lang="en">
                panel painting
            </lido:term>
        </lido:termMaterialsTech>
    </lido:materialsTech>
    <lido:materialsTech>
        <!-- Technique -->
        <lido:termMaterialsTech
            lido:type="http://terminology.lido-schema.org/lido00131">
            <skos:Concept
                rdf:about="http://vocab.getty.edu/aat/300053421">
                <skos:prefLabel
                    xml:lang="en">
                    sfumato
                </skos:prefLabel>
            </skos:Concept>
            <!-- Display label -->
            <lido:term
                lido:pref="http://terminology.lido-schema.org/lido00526"
                xml:lang="en">
                sfumato
            </lido:term>
        </lido:termMaterialsTech>
        <lido:extentMaterialsTech>
            <skos:Concept
                rdf:about="http://vocab.getty.edu/aat/300008626">
                <skos:prefLabel
                    xml:lang="en">
                    landscapes (environments)
                </skos:prefLabel>
            </skos:Concept>
            <!-- Display label -->
            <lido:term
                lido:pref="http://terminology.lido-schema.org/lido00526"
                xml:lang="en">
                landscape
            </lido:term>
        </lido:extentMaterialsTech>
    </lido:materialsTech>
</lido:objectMaterialsTechSet>
```

### Event

**Element name:** <[lido:event](https://lido-schema.org/schema/latest/lido.html#event)>

**Note:** Event contains information about occurences associated with the object in some way. The element is meant to be used in the following contexts, to refer to
- an event the object participated in or was present at, e.g., its production, modification, or provenance as a series of events, to be recorded in <[lido:eventSet](https://lido-schema.org/schema/latest/lido.html#eventSet)>.

- an event that is related to the event in focus, e.g., the so-called "[Benin Expedition](https://www.wikidata.org/wiki/Q3966451)" as associated with the taking of the "[Benin Bronzes](https://www.wikidata.org/wiki/Q248101)", to be recorded in <[lido:relatedEventSet](https://lido-schema.org/schema/latest/lido.html#relatedEventSet)>;
- an event that is a topic of the object in focus, e.g., the "Coronation of Maria de Medici" as a theme in Rubens' painting "The death and apothesis of Henry IV", to be recorded in <[lido:subjectEventSet](https://lido-schema.org/schema/latest/lido.html#subjectEventSet)>.

It is recommended to catalog a production event for human-made objects as a minimum. For natural objects, the equivalent should be the collection event. If the creator or producer of a human-made object is not identified, the value "[unknown](http://vocab.getty.edu/ulan/500125274)" from the Union List of Artist Names (ULAN) authority may be used as a value. If known or applicable, the cultural context for the object should be indexed.

#### Event Type

**Element name:** <[lido:eventType](https://lido-schema.org/schema/latest/lido.html#eventType)>

**Note:** Event Type indicates the kind of an event, whether it is, e.g., the production, acquisition, or use of an object in its lifecycle. Terms for these types are provided by the LIDO Terminology or by application profiles. For related events, and events as subjects of a work, the term for Event Type should be drawn from external authorities.

This element is required if <[lido:event](https://lido-schema.org/schema/latest/lido.html#event)> is set.

Terms for Event Type in the object's lifecycle (<[lido:eventSet](https://lido-schema.org/schema/latest/lido.html#eventSet)>) are provided by the LIDO [Event Type Vocabulary](http://terminology.lido-schema.org/eventType). If terms of the Event Type Terminology are not sufficient for a specific use case concerning the object's lifecycle, compatible vocabulary recommendations may be provided within an application profile. Such recommendations should ideally extend the LIDO Event Type vocabulary.

Terms for <[lido:relatedEvent](https://lido-schema.org/schema/latest/lido.html#relatedEvent)> and <[lido:subjectEvent](https://lido-schema.org/schema/latest/lido.html#subjectEvent)> should be drawn from external authorities. For suggestions see the LIDO [Terminology Recommendation](http://lido-schema.org/documents/terminology-recommendation.html#subjectEvent).

**Example:** The following example describes the production of the *Mona Lisa* as the most notable event. The Event Type is indexed with the concept "[Production](http://terminology.lido-schema.org/lido00007)" from the LIDO Terminology. The mapping relation to the AAT descriptor is provided by the LIDO Term.

> **Event:** Painted by Leonardo da Vinci, probably between 1503/1519<br />
**Event Type:** Production ***Preference:*** Preferred

```xml
<lido:eventSet
    lido:sortorder="2"
    lido:mostNotableEvent="1">
    <lido:displayEvent
        xml:lang="en">
        Painted by Leonardo da Vinci, probably between 1503/1519
    </lido:displayEvent>
    <lido:event>
        <lido:eventType>
            <skos:Concept
                rdf:about="http://terminology.lido-schema.org/lido00007">
                <skos:prefLabel
                    xml:lang="en">
                    Production
                </skos:prefLabel>
            </skos:Concept>
        </lido:eventType>
    </lido:event>
</lido:eventSet>
```

#### Event Actor

**Element name:** <[lido:eventActor](https://lido-schema.org/schema/latest/lido.html#eventActor)>

**Note:** Event Actor contains information about the person or organization involved in the event in focus, including identifying data, names, roles, and possibly biographical details.

**Example:** The following example describes Leonardo da Vinci in the role of painter. The reference to the painting of the *Mona Lisa* is provided by the parent element <[lido:eventSet](https://lido-schema.org/schema/latest/lido.html#eventSet)> with the Event Type "[Production](http://terminology.lido-schema.org/lido00007)".

> **Actor:** Painter: Leonardo da Vinci (1452–1519)<br />
**Name Actor:** Leonardo, da Vinci ***Preference:*** Preferred • Leonardo da Vinci ***Preference:*** Display<br />
**Role Actor:** painter ***Preference:*** Display

```xml
<lido:eventActor>
    <lido:displayActorInRole
        xml:lang="en">
        Painter: Leonardo da Vinci (1452–1519)
    </lido:displayActorInRole>
    <lido:actorInRole>
        <lido:actor
            lido:type="http://terminology.lido-schema.org/lido00413">
            <lido:actorID
                lido:type="http://terminology.lido-schema.org/lido00099">
                http://viaf.org/viaf/24604287
            </lido:actorID>
            <lido:nameActorSet>
                <!-- Preferred name -->
                <lido:appellationValue
                    lido:pref="http://terminology.lido-schema.org/lido00169"
                    xml:lang="en">
                    Leonardo, da Vinci
                </lido:appellationValue>
                <!-- Display name -->
                <lido:appellationValue
                    lido:pref="http://terminology.lido-schema.org/lido00526"
                    xml:lang="en">
                    Leonardo da Vinci
                </lido:appellationValue>
            </lido:nameActorSet>
        </lido:actor>
        <lido:roleActor>
            <skos:Concept
                rdf:about="http://vocab.getty.edu/aat/300025136">
                <skos:prefLabel
                    xml:lang="en">
                    painters (artists)
                </skos:prefLabel>
                <skos:prefLabel
                    xml:lang="de">
                    Maler
                </skos:prefLabel>
            </skos:Concept>
            <!-- Display label -->
            <lido:term
                lido:pref="http://terminology.lido-schema.org/lido00526"
                xml:lang="en">
                painter
            </lido:term>
            <lido:term
                lido:pref="http://terminology.lido-schema.org/lido00526"
                xml:lang="de">
                Maler
            </lido:term>
        </lido:roleActor>
    </lido:actorInRole>
</lido:eventActor>
```

#### Event Place

**Element name:** <[lido:eventPlace](https://lido-schema.org/schema/latest/lido.html#eventPlace)>

**Note:** Event Place contains information about the place where the event occured, including identifying data, names and possibly coordinates. The type of place, whether it is an adminstrative entity such as a city or country, or a physical feature such as a river or a mountain, can be captured in detail using the <[lido:placeClassification](https://lido-schema.org/schema/latest/lido.html#placeClassification)> element.

**Example:** The following example describes the city "Florence" as a possible place of production of the *Mona Lisa*. The reference to the painting is provided by the <[lido:eventSet](https://lido-schema.org/schema/latest/lido.html#eventSet)> element with the Event Type "[Production](http://terminology.lido-schema.org/lido00007)". The place type "city" is expressed by the attribute *[lido:politicalEntity](https://lido-schema.org/schema/latest/lido.html#politicalEntity)*, and the URI for "city" is taken from Wikidata.

>**Event Place:** Florence between 1502/1506 ***Political Entity:*** City<br />
**Name Place:** Florence ***Preference:*** Preferred<br />
**Coordinates:** 43.771389, 11.254167

```xml
<lido:eventPlace>
    <lido:displayPlace
        xml:lang="en">
        Possibly Florence, between 1502/1506
    </lido:displayPlace>
    <!-- City -->
    <lido:place
        lido:politicalEntity="http://www.wikidata.org/entity/Q515">
        <lido:placeID
            lido:type="http://terminology.lido-schema.org/lido00099">
            http://www.wikidata.org/entity/Q2044
        </lido:placeID>
        <lido:namePlaceSet>
            <!-- Preferred label -->
            <lido:appellationValue
                lido:pref="http://terminology.lido-schema.org/lido00169"
                xml:lang="en">
                Florence
            </lido:appellationValue>
        </lido:namePlaceSet>
        <lido:gml>
            <gml:Point>
                <gml:pos>43.771389, 11.254167</gml:pos>
            </gml:Point>
        </lido:gml>
    </lido:place>
</lido:eventPlace>
```

#### Event Date

**Element name:** <[lido:eventDate](https://lido-schema.org/schema/latest/lido.html#eventDate)>

**Note:** Event Date contains information about the date or timespan when the event took place. The dates are expressed in the elements <[lido:earliestDate](https://lido-schema.org/schema/latest/lido.html#earliestDate)> and <[lido:latestDate](https://lido-schema.org/schema/latest/lido.html#latestDate)>, both of which are required. If earliest and latest date concur, the date is given twice.
**Example:** The following example describes the estimated timespan for the production of the *Mona Lisa*. The reference to the painting is provided by the parent element <[lido:eventSet](https://lido-schema.org/schema/latest/lido.html#eventSet)> with the Event Type "[Production](http://terminology.lido-schema.org/lido00007)".

> **Event Date:** Between 1506/1519<br />
**Earliest Date:** 1506 ***Type:*** Estimated date<br />
**Latest Date:** 1519 ***Type:*** Estimated date

```xml
<lido:eventDate>
    <lido:displayDate xml:lang="en">
        Between 1506/1519
    </lido:displayDate>
    <lido:date>
        <!-- Estimated date -->
        <lido:earliestDate
            lido:type="http://terminology.lido-schema.org/lido00529">
            1503
        </lido:earliestDate>
        <lido:latestDate
            lido:type="http://terminology.lido-schema.org/lido00529">
            1519
        </lido:latestDate>
    </lido:date>
</lido:eventDate>
```

### Related Work

**Element name:** <[lido:relatedWork](https://lido-schema.org/schema/latest/lido.html#relatedWork)>

**Note**: Related Work contains information about an object that is directly associated with the object in focus. An extensive list of such relationships can be found in the LIDO Terminology for [Relationship Type](http://terminology.lido-schema.org/lido00138). Examples are [preparatory](http://terminology.lido-schema.org/lido00586) and [derivative]() relationhips, or associations between [companion pieces](http://terminology.lido-schema.org/lido00579). However, there is no general answer to whether or not a work should be linked as associated. The decision will depend on how the benefits in retrieval are estimated. Will retrieving both objects at one go be meaningful to users? Or will the relation of the objects lead to a deluge of unwanted search results? These questions should be weighed when establishing an associative relation between works.

**Example:** The following example describes the ready made "L.H.O.O.Q" by Marcel Duchamp as a related work to Leonardo's *Mona Lisa*. Although these works are not directly associated, they are contextually connected in such a way that users may want to become aware of both in one search session.

> **Related Work:** Marcel Duchamp, L.H.O.O.Q., Ready-made 1919<br />
**Note:** L.H.O.O.Q; photographic reproduction; Marcel Duchamp; 1919; Israel Museum<br />
**Relationship Type:** is reproduction of

```xml
<lido:relatedWorkSet
    lido:sortorder="3">
    <lido:displayRelatedWork
        xml:lang="en">
        Marcel Duchamp, L.H.O.O.Q., Ready-made 1919
    </lido:displayRelatedWork>
    <lido:relatedWork>
        <lido:object>
            <lido:objectWebResource
                lido:formatResource="text/html"
                xml:lang="en">
                https://www.imj.org.il/en/collections/199796-0
            </lido:objectWebResource>
            <lido:objectID
                lido:type="http://terminology.lido-schema.org/lido00099"
                lido:source="Israel Museum, Jerusalem">
                B99.0575
            </lido:objectID>
            <lido:objectNote
                xml:lang="en">
                L.H.O.O.Q; photographic reproduction; Marcel Duchamp; 1919; Israel Museum
            </lido:objectNote>
        </lido:object>
    </lido:relatedWork>
    <lido:relatedWorkRelType>
        <skos:Concept
            rdf:about="http://terminology.lido-schema.org/lido00607">
            <skos:prefLabel
                xml:lang="en">
                is reproduction of
            </skos:prefLabel>
        </skos:Concept>
    </lido:relatedWorkRelType>
</lido:relatedWorkSet>
```

### Subject

**Element name:** <[lido:subject](https://lido-schema.org/schema/latest/lido.html#subject)>

**Note:** Subject contains information about what is shown in an object or what is a theme of the work in focus. Indexing subject matter is strongly recommended, since it is a primary access point in retrieval, and users quite often perform topical searches. Subject may occur as depicted items, themes, or narrative content, and refers to abstract concepts and equally refers to abstract concepts and named entities.

Terms designating a generic notion are indexed in <[lido:subjectConcept](https://lido-schema.org/schema/latest/lido.html#subjectConcept)>. Examples for concepts in the role of subject may be perceivable things, such as bridges or bicycles, or abstract ideas, like love or fear. In the context of works of art, what is depicted is referred to as the *ofness* of an object, e.g., a sitting woman, a landscape, an armrest. For this kind of subject, the type is "[Description](http://terminology.lido-schema.org/lido00525)".
Beyond this, cultural objects most often have content calling for interpretation, e.g., when a picture conveys allegorical meaning or relates to a mythological narrative. This kind of knowledgeable information is called the *aboutness* of an object. Here, the type of subject is "[Interpretation](http://terminology.lido-schema.org/lido00524)". In the library context aboutness refers to the totality of subjects explicitly or implicitly addressed in the text of a document. Note that indexing values for &lt;lido:subjectConcept&gt; drawn from Iconclass are of type "[Iconclass notation](http://terminology.lido-schema.org/lido00745)".

Content referring to named entities, such as a certain person or a particular place, is indexed in distinct LIDO elements: <[lido:subjectActor](https://lido-schema.org/schema/latest/lido.html#subjectActor)> for individual persons or organizations, whether alive or historical, <[lido:subjectDate](https://lido-schema.org/schema/latest/lido.html#subjectDate)> for date values,<[lido:subjectEvent](https://lido-schema.org/schema/latest/lido.html#subjectEvent)> for named historical events, <[lido:subjectPlace](https://lido-schema.org/schema/latest/lido.html#subjectPlace)> for real places, and <[lido:subjectObject](https://lido-schema.org/schema/latest/lido.html#subjectObject)> for a named object that is shown or described in or by the object at hand. Please note, that named objects comprise not only material objects, but also immaterial ones, that are documented as single units or serve as topic of discourse. These include fictitious actors, fictitious places and fictitious events, for instance, from legends, religion, mythology, or literature and performing arts. They are all indexed as <[lido:subjectObject](https://lido-schema.org/schema/latest/lido.html#subjectObject)>.


The subject type for named entities is "[Identification](http://terminology.lido-schema.org/lido00136)". Note that indexing values from Iconclass belong to a type of its own, labelled "[Iconclass notation](http://terminology.lido-schema.org/lido00745)".

The usage of subject for, e.g., natural objects, buildings, or utilitarian items, should be specifically elaborated for respective areas of collection. In the following element descriptions, <[lido:subjectDate](https://lido-schema.org/schema/latest/lido.html#subjectDate)> is omitted since the value recommendations are the same as for <[lido:date](https://lido-schema.org/schema/latest/lido.html#date)> in general.

#### Type of Subject

The type of <[lido:subject](https://lido-schema.org/schema/latest/lido.html#subject)>, also called "indexing type", indicates whether the indexing term is an impartial *description* of what is depicted, a more subjective *interpretation* of the topic, or an *identification* of a named entity. The following types are the most common ones:

- [Description](http://terminology.lido-schema.org/lido00525) relates to the *ofness* of an object. It states what the depicted content is.
- [Interpretation](http://terminology.lido-schema.org/lido00524) relates to the *aboutness* of an object, stating what the thematic, iconographical, or symbolic meaning is.
- [Identification](http://terminology.lido-schema.org/lido00136) relates to named individuals that the object at hand is of or about.
- [Iconclass notation](http://terminology.lido-schema.org/lido00745) is used for all subjects, whether concepts or named entities, that are indexed with an Iconclass notation.

For subject types see terms in the LIDO [Subject Type-Vocabulary](https://lido-schema.org/documents/terminology-recommendation.html#subject_type).

#### Subject Concept

**Element name:** <[lido:subjectConcept](https://lido-schema.org/schema/latest/lido.html#subjectConcept)>

**Note:** Subject Concept contains generic concepts describing what the object depicts or what it is about.

**Example for "Description":** The following example refers to a term used as subject for Leonardo's painting in the [EDM Primer](https://pro.europeana.eu/files/Europeana_Professional/Share_your_data/Technical_requirements/EDM_Documentation/EDM_Primer_130714.pdf), showing the possible inference of broader concepts when using controlled structured vocabularies (see Fig. 9 Mona Lisa – enriched using contextual entities, page 15). In the example below, it is concluded from the Wikidata subclass relation for "[woman](http://www.wikidata.org/entity/Q467)" that a woman is a "[female human](https://www.wikidata.org/wiki/Q84048852)" which in turn is a "[human](https://www.wikidata.org/wiki/Q5)". Subject type is "Description" because the indexing term refers to what can be seen in the picture by a non-expert.

> **Subject Concept:** woman ***Preference:*** Display<br/>
***Subject Type:*** Description

```xml
<lido:subjectSet>
  <!-- Description -->
    <lido:subject
        lido:type="http://terminology.lido-schema.org/lido00525">
        <lido:subjectConcept>
            <skos:Concept
                rdf:about="http://www.wikidata.org/entity/Q467">
                <skos:prefLabel
                    xml:lang="en">
                    women (human females)
                </skos:prefLabel>
            </skos:Concept>
            <!-- Display label -->
            <lido:term
                lido:pref="http://terminology.lido-schema.org/lido00526"
                xml:lang="en">
                woman
            </lido:term>
        </lido:subjectConcept>
    </lido:subject>
</lido:subjectSet>
```

**Example for "Extent":** Here, the part of the painting to which the indexing term "landscape" refers is recorded as "background" in <[lido:extentSubject](https://lido-schema.org/schema/latest/lido.html#extentSubject)>. Since the landscape is only a secondary theme in this painting, search portals may use this information for ranking, or for explaining the connection in the keyword display.

> **Subject Concept:** landscape ***Preference:*** Display<br />
***Subject Type:*** Description <br/>
**Extent:** background

```xml
<lido:subjectSet>
    <!-- Description -->
    <lido:subject
        lido:type="http://terminology.lido-schema.org/lido00525">
        <lido:extentSubject>
            <skos:Concept
                rdf:about="http://vocab.getty.edu/aat/300056369">
                <skos:prefLabel
                    xml:lang="en">
                    background
                </skos:prefLabel>
            </skos:Concept>
        </lido:extentSubject>
        <lido:subjectConcept>
            <skos:Concept
                rdf:about="http://vocab.getty.edu/aat/300008626">
                <skos:prefLabel
                    xml:lang="en">
                    landscapes (environments)
                </skos:prefLabel>
            </skos:Concept>
            <!-- Display label -->
            <lido:term
                lido:pref="http://terminology.lido-schema.org/lido00526"
                xml:lang="en">
                landscape
            </lido:term>
        </lido:subjectConcept>
    </lido:subject>
<lido:subjectSet>
```

**Example for "Interpretation":** The following is an example for subject type "Interpretation". Even though in this example the smile of the depicted person is immediately evident, a statement about a facial expression is still a subjective judgement and thus an interpretation.

> **Subject Concept:** smile ***Preference:*** Display<br />
 ***Subject Type:*** Interpretation

```xml
<lido:subjectSet>
    <!-- Interpretation -->
    <lido:subject
        lido:type="http://terminology.lido-schema.org/lido00524">
        <lido:subjectConcept>
            <skos:Concept
                rdf:about="http://vocab.getty.edu/aat/300417502">
                <skos:prefLabel
                    xml:lang="en">
                    smiling
                </skos:prefLabel>
            </skos:Concept>
            <!-- Display label-->
            <lido:term
                lido:pref="http://terminology.lido-schema.org/lido00526"
                xml:lang="en">
                smile
            </lido:term>
        </lido:subjectConcept>
    </lido:subject>
 </lido:subjectSet>
```

#### Subject Actor

**Element name:** <[lido:subjectActor](https://lido-schema.org/schema/latest/lido.html#subjectActor)>

**Note:** Subject Actor contains information about a person or a group of persons, depicted in or addressed by an object. Fictitious actors are indexed as <[lido:subjectObject](https://lido-schema.org/schema/latest/lido.html#subjectObject)>.

**Example:** The following example describes Lisa del Giocondo as the person portrayed in Leonardo's *Mona Lisa*. Here, the attributes "[Index label](http://terminology.lido-schema.org/lido00961)" and "[Display label](http://terminology.lido-schema.org/lido00526)" from the LIDO [Preference Vocabulary](http://terminology.lido-schema.org/pref) for <[lido:term](http://lido-schema.org/schema/latest/lido.html#term)> are used to govern how the labels are to be processed. The subject type is "[Identification](http://terminology.lido-schema.org/lido00136)".

> **Subject Actor:** Sitter: Lisa del Giocondo (1479–1542) ***Type:*** Person<br />
***Subject Type:*** Identification<br />
**Name Actor:** Del Giocondo, Lisa ***Preference:*** Index<br />
• Lisa del Giocondo ***Preference:*** Display

```xml
<lido:subjectSet>
    <!-- Identification -->
    <lido:subject
        lido:type="http://terminology.lido-schema.org/lido00136">
        <lido:subjectActor>
            <lido:displayActor
                xml:lang="en">
                Sitter: Lisa del Giocondo (1479–1542)
            </lido:displayActor>
                <!-- Person -->
                <lido:actor
                    lido:type="http://terminology.lido-schema.org/lido00163">
                <lido:actorID
                    lido:type="http://terminology.lido-schema.org/lido00099">
                    http://viaf.org/viaf/50517148
                </lido:actorID>
                <lido:nameActorSet>
                    <!-- Index name -->
                    <lido:appellationValue
                        lido:pref="http://terminology.lido-schema.org/lido00961"
                        xml:lang="en">
                        Del Giocondo, Lisa
                    </lido:appellationValue>
                    <!-- Display name -->
                    <lido:appellationValue
                        lido:pref="http://terminology.lido-schema.org/lido00526"
                        xml:lang="en">
                        Lisa del Giocondo
                    </lido:appellationValue>
                </lido:nameActorSet>
            </lido:actor>
        </lido:subjectActor>
    </lido:subject>
</lido:subjectSet>
```

#### Subject Event

**Element name:** <[lido:subjectEvent](https://lido-schema.org/schema/latest/lido.html#subjectEvent)>

**Note:** Subject Event contains information about a particular event the object depicts or is about.  Iconographical narratives and fictitious events are indexed as <[lido:subjectObject](https://lido-schema.org/schema/latest/lido.html#subjectObject)>.

**Example:** Since there is no event as a topic in Leonardo's *Mona Lisa*, a [Punch cartoon](https://www.deutsche-digitale-bibliothek.de/item/34FHQ3ZRQMVVHPX3W63EGF5Q7I2MJDZ7) has been chosen as an example instead, addressing the theft of the *Mona Lisa* from the Louvre in 1911. The values for the preferred labels in English, German, and French are drawn from [Wikidata](http://www.wikidata.org/entity/Q2727213). Note that the LIDO [Event Type Terminology](http://terminology.lido-schema.org/lido00222) does not provide terms for &lt;lido:subjectEvent&gt;. Rather, the event type should be drawn from an external vocabulary. Many authority files provide a generic concept as a broader term for named events that may be used as values for &lt;lido:eventType&gt;. In the following snippet, a superclass for the "[theft of Mona Lisa](https://www.wikidata.org/wiki/Q61266859)" from Wikidata is used. The subject type is "[Identification](http://terminology.lido-schema.org/lido00136)".

> **Subject Event:** Theft of Mona Lisa <br />
***Subject Type:*** Identification<br/>
**Event Type:** theft ***Preference:*** Preferred

```xml
<lido:subjectSet>
    <!-- Identification -->
    <lido:subject
        lido:type="http://terminology.lido-schema.org/lido00136">
        <lido:subjectEvent>
            <lido:displayEvent
                xml:lang="en">
                Theft of Mona Lisa
            </lido:displayEvent>
            <lido:event>
                <lido:eventID
                    lido:type="http://terminology.lido-schema.org/lido00099">
                    http://www.wikidata.org/entity/Q61266859
                </lido:eventID>
                <lido:eventType>
                    <skos:Concept
                        rdf:about="http://www.wikidata.org/entity/Q2727213">
                        <skos:prefLabel
                            xml:lang="en">
                            theft
                        </skos:prefLabel>
                    </skos:Concept>
                </lido:eventType>
            </lido:event>
        </lido:subjectEvent>
    </lido:subject>
</lido:subjectSet>
```

#### Subject Place

**Element name:** <[lido:subjectPlace](http://lido-schema.org/schema/latest/lido.html#subjectPlace)>

**Note:** Subject Place contains information about a particular place the object depicts or is about. Fictitious places are indexed as <[lido:subjectObject](https://lido-schema.org/schema/latest/lido.html#subjectObject)>

**Example:** The place depicted in Leonardo's Mona Lisa is possibly Bobbio in the Province of Piacenza. Coordinates are taken from Wikidata. The subject type is "[Identification](http://terminology.lido-schema.org/lido00136)".

> **Subject Place:** Possibly Bobbio in the Province of Piacenza<br />
***Subject Type:*** Identification<br />
**Place Name:** Bobbio ***Preference:*** Preferred
**Coordinates:** 44.7715, 9.3864

```xml
<lido:subjectSet>
    <!-- Identification -->
    <lido:subject
        lido:type="http://terminology.lido-schema.org/lido00136">
        <lido:subjectPlace>
            <lido:displayPlace
                xml:lang="en">
                Possibly Bobbio in the Province of Piacenza
            </lido:displayPlace>
            <lido:place>
                <lido:placeID
                    lido:type="http://terminology.lido-schema.org/lido00099">
                    http://www.wikidata.org/entity/Q11498
                </lido:placeID>
                <lido:namePlaceSet>
                <!-- Preferred name -->
                <lido:appellationValue
                    lido:pref="http://terminology.lido-schema.org/lido00169">
                    Bobbio
                </lido:appellationValue>
                <lido:sourceAppellation
                    xml:lang="en">
                    Glori, Carla. Il paesaggio della Gioconda illustrato. 2022/06/09. URL: https://www.researchgate.net/publication/361190634_Il_paesaggio_della_Gioconda_illustrato [2022-11-08]
                </lido:sourceAppellation>
                </lido:namePlaceSet>
                <lido:gml>
                    <gml:Point>
                        <gml:pos>
                            44.7715, 9.3864
                        </gml:pos>
                    </gml:Point>
                </lido:gml>
            </lido:place>
        </lido:subjectPlace>
    </lido:subject>
</lido:subjectSet>
```

#### Subject Object

**Element name:** <[lido:subjectObject](https://lido-schema.org/schema/latest/lido.html#subjectObject)>

**Note:** Subject Object contains information about a particular object depicted in, or referred to, by the object in focus. Fictitious actors, fictitious places, iconographical narratives and fictitious events are indexed as Subject Object as well.

**Example:** Since there is no object as a topic in Leonardo's *Mona Lisa*, a [photograph](http://photothek.khi.fi.it/documents/obj/07703517/07703517%2Ffld0008210x_p) depicting the painting when it was exhibited in the Uffizi 1913 has been chosen as an example instead. The subject type is "[Identification](http://terminology.lido-schema.org/lido00136)".

> **Subject Object:** Mona Lisa<br />
***Subject Type:*** Identification

```xml
<lido:subjectSet>
    <!-- Identification -->
    <lido:subject
        lido:type="http://terminology.lido-schema.org/lido00136">
        <lido:subjectObject>
            <lido:displayObject
                xml:lang="en">
                Mona Lisa
            </lido:displayObject>
            <lido:object>
                <lido:objectID
                    lido:type="http://terminology.lido-schema.org/lido00099">
                    https://collections.louvre.fr/en/ark:/53355/cl010062370
                </lido:objectID>
                <lido:objectNote
                    xml:lang="en">
                    Photograph of the painting exhibited in the Uffizi 1913
                </lido:objectNote>
            </lido:object>
        </lido:subjectObject>
    </lido:subject>
</lido:subjectSet>
```

> For more information on subject indexing see Getty Vocabulary Program, CONA Editorial Rules, [Chapter 3.6.3 Depicted Subject, Iconography Authority](https://www.getty.edu/research/tools/vocabularies/guidelines/cona_3_6_3_subject_authority.html).

## Elements from other namespaces

LIDO "borrows" some definitions from other schemas, following the principle that useful data types and elements defined elsewhere should be reused instead of redefined. All schemas are identified by namespace prefixes which must be declared using the xmlns Attribute, preferably on the outermost element of a LIDO record.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<lido:lido
    xsi:schemaLocation="http://www.lido-schema.org http://lido-schema.org/schema/v1.1/lido-v1.1.xsd"
    xmlns:lido="http://www.lido-schema.org"
    xmlns:gml="http://www.opengis.net/gml"
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#"
    xmlns:xml="http://www.w3.org/XML/1998/namespace"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
```

### Geographic locations (gml)

**Namespace:** <http://www.opengis.net/gml>

**Origin:** Open Geospatial Consortium (OGC)

**Elements used:** gml:Point, gml:LineString, gml:Polygon

GML is a huge XML schema widely used in geographic information system (GIS) applications. LIDO reuses three elements from GML for representing location coordinates wrapped up in the <[lido:gml](https://lido-schema.org/schema/latest/lido.html#gml)> element. &lt;lido:gml&gt; is available in all LIDO elements derived from [lido:placeComplexType](https://lido-schema.org/schema/latest/lido.html#placeComplexType). These are:

- [lido:partOfPlace](http://www.lido-schema.org/schema/v1.1/lido-v1.1.html#partOfPlace)
- [lido:place](http://www.lido-schema.org/schema/v1.1/lido-v1.1.html#place)
- [lido:repositoryLocation](http://www.lido-schema.org/schema/v1.1/lido-v1.1.html#repositoryLocation)
- [lido:vitalPlaceActor](http://www.lido-schema.org/schema/v1.1/lido-v1.1.html#vitalPlaceActor)

The &lt;lido:gml&gt; element is structured as follows:

![Figure 10: Sub-elements of lido:gml](img/figure_gml.png)

All three GML elements can have an attribute, *srsName*, for the [spatial reference system](https://spatialreference.org/) (SRS) to which the coordinate values apply. If the *srsName* attribute is omitted, then the [World Geodetic System 1984](https://spatialreference.org/ref/epsg/wgs-84/) (WGS 84, also known as EPSG 4326) coordinate system is assumed. Thus the *srsName* attribute is only required in the rare case where a location given in historical coordinates has not been translated to WGS 84.

The most common GML sub-element will be *gml:point* for a single coordinate pair. Here, coordinates are written as decimal degrees with negative values for western and southern hemispheres, respectively. The format is latitude, followed by a space, followed by longitude. For example, &lt;gml:pos&gt;-34.6 -58.38&lt;/gml:pos&gt; is the approximate location of Buenos Aires, Argentina, and &lt;gml:pos&gt;43.78 11.25&lt;/gml:pos&gt; encodes the location Florence, Italy.

Validating parsers will check the syntax of all GML elements within a LIDO record using the GML XML schema included via an *xs:import* directive.

### Concepts (skos)

**Namespace:** <http://www.w3.org/2004/02/skos/core>#

**Origin:** W3C Semantic Web Deployment Working Group

**Element used:** skos:Concept

The [Simple Knowledge Organization System](https://www.w3.org/2004/02/skos/) (SKOS) is a data model for representing controlled vocabularies in the [Resource Description Framework](https://www.w3.org/RDF/) (RDF). SKOS has evolved to the de-facto standard for authority data in the Linked Data sphere. Many important controlled vocabularies are published in SKOS, for instance, the [Art & Architecture Thesaurus](https://www.getty.edu/research/tools/vocabularies/aat/index.html) (AAT) or the [Library of Congress Subject Headings](https://id.loc.gov/authorities/subjects.html) (LCSH).

The SKOS class &lt;skos:Concept&gt; is introduced in LIDO v1.1 to represent LOD vocabularies. All LIDO elements for which a <[lido:conceptID]> element is defined, can preferably use the SKOS class &lt;skos:Concept&gt; together with allowed SKOS properties of this class represented in RDF/XML syntax.

Following this syntax, &lt;skos:Concept&gt; becomes an XML element with the attribute 'rdf:about' for the URI of the Linked Data resource to be referenced. In its most basic form, &lt;skos:Concept&gt; could be used as follows in LIDO v1.1, referencing the AAT concept '[oil paintings](http://vocab.getty.edu/aat/300033799)':

```xml
<skos:Concept rdf:about="http://vocab.getty.edu/aat/300033799"/>
```

which is equivalent to

```xml
<lido:conceptID
    type="http://terminology.lido-schema.org/lido00099">
    http://vocab.getty.edu/aat/300033799
</lido:conceptID>
```

The advantage of using &lt;skos:Concept&gt; instead of &lt;lido:conceptID&gt; becomes evident when further information from or about the Linked Data resource is added to the concept reference, for example:

```xml
<skos:Concept
    rdf:about="http://vocab.getty.edu/aat/300033656">
    <skos:prefLabel
        xml:lang="en">
        panel paintings
    </skos:prefLabel>
    <skos:prefLabel
        xml:lang="de">
        Tafelbild (Gemälde)
    </skos:prefLabel>
    <skos:exactMatch 
        rdf:resource="http://www.wikidata.org/entity/Q55439"/>
    <skos:exactMatch 
        rdf:resource="https://d-nb.info/gnd/4037220-0"/>
</skos:Concept>
```

This gives users and consumers of the LIDO record several clues as to how the concept can be represented in a search index or in a multilingual display, and where contextual information can be obtained from other Linked Data sources. Moreover, when the object at hand is described with several concepts from different vocabularies using <[lido:conceptID](https://lido-schema.org/schema/latest/lido.html#conceptID)>, then it is impossible to tell if two or more identifiers relate to similar concepts and which term relates to which identifier. Using a mapping relation from SKOS prevents such possible confusion. In the example above, the concept URIs from Wikidata and GND are explicitly stated as equivalent to the AAT concept. This greatly facilitates the alignment of indexing vocabulary in cases where an aggregator receives LIDO records using different preferred vocabularies. Using SKOS label properties also obviates the need for <[lido:term](https://lido-schema.org/schema/latest/lido.html#term)> elements in cases where portals or aggregators are not prepared to collect term data from Linked Data URIs.

Usually, such augmented SKOS concept statements will not be constructed manually, but by software components that produce LIDO records. &lt;lido:conceptID&gt; is still appropriate for indexing terms with local identifiers from vocabularies not published as LOD.

The LIDO XML schema only demands that no other foreign namespace besides SKOS is used within a concept element (i.e., one that is derived from [conceptComplexType](https://lido-schema.org/schema/latest/lido.html#conceptComplexType) and [conceptMixedComplexType](https://lido-schema.org/schema/latest/lido.html#conceptMixedComplexType)). Proper usage of the SKOS namespace must therefore be checked using the given Schematron rules or equivalent tools.

> For more detailed information about the usage of &lt;skos:Concept&gt; and allowed properties, see [*4.2.3 Display and index elements*](#display-and-index-elements) in this document.

### Individuals (owl)

**Namespace:** <http://www.w3.org/2002/07/owl>#

**Origin:** W3C OWL Working Group

**Element used:** owl:sameAs

Individual entities such as persons, organizations, places, objects or named events can be described in more than one authority file, meaning that there can be more than one URI for referencing such an entity. When the content of LIDO records is processed or presented to users, it can be useful to choose among alternative Linked Data sources or to collect complementary information from several sources.

The [Web Ontology Language](https://www.w3.org/TR/owl2-overview/) (OWL) defines a property, '[owl:sameAs](https://www.w3.org/TR/owl-ref/#sameAs-def)', for stating that two identifiers refer to the same individual in the real world. LIDO, from version 1.1, adopts the &lt;owl:sameAs&gt; statement for use in elements defined by the following complex types in LIDO:

- [actor](https://lido-schema.org/schema/latest/lido.html#actorComplexType)
- [event](https://lido-schema.org/schema/latest/lido.html#eventComplexType)
- [legal body](http://www.lido-schema.org/schema/latest/lido.html#legalBodyRefComplexType)
- [object](https://lido-schema.org/schema/latest/lido.html#objectComplexType)
- [place](https://lido-schema.org/schema/latest/lido.html#placeComplexType)

As with the SKOS namespace described above, the OWL namespace identifies an RDF data model. 'owl:sameAs' is therefore an RDF property represented in XML using  RDF/XML syntax. The LIDO schema only demands that identity statements in the above elements are from the OWL namespace. Proper use of this namespace (i.e., only the &lt;owl:sameAs&gt; property is permitted in LIDO and it can only contain a URI) must therefore be checked using the given Schematron rules or equivalent tools.

## Attributes

Many LIDO elements can be provided with attributes. An attribute is an XML construct used for qualifying or typifying the contents of an element. The LIDO schema determines which elements can have attributes and which attribute (or attributes) an element is permitted or required to hold. Each attribute appears as a pair of attribute name and attribute value (known as a key-value pair). Many attributes are used for several different elements in LIDO. For many of the LIDO attributes there are recommended value lists provided by the LIDO Terminology. This particularly applies to the [*lido:type*](http://lido-schema.org/schema/latest/lido.html#type) attribute.

Fourteen attributes are defined in the LIDO Schema Version 1.1 namespace. In addition, the language attribute [*xml:lang*](https://www.w3.org/TR/REC-xml/#sec-lang-tag) from the XML namespace can be used for all text elements and is mandatory for the elements <[lido:administrativeMetadata](https://lido-schema.org/schema/latest/lido.html#administrativeMetadata)> and <[lido:descriptiveMetadata](https://lido-schema.org/schema/latest/lido.html#descriptiveMetadata)>.

### @type

**The type attribute** is of particular importance. The [LIDO Terminology](#content-lido-terminology) contains LIDO Vocabularies for some type attributes, depending on the element in which the attribute occurs:

- [Actor Type-Vocabulary](http://terminology.lido-schema.org/actor_type)
- [Classification Type-Vocabulary](http://terminology.lido-schema.org/classification_type)
- [Earliest Date Type-Vocabulary](http://terminology.lido-schema.org/earliestDate_type)
- [Event Place Type-Vocabulary](http://terminology.lido-schema.org/eventPlace_type)
- [Gender Actor Type-Vocabulary](http://terminology.lido-schema.org/genderActor_type)
- [Identifier Type-Vocabulary](http://terminology.lido-schema.org/identifier_type)
- [Latest Date Type-Vocabulary](http://terminology.lido-schema.org/latestDate_type)
- [Nationality Actor Type-Vocabulary](http://terminology.lido-schema.org/nationalityActor_type)
- [Object Description Set Type-Vocabulary](http://terminology.lido-schema.org/objectDescriptionSet_type)
- [Object Measurements Set Type-Vocabulary](http://terminology.lido-schema.org/objectMeasurementsSet_type)
- [Object/Work Type Type-Vocabulary](http://terminology.lido-schema.org/objectWorkType_type)
- [Period Name Type-Vocabulary](http://terminology.lido-schema.org/periodName_type)
- [Record Info Set Type-Vocabulary](http://terminology.lido-schema.org/recordInfoSet_type)
- [Record Metadata Date Type-Vocabulary](http://terminology.lido-schema.org/recordMetadataDate_type)
- [Repository Set Type-Vocabulary](http://terminology.lido-schema.org/repositorySet_type)
- [Repository Work ID Type-Vocabulary](http://terminology.lido-schema.org/workID_type)
- [Resource Representation Type-Vocabulary](http://terminology.lido-schema.org/resourceRepresentation_type)
- [Rights Type Type-Vocabulary](http://terminology.lido-schema.org/rightsType_type)
- [Term Materials/Techniques Type-Vocabulary](http://terminology.lido-schema.org/termMaterialsTech_type)
- [Vital Dates Actor Type-Vocabulary](http://terminology.lido-schema.org/vitalDatesActor_type)
- [Vital Place Actor Type-Vocabulary](http://terminology.lido-schema.org/vitalPlaceActor_type)


### @addedSearchTerm

**Attribute name:** [lido:addedSearchTerm](https://lido-schema.org/schema/latest/lido.html#addedSearchTerm)

**Note:** Boolean attribute of the <[lido:term](https://lido-schema.org/schema/latest/lido.html#term)> element indicating that the term has been added to enhance retrieval when set to "yes" ("no" is default). The additional term may be a synonym, a broader term, or an equivalent term in a further language, taken from a local or published controlled vocabulary.

Possible use cases for adding a term are: The index term from a linked data vocabulary (a) does not provide a (correct) term in the desired language; (b) does not include a synonym considered useful as search entry by the data provider; (c) is not expected to be fully exploited with regard to its semantic relationships by the portal, so that adding a broader term is useful for expanding the search result.

**Example 1:** The following XML code snippet is an example for use case (a): The [AAT descriptor](http://vocab.getty.edu/page/aat/300015050) provides an incorrect German equivalent, 'Ölgemälde'.

 ```xml
 <!-- Medium -->
<lido:termMaterialsTech
    lido:type="http://terminology.lido-schema.org/lido00513">
    <skos:Concept
        rdf:about="http://vocab.getty.edu/aat/300015050">
        <skos:prefLabel
            xml:lang="en">
            oil paint (paint)
        </skos:prefLabel>
        <!-- The following label is an incorrect German translation -->
        <skos:prefLabel
            xml:lang="de">
            Ölgemälde
        </skos:prefLabel>
    </skos:Concept>
    <lido:term
        lido:addedSearchTerm="yes"
        xml:lang="de">
        Ölfarbe
    </lido:term>
</lido:termMaterialsTech>
 ```

**Example 2:** The following XML code snippet is an example for use case (b): The AAT concept is represented by two terms, 'smiling' and 'smiled'. The term 'smile' which is likely to be searched for, is not provided and therefore added as a search term.

 ```xml
 <lido:subjectConcept>
    <skos:Concept
        rdf:about="http://vocab.getty.edu/aat/300417502">
        <skos:prefLabel
            xml:lang="en">
            smiling
        </skos:prefLabel>
        <skos:altLabel
            xml:lang="en">
            smiled
        </skos:altLabel>
        <skos:exactMatch 
            rdf:resource="http://www.wikidata.org/entity/Q487"/>
        <skos:exactMatch 
            rdf:resource="https://d-nb.info/gnd/4034011-9"/>
    </skos:Concept>
    <lido:term
        lido:addedSearchTerm="yes"
        xml:lang="en">
        smile
    </lido:term>
    <lido:term
        lido:addedSearchTerm="yes"
        xml:lang="de">
        Lächeln
    </lido:term>
</lido:subjectConcept>
 ```

### @codecResource

**Attribute name:** [lido:codecResource](http://lido-schema.org/schema/latest/lido.html#codecResource)

**Note:** Attribute for <[lido:linkResource](https://lido-schema.org/schema/latest/lido.html#linkResource)> and <[lido:resourceSet](https://lido-schema.org/schema/latest/lido.html#resourceSet)> to indicate that a particular codec is required for rendering the resource. Values and URIs for codec names can be found in the [Open Metadata Registry](http://metadataregistry.org/uri/Codecs).

**Example:** The following XML code snippet indicates that the linked resource is a provided audio to be rendered as digital audio encoded according to a specification officially named MPEG Audio Version 1 Layer 3 (colloquially referred to as MP3). The resource is an '[audio excerpt](https://www.deutsche-digitale-bibliothek.de/item/CW4ACDL2YTHT7YP2BKIAO56KM66PKPBQ)' from the opera *[Mona Lisa](https://en.wikipedia.org/wiki/Mona_Lisa_(opera))*, provided by the German Digital Library.

```xml
<lido:resourceRepresentation
    lido:type="http://terminology.lido-schema.org/lido00465">
    <lido:linkResource
        lido:codecResource="MPEG Audio Version 1 Layer 3"
        lido:formatResource="audio/mpeg">
        http://media.slub-dresden.de/fon/snp/a/012100/fon_snp_a_012100_01.mp3
    </lido:linkResource>
</lido:resourceRepresentation>
```

### @encodingAnalog

**Attribute name:** [lido:encodinganalog](http://lido-schema.org/schema/latest/lido.html#encodinganalog)

**Note:** Attribute for many LIDO elements to record the field label in the source database from which the element content was migrated. The source format for the whole document is indicated in the attribute [*lido:relatedencoding*](http://lido-schema.org/schema/latest/lido.html#relatedencoding) of the [LIDO Wrapper](http://lido-schema.org/schema/latest/lido.html#lidoWrap).

**Example:** The following XML code snippet contains the popular French title for *Mona Lisa*, "La Joconde", stored in the source database as "titre d'usage" under the field label "Type de titre". The title type is indexed with the AAT descriptor '[popular title](http://vocab.getty.edu/page/aat/300417200)'.

```xml
<lido:titleSet
    lido:type="http://vocab.getty.edu/aat/300417200">
    <lido:appellationValue
        lido:pref="http://terminology.lido-schema.org/lido00170"
        lido:encodinganalog="Type de titre"
        lido:label="titre d'usage"
        xml:lang="fr">
        La Joconde
    </lido:appellationValue>
    <lido:sourceAppellation>
        "Institut national d'histoire de l'art, Portrait de Lisa Gherardini. URL: https://agorha.inha.fr/ark:/54721/8e279ee1-f842-4eb3-994d-797ca8562c09
    </lido:sourceAppellation>
</lido:titleSet>
```

### @formatResource

**Attribute name:** [lido:formatResource](http://lido-schema.org/schema/latest/lido.html#formatResource)

**Note:** Attribute for elements referring to the internet media type of a web resource, i.e., <[lido:legalBodyWeblink](http://lido-schema.org/schema/latest/lido.html#legalBodyWeblink)>, <[lido:linkResource](http://lido-schema.org/schema/latest/lido.html#linkResource)>, <[lido:objectWebResource](http://lido-schema.org/schema/latest/lido.html#objectWebResource)>, and <[lido:recordInfoLink](http://lido-schema.org/schema/latest/lido.html#recordInfoLink)>. It is recommended to use the [IANA Media Types](https://www.iana.org/assignments/media-types/media-types.xhtml) as values.

**Example:** The following XML code snippet marks the format of the Web resource for a study of the *Mona Lisa* as "text/html" according to the IANA Media Type.

```xml
<lido:object>
    <lido:objectWebResource
        lido:formatResource="text/html"
        xml:lang="de">
        http://foto.biblhertz.it/obj08012138
    </lido:objectWebResource>
</lido:object>
```

### @geographicalEntity

**Attribute name:** [lido:geographicalEntity](http://lido-schema.org/schema/latest/lido.html#geographicalEntity)

**Note:** Attribute for <[lido:partOfPlace](http://lido-schema.org/schema/latest/lido.html#partOfPlace)>, <[lido:place](http://lido-schema.org/schema/latest/lido.html#place)>, <[lido:repositoryLocation](http://lido-schema.org/schema/latest/lido.html#repositoryLocation)>, and <[lido:vitalPlaceActor](http://lido-schema.org/schema/latest/lido.html#vitalPlaceActor)> to indicate the kind of place as a physical entity, such as a *forest* or a *river*. This attribute was intended in LIDO v1.0 to qualify the type of the given place entity including natural environment and landscape. In LIDO v1.1 these values can be expressed in greater detail through <[lido:placeClassification](https://lido-schema.org/schema/latest/lido.html#placeClassification)>. Note that Place Classification in LIDO corresponds to '[Place Type](https://www.getty.edu/research/tools/vocabularies/guidelines/tgn_3_6_place_types.html)' in the [Getty Thesaurus of Geographic Names](https://www.getty.edu/research/tools/vocabularies/tgn/index.html).
It is recommended to use a URI to refer to the kind of geographical entity.

**Example:** For an analogous example see 'lido:politicalEntity' below in this list.

> For more information see the note for '[geographicalEntity](http://lido-schema.org/documents/terminology-recommendation.html#geographicalEntity)' in the LIDO Terminology Recommendation.

### @label

**Attribute name:** [lido:label](http://lido-schema.org/schema/latest/lido.html#label)

**Note:** Attribute for many LIDO elements to indicate how the data element from which the data were migrated is labelled at the user interface. Note that this can be different from how the element is named in the source database (see [*lido:encodinganalog*](https://lido-schema.org/schema/latest/lido.html#encodinganalog) for this case).

**Example:** The following XML code snippet marks the label for the "Historique" note displayed at the user interface of the [PORTRAIT DE MONA LISA (1479-1528)](https://www.pop.culture.gouv.fr/notice/joconde/000PE025604).

```xml
<lido:descriptiveNoteValue
    lido:label="Historique"
    xml:lang="fr">
    Commandé par le florentin Francesco del Giocondo, époux de Mona Lisa entre 1503 et 1506
</lido:descriptiveNoteValue>
```

### @measurementsGroup

**Attribute name:** [lido:measurementsGroup](http://lido-schema.org/schema/latest/lido.html#measurementsGroup)

**Note:** Attribute for <[lido:eventObjectMeasurements](http://lido-schema.org/schema/latest/lido.html#eventObjectMeasurements)> and <[lido:objectMeasurementsSet](http://lido-schema.org/schema/latest/lido.html#objectMeasurementsSet)> to indicate the kind of measurements given in multiple <[lido:measurementsSet](https://lido-schema.org/schema/latest/lido.html#measurementsSet)> elements. This attribute is intended to be used in application profiles.

**Example:** The following XML code snippet shows how the "[Measurement object requirement](http://terminology.lido-schema.org/lido00923)" concerning temperature conditions is recorded.

```xml
<!-- Measurement object requirement for @measurementsGroup "temperature" -->
<lido:eventObjectMeasurements
    lido:type="http://terminology.lido-schema.org/lido00923"
    lido:measurementsGroup="http://vocab.getty.edu/aat/300056066">
    <lido:displayObjectMeasurements>
        16-25°C ± 1.5°C over 24 hours if the object is installed in a room
        equipped with an operating Heating Ventilation and Air-Conditioning
        (HVAC) system
    </lido:displayObjectMeasurements>
```

### @mostNotableEvent

**Attribute name:** [lido:mostNotableEvent](https://lido-schema.org/schema/latest/lido.html#mostNotableEvent)

**Note:** Attribute for <[lido:eventSet](https://lido-schema.org/schema/latest/lido.html#eventSet)> and <[lido:eventWrap](https://lido-schema.org/schema/latest/lido.html#eventWrap)> to indicate that the event in focus is considered to be the most notable or significant event among others recorded for the object.

**Example:** The following XML code snippet declares the event "[Production](http://terminology.lido-schema.org/lido00007)" as the most notable one.

```xml
<lido:eventSet
    lido:sortorder="1"
    lido:mostNotableEvent="1">
    <lido:event>
        <lido:eventType>
            <skos:Concept
                rdf:about="http://terminology.lido-schema.org/lido00007">
                <skos:prefLabel
                    xml:lang="en">
                    Production
                </skos:prefLabel>
                <skos:prefLabel
                    xml:lang="de">
                    Herstellung
                </skos:prefLabel>
            </skos:Concept>
         </lido:eventType>
    <lido:event>
</lido:eventSet>
```

### @politicalEntity

**Attribute name:** [lido:politicalEntity](http://lido-schema.org/schema/latest/lido.html#politicalEntity)

**Note:** Attribute for <[lido:partOfPlace](http://lido-schema.org/schema/latest/lido.html#partOfPlace)>, <[lido:place](http://lido-schema.org/schema/latest/lido.html#place)>, <[lido:repositoryLocation](http://lido-schema.org/schema/latest/lido.html#repositoryLocation)>, and <[lido:vitalPlaceActor](http://lido-schema.org/schema/latest/lido.html#vitalPlaceActor)> to indicate the kind of place as an administrative, political entity. This attribute was intended in LIDO v1.0  to qualify the type of the given place entity according to political structures, including values such as *country* or *city*. In LIDO v1.1 these values can be expressed in greater detail through <[lido:placeClassification](https://lido-schema.org/schema/latest/lido.html#placeClassification)>. Note that Place Classification in LIDO corresponds to '[Place Type](https://www.getty.edu/research/tools/vocabularies/guidelines/tgn_3_6_place_types.html)' in the [Getty Thesaurus of Geographic Names](https://www.getty.edu/research/tools/vocabularies/tgn/index.html).
It is recommended to use a URI to refer to the kind of political entity.

 **Example:** The following XML code snippet shows the index entry for "[France](http://www.wikidata.org/entity/Q142)", qualified as a "[country](http://www.wikidata.org/entity/Q6256)" by the value for the political entity attribute.

 ```xml
<lido:partOfPlace
    lido:politicalEntity="http://www.wikidata.org/entity/Q6256">
    <lido:placeID
        lido:type="http://terminology.lido-schema.org/lido00099">
        http://www.wikidata.org/entity/Q20861
    </lido:placeID>
</lido:partOfPlace>
 ```

 > For more information, see the note for '[politicalEntity](http://lido-schema.org/documents/terminology-recommendation.html#politicalEntity)' in the LIDO Terminology Recommendation.

### @pref

**Attribute name:** [lido:pref](http://lido-schema.org/schema/latest/lido.html#pref)

**Note:** Attribute for text elements like <[lido:term](https://lido-schema.org/schema/latest/lido.html#term)> or appellations, as well as identifier elements, to indicate whether, e.g., the value is a preferred or an alternative one, or should be used as a search entry only. The [LIDO Preference Vocabulary](http://terminology.lido-schema.org/pref) contains a value list for the pref attribute. For concept terms and names of individual entities a "[Preferred label](http://terminology.lido-schema.org/lido00169)" (one and only one per language), always has to be provided. If only one value is supplied, this will be chosen as the preferred one. To determine how a value should be displayed in a portal, the preference role "[Display label](http://terminology.lido-schema.org/lido00526)" may be used.

**Example:** The following XML code snippet represents the concept with indexing terms as defined in the SKOS namespace. In order to predefine a label for display, the value "Display label" is used for the 'lido:pref' attribute.

 ```xml
 <lido:roleActor>
    <skos:Concept
        rdf:about="http://vocab.getty.edu/aat/300025136">
        <skos:prefLabel
            xml:lang="en">
            painters (artists)
        </skos:prefLabel>
        <skos:prefLabel
            xml:lang="de">
            Maler
        </skos:prefLabel>
    </skos:Concept>
    <!-- Display label-->
    <lido:term
        lido:pref="http://terminology.lido-schema.org/lido00526"
        xml:lang="en">
        Painter
    </lido:term>
    <lido:term
        lido:pref="http://terminology.lido-schema.org/lido00526"
        xml:lang="de">
        Maler
    </lido:term>
</lido:roleActor>
 ```

### @relatedEncoding

**Attribute name:** [lido:relatedEncoding](http://lido-schema.org/schema/latest/lido.html#relatedEncoding)

**Note:** Attribute held by the <[lido:lidoWrap](https://lido-schema.org/schema/latest/lido.html#lidoWrap)> and <[lido:lido](https://lido-schema.org/schema/latest/lido.html#lido)> elements to indicate the format of the whole data source from which the data were migrated. For individual elements with data values the corresponding source data fields can be indicated using the attributes [lido:encodinganalog](https://lido-schema.org/schema/latest/lido.html#encodinganalog) and [lido:label](https://lido-schema.org/schema/latest/lido.html#label).

### @sortorder

**Attribute name:** [lido:sortorder](http://lido-schema.org/schema/latest/lido.html#sortorder)

**Note:** Attribute held by many LIDO elements to suggest a sequential ordering among sibling elements for online presentation.

**Example:** The following XML code snippet marks the "[Aquisition](http://terminology.lido-schema.org/lido00001)" of the object as the second event in the list of events the object is involved in.

```xml
<lido:eventSet
    lido:sortorder="2">
    <lido:event>
        <lido:eventType>
            <skos:Concept
                rdf:about="http://terminology.lido-schema.org/lido00001">
                <skos:prefLabel
                    xml:lang="en">
                    Acquisition
                </skos:prefLabel>
                <skos:prefLabel
                    xml:lang="de">
                    Erwerb
                </skos:prefLabel>
            </skos:Concept>
         </lido:eventType>
    <lido:event>
</lido:eventSet>
```

### @source

**Attribute name:** [lido:source](http://lido-schema.org/schema/latest/lido.html#source)

**Note:** Attribute held by identifier and date elements to describe the source of the information given in the holding element.

**Example:** The following XML code snippet shows the source for the estimated earliest date of the creation of the Mona Lisa by Leonardo da Vinci.

```xml
<lido:earliestDate
    lido:source="Collections des musées de France (Joconde), Portait de Mona Lisa. URL: https://www.pop.culture.gouv.fr/notice/joconde/000PE025604"
    lido:type="http://terminology.lido-schema.org/lido00529">
    1503
<lido:earliestDate>
```
