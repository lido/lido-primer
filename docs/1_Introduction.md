
# About this document

## Scope

The LIDO Primer is a user guide for those who wish to represent information describing objects or works of material culture and natural heritage with LIDO. Described objects may be human-made art works or utilitarian objects, as well as natural specimens or archeological remains. It is intended for anyone concerned with producing, maintaining, transforming, or publishing metadata for cultural heritage collections, not only limited to museums, but also including, for example, libraries and archives. The Primer complements the more technical documents listed below by giving background information and introductory examples for applying the LIDO schema in practice.

Throughout this Primer, the painting *Mona Lisa* by Leonardo da Vinci serves as an example of an object description in LIDO Version 1.1. XML snippets are provided to show the usage of LIDO elements, attributes, and recommended terminology. Besides, the *Mona Lisa* also features in the Europeana Data Model Primer ([EDM Primer](https://pro.europeana.eu/files/Europeana_Professional/Share_your_data/Technical_requirements/EDM_Documentation/EDM_Primer_130714.pdf)) to illustrate the main features of the model. The example in LIDO intends to demonstrate similarities between its XML-based model and the RDF graph resulting from application of the EDM.

## Associated documents

- The **LIDO XML Schema Definition** is the authoritative reference for all LIDO elements and attributes. The latest version can be found here: <http://lido-schema.org/schema/latest/lido.xsd>
- The **LIDO Documentation** provides a concise description of all LIDO elements and attributes as well as detailed structural and technical information. It is recommended to consult the documentation for a systematic description of all LIDO elements and attributes. The latest version can be found here: <http://lido-schema.org/schema/latest/lido.html>
- The **LIDO Terminology Recommendation** provides references to recommended terms from the LIDO Terminology or other linked open vocabularies for a number of LIDO elements and attributes. The latest version can be found here: <http://lido-schema.org/documents/terminology-recommendation.html>
- The **LIDO Terminology** is publicly accessible via <http://terminology.lido-schema.org>. A short introduction and a documentation of the LIDO Terminology LOD interface is given here: <https://cidoc.mini.icom.museum/working-groups/lido/lido-overview/lido-terminology/>
- **LIDO Examples** can be found here: [LIDO Examples](https://lido-schema.org/examples)
- The **LIDO website** provides further information about LIDO and other resources: <https://lido-schema.org>
