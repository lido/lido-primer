
# Design principles

## Basic considerations

The design of LIDO was guided by the following considerations:

- **Reuse existing standards**
Preexisting models for representing cultural heritage metadata contain assumptions that are still valid today. These should be preserved and re-used in the design of LIDO. For an overview of preexisting models see section [*2.3 Background*](#background).
- **Use proven technologies**
Metadata needs to be processed in a wide range of system environments. Choosing proprietary or poorly supported technologies can result in barriers and unnecessary costs. For a brief summary of the XML family of technologies adopted for LIDO see section [*3.2 Choice of technologies*](#choice-of-technologies).
- **Facilitate interoperability**
Metadata is moved, transformed, distributed, and interconnected at an increasing rate. LIDO should therefore facilitate and encourage higher levels of harmonization than in the past. Next to technical and organizational considerations described in section [*3.3 Supporting Web principles*](#supporting-web-principles), mappings between LIDO and standards as well as models from other domains are essential tools for enabling and maintaining such harmonization.
- **Enable adaptability**
The information landscape is changing continuously, bringing up new requirements that need to be addressed without invalidating existing data records. Not all future requirements can be anticipated. Well-defined extension points should be provided as parts of the model. Moreover, cultural heritage metadata will continue to be produced in varying degrees of detail and granularity. The model should therefore be amenable to object descriptions of widely different richness by allowing for the introduction of application-specific restrictions (see section [*3.4 Enabling adaptability*](#enabling-adaptability)).

## Choice of technologies

### An XML schema for LIDO

The [Exensible Markup Language (XML)](https://www.w3.org/TR/xml/) has evolved from a mere markup language for structuring machine-processable text into a major technological ecosystem with a wide range of auxiliary specifications and tools. This long-term evolution justifies the assumption that XML will not become obsolete in the foreseeable future. Nevertheless, it is fair to say that JSON ([JavaScript Object Notation](https://www.json.org/json-en.html)) is emerging as a contender in many areas where use of XML is currently prevalent. Moreover, the Linked Data paradigm, briefly described in chapter [*3.3 Supporting Web principles*](#supporting-web-principles), presents a challenge due to the core differences between the XML and RDF data models. Version 1.1 of the LIDO schema is an attempt at reconciling some of these differences.

Given the maturity of XML and the broad support in the information processing sphere, it is reasonable to expect that this technology is, and will remain, a solid and reliable platform for the LIDO metadata schema.

XML itself defines the format of markup elements (often referred to as tags) and how these can be nested to form a tree-like structure. Any XML document that obeys the basic element syntax and has matching start and end tags is said to be well-formed.

In order to allow for predictable processing of the document content, the order and occurrence of markup elements is typically restricted by a schema (sometimes referred to as the document grammar). At least three languages (known as <abbr title="Document Type Definition">DTD</abbr>, [RELAX NG](https://relaxng.org/), and XSD) are available for writing XML schema definitions. The LIDO schema is written in the [XML Schema Definition (XSD)](https://www.w3.org/TR/xmlschema-1/)  language, which can be considered the most expressive of these three.

The core specification for LIDO is laid down in an [XSD document](http://www.lido-schema.org/schema/latest/lido.xsd). This schema document can be used by software tools known as validating parsers in order to verify if a LIDO record complies with all rules and restrictions given in the schema. Such validation is essential wherever the LIDO record is to be processed for usage in databases, web portals, or in transformation chains as used in large-scale aggregation portals.

### Schema design

The XSD language provides some mechanisms for *object-oriented* design. This means that schema constructs can be defined on an abstract level, acting as types or templates for actual element or attribute definitions. The LIDO schema definition makes extensive use of type declarations for elements composed of more than one element, or of simple data types with attributes. In the XSD language, these type declarations are referred to as complex types. In fact, the entire LIDO record structure is first modeled on the abstract level of complex types, before declaring the derived elements that are actually used in a LIDO record. This design principle offers a clear distinction between the model and its implementation.

The basic structure of LIDO is determined by the XML schema specification which defines the building blocks of a LIDO XML document:

- the elements and attributes that may occur in a LIDO document,
- the allowed data types for values of elements and attributes,
- the number and order of child elements is declared in [*complex types*](https://www.w3schools.com/xml/el_complextype.asp).

### Namespaces

As explained in section [*2.3 Background*](#background), reuse of existing specifications is an important objective in the design of the LIDO schema. XML has mechanisms for combining definitions from more than one schema. This allows a schema definition to "borrow" element or attribute definitions from other schemas, typically using a mechanism known as *namespaces*.

Declaring a namespace for LIDO allows to distinguish between elements or attributes that are part of the LIDO data model, and others that are defined elsewhere. Namespaces determine the scope and origin of an element or attribute name. In other words, a namespace indicates who is responsible for the definition of a particular element or attribute name and where XML processing software can find the corresponding schema. Namespaces also allow to distinguish between elements or attributes that carry identical names. As an example, the meaning of *rdf:type* is different from that of *lido:type*.

A combination of namespace and name is referred to as *qualified name* (a QName in XML terminology). QNames can be written either in full or in an abbreviated form:

```xml
<http://www.lido-schema.org/titleSet>
```

is identical to

```xml
<lido:titleSet xmns:lido="http://www.lido-schema.org">
```

In practice, the abbreviated namespace (in this case, "lido") is only declared once in the outermost element of an XML document. This allows all subsequent elements and attributes from the LIDO namespace to be written with what is known as a *namespace prefix*, followed by a colon:

```xml
<lido:lidoWrap  xmns:lido="http://www.lido-schema.org" (...)  >
	(...)
		<lido:titleSet>
	(...)
```

LIDO XML records prepared using the Schema Version 1.1 can (and should) include the following prefix declarations in their outermost element.

**Table 1: Prefix declarations**

| Namespace URI                                                        | Prefix  | Description |
| ------------------------------------------------------------------| -------------------------------------------------------- | ------------------------------------------------------------ |
| <http://www.lido-schema.org>    | lido | The LIDO XML schema. A particular version can be selected with the *xsi:schemaLocation attribute*. |
| <http://www.opengis.net/gml>    | gml  | The Geography Markup Language (GML) schema, used for location coordinates.|
| <http://www.w3.org/2002/07/owl>#    | owl | The Web Ontology Language (OWL) namespace, used for the *sameAs* identity statement. |
| <http://www.w3.org/1999/02/22-rdf-syntax-ns>#  | rdf | The RDF namespace, used within elements from the SKOS namespace. |
| <http://www.w3.org/2004/02/skos/core># | skos | The SKOS namespace, used for statements about linked vocabulary data. |
| <http://www.w3.org/XML/1998/namespace>              |xml | The XML namespace, used for the language (*xml:lang*) attribute. |
| <http://www.w3.org/2001/XMLSchema-instance> | xsi | An XML utility namespace, used for retrieving schema declarations in cases where the namespace URI does not resolve to the desired schema version. |

## Supporting Web principles

The Web as a platform for cultural heritage information has seen a number of changes since the introduction of LIDO v1.0. Arguably the most significant developments are concerned with openness and interconnection of published knowledge. In LIDO Schema Version 1.1 new constructs were introduced, primarily focussing on requirements from two areas: the [FAIR Principles](https://www.go-fair.org/) and the [Linked Data Platform Guidelines](https://www.w3.org/TR/ldp-bp/).

### FAIR Principles

The FAIR Principles, first published in 2016, are guidelines provided by the research data community to improve the computability for finding, accessing, interoperating, and reusing data. The principles may also serve as a checklist for funding agencies. LIDO is well suited to play a key role in implementing FAIR principles in the domain of cultural heritage metadata. Four main principles are defined that should guide the preparation of data and metadata, in particular (identifiers in square brackets refer to the corresponding topics from the FAIR guidelines):

**F – Findable:** LIDO mandates the use of publishable identifiers both for the object [[F3](https://www.go-fair.org/fair-principles/f3-metadata-clearly-explicitly-include-identifier-data-describe/)] and the metadata record [[F1](https://www.go-fair.org/fair-principles/f1-meta-data-assigned-globally-unique-persistent-identifiers/)]. LIDO implements a rich metadata model [[F2](https://www.go-fair.org/fair-principles/f2-data-described-rich-metadata/)] designed for indexing and retrieval [[F4](https://www.go-fair.org/fair-principles/f4-metadata-registered-indexed-searchable-resource/)].

**A – Accessible:** LIDO metadata is readily distributed via open protocols such as OAI-PMH and REST-based APIs [[A1](https://www.go-fair.org/fair-principles/metadata-retrievable-identifier-standardised-communication-protocol/)] and is harvested by aggregators [[A2](https://www.go-fair.org/fair-principles/a2-metadata-accessible-even-data-no-longer-available/)] at significant scale.

**I – Interoperable:** LIDO builds upon a well-established and extensively documented XML schema definition [[I1](https://www.go-fair.org/fair-principles/i1-metadata-use-formal-accessible-shared-broadly-applicable-language-knowledge-representation/)] that expressly supports the use of linked open vocabularies [[I2](https://www.go-fair.org/fair-principles/i2-metadata-use-vocabularies-follow-fair-principles/)].

**R – Reusable**: LIDO has provisions for detailed licensing information, for both the data resources and the metadata [[R1.1](https://www.go-fair.org/fair-principles/r1-1-metadata-released-clear-accessible-data-usage-license/)], for metadata provenance [[R1.2](https://www.go-fair.org/fair-principles/r1-2-metadata-associated-detailed-provenance/)], and for domain-specific metadata such as events occuring in the object lifecycle [[R1.3](https://www.go-fair.org/fair-principles/r1-3-metadata-meet-domain-relevant-community-standards/)].

### Semantic Web technologies

The notion of a [Semantic Web](https://www.w3.org/standards/semanticweb/) originated soon after the "classic" Web had gained acceptance. While ordinary Web pages with hyperlinks support little more than free-text search, a desire was felt to make assertions on the Web that can be acted upon by machines more precisely, for example, for compiling databases of factual statements derived from, and linked to, Web pages. Initially, metadata standards such as LIDO were not primarily designed as building blocks for the Semantic Web, but rather to co-exist with it.

The Semantic Web idea gave rise to a technology known as the [Resource Description Framework](https://www.w3.org/TR/rdf11-concepts/) (RDF). RDF is a data model that uses a sentence-like structure (subject–predicate–object) to express statements about things. For example, a painting (the *subject*) was created (the *predicate*) by a person (the *object*) where the person can become the subject for further statements, such as the person's date of birth, place of activity, working field, etc.

The LIDO schema beginning with version 1.1, while strictly rooted in XML technology, permits a limited number of RDF statements ([&lt;skos:Concept&gt;](https://www.w3.org/2009/08/skos-reference/skos.html#Concept), [&lt;owl:sameAs&gt;](https://www.w3.org/TR/owl-ref/#sameAs-def)) to be used as a bridge to Linked Open Data (LOD), and thus the Semantic Web. Moreover, the [LIDO Terminology](http://terminology-view.lido-schema.org) was designed from the ground up to comply with Semantic Web principles.

Initiatives for transforming LIDO data to RDF are underway. However, development of a fully RDF-based metadata schema for cultural heritage is still at an early stage. Examples are the proposed [RDF Ontology](http://vraweb.org/vra-core-rdf-ontology-available-for-review/) for the [Visual Resources Association Core](http://www.loc.gov/standards/vracore/) (VRA CORE) standard, and the data model proposed by the [Linked Art](https://linked.art/) community.

> Progress in this area can be followed by subscribing to the LIDO mailing list or by sending email to
the LIDO working group. For details, see <https://cidoc.mini.icom.museum/working-groups/lido/lido-community/>.

### Linked Data

Data published in accordance with Semantic Web principles is known as [Linked Data](https://www.w3.org/wiki/LinkedData). Most Linked Data of interest to the cultural heritage community is freely available as *Linked Open Data* (LOD).

LIDO strongly recommends the use of controlled vocabularies wherever an index element for a concept can occur, and of shared authority files where a reference to a named entity, such as a person, an organization, or a place is expected. Very often the relevant concept or entity is described in one of the major LOD sources such as the [Art & Architecture Thesaurus](https://www.getty.edu/research/tools/vocabularies/aat/index.html) (AAT), the German [Integrated Authority File](https://www.dnb.de/DE/Home/home_node.html) (GND), or in an aggregated Linked Data source such as [VIAF](https://viaf.org/) or [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page). Last, but not least, the [LIDO Terminology](http://terminology-view.lido-schema.org), recommended for many of the type attributes in LIDO, is also available as a LOD source.

In order to qualify as Linked Data, a data source must use the RDF data model. Each item must be uniquely and persistently identified by a URI, and the URI must be dereferenceable as a Web resource. The latter means that machines or humans can invoke the URI as a network address and obtain the corresponding resource in the desired RDF syntax, or in a human-readable rendering. A useful add-on to many LOD sources is the option to obtain arbitrary selections or aggregates of the available data by sending queries in the [SPARQL Query Language for RDF](https://www.w3.org/TR/rdf-sparql-query/).

The LIDO Terminology meets all of these requirements and can therefore claim to be a fully qualified LOD source. LIDO itself can only partly comply with Linked Data requirements in that LIDO records can be published under persistent URIs. Extracting single statements from a LIDO record requires querying the XML syntax tree using a foundational technology that is different from what is used for Linked Data.

In summary, it can be stated that LIDO supports publishing data as Linked Open Data for the following reasons:

- LIDO supports the use of LOD-enabled controlled vocabularies and authority files.
- The LIDO Terminology is available as LOD vocabularies, where each LIDO Term is a resolvable RDF resource, identified by a persistent Uniform Resource Identifier (URI). For instance, the URI for the Event Type "Production" is <http://terminology.lido-schema.org/lido00007>.
- The element and attribute names declared in the LIDO schema could be made into RDF class and property names in a future RDF rendering of LIDO records.

## Enabling adaptability

Adaptability means decreasing or increasing the expressivity, and thus the complexity, of the data model. Doing so requires precautions to ensure that all resulting LIDO records are still valid with respect to the current LIDO XML schema.

### Restriction and simplification

The LIDO XML schema itself permits a considerable degree of freedom for choosing the amount of detail that goes into a data record. In some cases it may be useful, or even required, to suppress the usage of elements or attributes explicitly so that processing systems do not have to be prepared to process them.

Restrictions may be declared in ancillary files, e.g., in the form of external Schematron rules (see chapter [*6.2 Schematron*](#schematron)), transformation scripts, or any other technology suitable for checking or restricting the content of XML records. Evidently, restrictions also must not lead to the disappearance of mandatory LIDO elements. The recommended way to implement restrictions, however, is the creation of an application profile (s. below).

A different case for simplification can occur if the content of the LIDO record is to be rendered in a less expressive data model. For example, some applications of the OAI-PMH harvesting protocol require that whatever schema is used for the original data, a simplified representation using Dublin Core must be provided alongside the original record. This requires transforming the LIDO record according to a mapping scheme, in the case of Dublin Core somewhat flippantly referred to as "dumbing down".

### Extension

Different collections of cultural and natural objects may have specific description requirements not readily available in LIDO. While simplification can be achieved without touching the LIDO XML schema, adding expressivity to the schema may sometimes require more complex solutions.

A frequent case is that a concept is either missing from a recommended vocabulary, or considered too broad for the intended purpose. Where a more suitable vocabulary is available as Linked Data, this may safely be used within the &lt;skos:Concept&gt; element without compromising the schema validity of the LIDO record.

### Application profiles

Application profiles are a way of economizing the use of broadly scoped standards. They allow users to specify rules and recommendations for specific use cases, define rules to improve data editing and quality, or solve possible ambiguities of the relevant standard. A LIDO application profile might include element restrictions or recommend or mandate specific vocabularies, and more.

An application profile is always a sub-standard of its parent. It may specify an element's semantics compared to generic LIDO, but never change its semantics. Further, mandatory elements and attributes must not be declared optional. This ensures that a LIDO record conforming to an application profile is still a valid generic LIDO record.

#### What Constitutes an Application Profile?

The rules of an application profile can either be expressed in a textual document (such as a print publication, HTML document, etc.) or a set of machine-actionable rules (XML Schema and/or Schematron rules). Ideally, an application profile comprises both, providing textual documentation of the rules and background of an application profile, as well as a machine-actionable file to automatically check records for conformance.

The LIDO Application Profile Workflow is the recommended way of creating application profiles. It provides an easy way to document differences between the application profile and generic LIDO, as well as to produce documentation and machine-actionable rules for the profile. You can find the workflow as well as more extensive documentation in the relevant [GitLab Repository](https://gitlab.gwdg.de/lido/profiles).

#### How to Use the Application Profile Workflow

Use of the application profile worklflow requires basic knowledge of XML and XML Schema Definition. Some familiarity with Schematron, XPath, and [TEI-Lite](https://tei-c.org/guidelines/customization/lite/) is useful, though not necessarily required.

![Figure 2: Schematic presentation of the LIDO Application Profile Workflow](img/lido_profile_workflow.svg)

The profile workflow is built around the LIDO Profile Definition XML. This file contains all the information users need
to provide for the process to produce a valid LIDO application profile: profile metadata, profile documentation, and the
profile rules. This way, the Profile Definition serves as a single source for both machine-actionable rules as well as
HTML documentation. Each of the output files is produced by a single XSL transformation of the Profile Definition.

Machine-actionable rules can be declared as XML Schema rules, as well as Schematron rules.

Profile documentation in the Profile Definition follows TEI-Lite and is the primary source for HTML documentation. For
ease of use, the workflow can also produce simple documentation of individual rules and Schematron constraints if desired.

The repository contains an empty Profile Definition template to facilitate the creation of new Profile Definitions by
users, as well as further resources to aid the creation of application profiles.

For more information on LIDO profiles, see section [*4.2.4 Application Profile*](#application-profile), the [GitLab Repository](https://gitlab.gwdg.de/lido/profiles) hosting the workflow, and <https://cidoc.mini.icom.museum/working-groups/lido/lido-overview/profiles/>.
