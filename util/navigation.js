
function createObserver(links) {
    const options = {
      rootMargin: "0px 0px -200px",
      threshold: 1
    }
    const callback = (e, o) => { handleObserver(e, o, links) }
    return new IntersectionObserver(callback, options)
  }

function handleObserver(entries, observer, links) {
    entries.forEach((entry)=> {
      const { target, isIntersecting, intersectionRatio } = entry
      if (isIntersecting && intersectionRatio >= 1) {
        const visibleId = `#${target.id}`
        updateLinks(visibleId, links)
      }
    })
  }

function updateLinks(visibleId, links) {
  links.map(link => {
    let href = link.getAttribute('href')
    link.classList.remove('is-active')
    if(href === visibleId) link.classList.add('is-active')
  })
}


function init() {
  const nav = document.querySelector('nav');
  const headings = [...document.querySelector('article').querySelectorAll('h1, h2, h3')];
  headings.shift();

  const links = [...nav.querySelectorAll('a')]
  const observer = createObserver(links)
  headings.map(heading => observer.observe(heading))
}

init();
