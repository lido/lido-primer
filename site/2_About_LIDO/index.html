<!DOCTYPE html>
<html lang="en">
<head>
  
  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta name="author" content="LIDO Terminology Working Group">
    
    <link rel="shortcut icon" href="../img/favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>2 About LIDO - LIDO Primer</title>
    <link href="../css/bootstrap-3.3.7.min.css" rel="stylesheet">
    <link href="../css/font-awesome-4.7.0.css" rel="stylesheet">
    <link href="../css/base.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/highlight.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/bootstrap-3.3.7.min.js"></script>
    <script src="../js/highlight.pack.js"></script>
    
    <base target="_top">
    <script>
      var base_url = '..';
      var is_top_frame = false;
        
        var pageToc = [
          {title: "About LIDO", url: "#_top", children: [
              {title: "2.1 What is LIDO?", url: "#21-what-is-lido" },
              {title: "2.2 Why LIDO?", url: "#22-why-lido" },
              {title: "2.3 Background of LIDO", url: "#23-background-of-lido" },
              {title: "2.4 Related standards", url: "#24-related-standards" },
          ]},
        ];

    </script>
    <script src="../js/base.js"></script> 
</head>

<body>
<script>
if (is_top_frame) { $('body').addClass('wm-top-page'); }
</script>



<div class="container-fluid wm-page-content">
  <a name="_top"></a>
    

    
    
      
    

  <div class="row wm-article-nav-buttons" role="navigation" aria-label="navigation">
    
    <div class="wm-article-nav pull-right">
      <a href="../3_Design_principles/" class="btn btn-xs btn-default pull-right">
        Next
        <i class="fa fa-chevron-right" aria-hidden="true"></i>
      </a>
      <a href="../3_Design_principles/" class="btn btn-xs btn-link">
        3 Design principles
      </a>
    </div>
    
    <div class="wm-article-nav">
      <a href="../1_Introduction/" class="btn btn-xs btn-default pull-left">
        <i class="fa fa-chevron-left" aria-hidden="true"></i>
        Previous</a><a href="../1_Introduction/" class="btn btn-xs btn-link">
        1 Introduction
      </a>
    </div>
    
  </div>

    

    <h1 id="about-lido">About LIDO<a class="headerlink" href="#about-lido" title="Permanent link">#</a></h1>
<h2 id="21-what-is-lido">2.1 What is LIDO?<a class="headerlink" href="#21-what-is-lido" title="Permanent link">#</a></h2>
<p>LIDO is a <strong>metadata schema</strong>, formally defined in the <strong>XML schema</strong> language. It is intended to be used for a unified representation of heterogenous objects in a variety of online services, from a local collections database to large aggregation portals. In promoting linked data mechanisms, it enables exposing, sharing, connecting, and discovering data on the web in multilingual environments. Its strength lies in its ability to support the typical scope of descriptive information about objects of material culture, ranging from natural objects to fine art works, including items from the arts, archaeology, technology, life sciences  and other domains.</p>
<p>A single object or work, or a group of objects, is described in a <strong>LIDO record</strong> using XML syntax. The representation of the LIDO Schema in LIDO XML documents is referred to as the <strong>LIDO format</strong>.
Initially designed as a harvesting standard, LIDO still serves this function in cultural heritage institutions where metadata is made available through the OAI Protocol for Metadata Harvesting (OAI-PMH). Harvesting refers to the process of collecting and updating metadata by aggregation portals.</p>
<p>Next to descriptive metadata, a LIDO record contains administrative metadata that may include links to resource representations, in which case an aggregation portal can guide the user to media items (such as facsimiles, images, audio, video, or 3D models) representing the collection object described in the metadata record. While such media items may be the actual goal of interacting with a portal, the medadata record is essential for making them findable, comprehensibe and to aid in their discovery</p>
<h2 id="22-why-lido">2.2 Why LIDO?<a class="headerlink" href="#22-why-lido" title="Permanent link">#</a></h2>
<p>LIDO is designed to allow for object descriptions at different levels of granularity or specificity. Few elements are considered mandatory in order to facilitate the recording of objects where detailed information is not available, while a great number of elements enables a fine-grained account of an object, supporting, for instance, a deeper exploration of data, possibly encouraging and facilitating research questions and the exploitation of research data. By improving interoperability through the support of Linked Data principles and by providing data via interfaces or APIs, resources and research data become more visible and accessible.</p>
<p>The LIDO XML Schema is designed to</p>
<ul>
<li>allow the description of cultural heritage objects or works, including non-art and natural objects, to the level requested;</li>
<li>allow an organization to decide how extensive or how simple the metadata sets they provide to a particular reuse context are , i.e., they can vary in different scenarios;</li>
<li>enable the provision of metadata about digital surrogates representing the object in focus;</li>
<li>provide links back to records in their "home" context, i.e., on an organization's website;</li>
<li>enable the display of detailed information about an object in a reader-friendly form, while the record content is equally well prepared for retrieval;</li>
<li>support the identification of a provided entity, such as an actor, an event, a place, or a concept, by enabling the reference of authority files and controlled vocabularies;</li>
<li>provide metadata for reading and searching information by defining display and index elements;</li>
<li>provide full support for multilingual records, either at the structural element level or at the individual term or appellation element level, or both;</li>
<li>distinguish between the identifiers of the documented object, the web page containing a description of the object, and its online digital surrogates.</li>
</ul>
<h2 id="23-background-of-lido">2.3 Background of LIDO<a class="headerlink" href="#23-background-of-lido" title="Permanent link">#</a></h2>
<p>LIDO takes account of standards and guidelines that have already been established for the description of cultural objects and the exchange of data. Particular emphasis is placed on standards that promote data integration from different cultural domains. In detail LIDO is the result of a joint effort of the CDWA Lite, CIDOC CRM, and Spectrum communities. The LIDO Schema integrates and extends <a href="https://www.getty.edu/research/publications/electronic_publications/cdwa/cdwalite.pdf">CDWA Lite</a>, is underpinned by the <a href="https://www.cidoc-crm.org/">CIDOC Conceptual Reference Model</a> (CIDOC CRM), and considers some information units from <a href="https://collectionstrust.org.uk/spectrum/spectrum-5/">Spectrum</a> definitions.</p>
<h3 id="categories-for-the-description-of-works-of-art-cdwa-lite">Categories for the Description of Works of Art (CDWA Lite)<a class="headerlink" href="#categories-for-the-description-of-works-of-art-cdwa-lite" title="Permanent link">#</a></h3>
<p>The LIDO Schema is based on <a href="https://www.getty.edu/research/publications/electronic_publications/cdwa/cdwalite.pdf">CDWA Lite</a>, an XML schema for encoding core records for works of art and material culture. CDWA Lite is derived from the <a href="https://www.getty.edu/research/publications/electronic_publications/cdwa/">Categories for the Description of Works of Art (CDWA)</a>, following the data content standard <a href="https://vraweb.org/resourcesx/cataloging-cultural-objects/">Cataloging Cultural Objects (CCO)</a>, both provided by the J. Paul Getty Trust and ARTstor. It is intended as a low-barrier way to enable institutions to contribute their collections information to union catalogs using the Open Archives Initiatives Protocol for Metadata Harvesting (OAI/PMH).  The schema defines 22 categories, i.e., elements, ... of which are considered core. CDWA Lite is integrated in and replaced by LIDO since ... </p>
<p>The most important extension to CDWA Lite is the introduction of an Event element, allowing the description of the object history and associated entities, in order to capture the event-centric model of  CIDOCCRM.
CDWA Lite is replaced since … by LIDO Version 1.0.</p>
<p>For more information on CDWA Lite see the <a href="https://www.getty.edu/research/publications/electronic_publications/cdwa/cdwalite.html">publication</a> of the Getty Research Institute.  </p>
<h3 id="cidoc-conceptual-reference-model-cidoc-crm">CIDOC Conceptual Reference Model (CIDOC CRM)<a class="headerlink" href="#cidoc-conceptual-reference-model-cidoc-crm" title="Permanent link">#</a></h3>
<p>The CIDOC Conceptual Reference Model is a formal ontology accepted as ISO standard 21127 since ... CIDOC CRM provides definitions and a formal structure for describing the implicit and explicit concepts and relationships used in cultural heritage documentation. It is intended to be a common language for domain experts and implementers to formulate requirements for information systems and to serve as a guide for good practice of conceptual modeling. Work is carried out through the CIDOC-CRM Special Interest Group. 
For more information on CIDOC CRM see the <a href="http://www.cidoc-crm.org/">homepage</a>.</p>
<h3 id="spectrum">Spectrum<a class="headerlink" href="#spectrum" title="Permanent link">#</a></h3>
<p>Spectrum is a collections management standard provided by the UK Collections Trust charity. It primarily comprises the definitions of workflows, known as „procedures“ in Spectrum, commonly used in museum documentation. It also defines rules to be applied per workflow and minimum requirements and information units for documentation in collection management systems. In LIDO, some of the information units defined in Spectrum have been considered. There is also a mapping available from Spectrum 5.0 to LIDO 1.0. 
For more information on Spectrum see: https://collectionstrust.org.uk/spectrum/ defines 21 procedures https://collectionstrust.org.uk/spectrum/spectrum-5/</p>
<h2 id="24-related-standards">2.4 Related standards<a class="headerlink" href="#24-related-standards" title="Permanent link">#</a></h2>
<p>LIDO builds upon several standards, specifications and guidelines. A structured view of relevant background information could distinguish between the following:</p>
<ul>
<li>
<p>Formal models as reference frameworks for data integration, notably the <a href="http://www.cidoc-crm.org/">CIDOC Conceptual Reference Model</a> (CIDOC CRM), the <a href="https://www.ifla.org/publications/node/11412">IFLA Library Reference Model</a> (IFLA LRM), and <a href="https://repository.ifla.org/handle/123456789/659">FRBRoo</a>, a conceptual model for bibliographic information in object-oriented formalism.</p>
</li>
<li>
<p>Rules for data content include cataloging guidelines, specifically <a href="http://www.getty.edu/research/publications/electronic_publications/cdwa/index.html">Categories for the Description of Works of Art</a> (CDWA) and <a href="https://vraweb.org/resourcesx/cataloging-cultural-objects/">Cataloging Cultural Objects</a>: A Guide to Describing Cultural Works and Their Images (CCO).</p>
</li>
<li>
<p>Controlled vocabularies for data values, such as authority files like the <a href="https://www.getty.edu/research/tools/vocabularies/ulan/">Union List of Artist Names</a> (ULAN) or thesauri like the <a href="http://www.getty.edu/research/tools/vocabularies/aat/">Art &amp; Architecture Thesaurus</a> (AAT).</p>
</li>
</ul>
<p><img alt="Related standards" src="../img/Impacts_on_LIDO_1.png" /></p>
<p><em>Figure 2: Related standards</em></p>
<p>For more related standards, see the list of <a href="https://cidoc.mini.icom.museum/standards/cidoc-standards-guidelines/">Standards and guidelines</a> provided by the CIDOC community, and the overview in <a href="https://www.getty.edu/research/publications/electronic_publications/cdwa/moreinfo.html">CDWA and Other Metadata Standards</a> as well as the <a href="https://www.getty.edu/research/publications/electronic_publications/intrometadata/crosswalks.html">Metadata Standards Crosswalk</a>, both maintained by the Getty Vocabulary Program. </p>

  <br>
    

    
    
      
    

  <div class="row wm-article-nav-buttons" role="navigation" aria-label="navigation">
    
    <div class="wm-article-nav pull-right">
      <a href="../3_Design_principles/" class="btn btn-xs btn-default pull-right">
        Next
        <i class="fa fa-chevron-right" aria-hidden="true"></i>
      </a>
      <a href="../3_Design_principles/" class="btn btn-xs btn-link">
        3 Design principles
      </a>
    </div>
    
    <div class="wm-article-nav">
      <a href="../1_Introduction/" class="btn btn-xs btn-default pull-left">
        <i class="fa fa-chevron-left" aria-hidden="true"></i>
        Previous</a><a href="../1_Introduction/" class="btn btn-xs btn-link">
        1 Introduction
      </a>
    </div>
    
  </div>

    <br>
</div>

<footer class="container-fluid wm-page-content">
  
  <p>Built with <a href="https://www.mkdocs.org/">MkDocs</a> using <a href="https://github.com/gristlabs/mkdocs-windmill">Windmill</a>.</p>
</footer>

</body>
</html>