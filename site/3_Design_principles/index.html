<!DOCTYPE html>
<html lang="en">
<head>
  
  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta name="author" content="LIDO Terminology Working Group">
    
    <link rel="shortcut icon" href="../img/favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>3 Design principles - LIDO Primer</title>
    <link href="../css/bootstrap-3.3.7.min.css" rel="stylesheet">
    <link href="../css/font-awesome-4.7.0.css" rel="stylesheet">
    <link href="../css/base.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/highlight.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/bootstrap-3.3.7.min.js"></script>
    <script src="../js/highlight.pack.js"></script>
    
    <base target="_top">
    <script>
      var base_url = '..';
      var is_top_frame = false;
        
        var pageToc = [
          {title: "Design principles", url: "#_top", children: [
              {title: "3.1 Choice of technologies", url: "#31-choice-of-technologies" },
              {title: "3.2 Supporting Web principles", url: "#32-supporting-web-principles" },
              {title: "3.3 Enabling adaptability", url: "#33-enabling-adaptability" },
          ]},
        ];

    </script>
    <script src="../js/base.js"></script> 
</head>

<body>
<script>
if (is_top_frame) { $('body').addClass('wm-top-page'); }
</script>



<div class="container-fluid wm-page-content">
  <a name="_top"></a>
    

    
    
      
    

  <div class="row wm-article-nav-buttons" role="navigation" aria-label="navigation">
    
    <div class="wm-article-nav pull-right">
      <a href="../4_Basic_structure_of_LIDO/" class="btn btn-xs btn-default pull-right">
        Next
        <i class="fa fa-chevron-right" aria-hidden="true"></i>
      </a>
      <a href="../4_Basic_structure_of_LIDO/" class="btn btn-xs btn-link">
        4 Basic structure of LIDO
      </a>
    </div>
    
    <div class="wm-article-nav">
      <a href="../2_About_LIDO/" class="btn btn-xs btn-default pull-left">
        <i class="fa fa-chevron-left" aria-hidden="true"></i>
        Previous</a><a href="../2_About_LIDO/" class="btn btn-xs btn-link">
        2 About LIDO
      </a>
    </div>
    
  </div>

    

    <h1 id="design-principles">Design principles<a class="headerlink" href="#design-principles" title="Permanent link">#</a></h1>
<p>The design of LIDO was guided by the following considerations:</p>
<ul>
<li>
<p><strong><em>Re-use existing standards</em></strong>
Pre-existing models for representing cultural heritage metadata contain assumptions that are still valid today. These should be preserved and re-used in the design of LIDO. For an overview of pre-existing models, see <a href="">2.3 Background of LIDO</a>.</p>
</li>
<li>
<p><strong><em>Use proven technologies</em></strong>
Metadata needs to be processed in a wide range of system enviroments. Choosing proprietary or poorly supported technologies can result in barriers and unneccessary costs. For a brief summary of the XML family of technologies adopted for LIDO, see <a href="">3.1 Choice of technologies</a>.</p>
</li>
<li>
<p><strong><em>Facilitate interoperability</em></strong>
Metadata are moved, transformed, distributed, and interconnected at an increasing rate. LIDO should therefore facilitate and encourage higher levels of harmonization than in the past. Next to technical and organizational considerations described in <a href="">3.2 Supporting Web principles</a>, mappings between LIDO and standards and models from other domains are essential tools for enabling and maintaining such harmonization.</p>
</li>
<li>
<p><strong><em>Enable flexibility</em></strong> 
The information landscape is changing continuously, bringing up new requirements that need to be addressed without invalidating existing data records. Not all future requirements can be anticipated. Well-defined extension points should be provided as parts of the model. Moreover, cultural heritage metadata will continue to be produced in varying degrees of detail and granularity. The model should therefore be amenable to object descriptions of widely different richness by allowing for the introductuon of application-specific restrictions and extensions as described in <a href="">3.3 Enabling flexibility</a>.</p>
</li>
</ul>
<h2 id="31-choice-of-technologies">3.1 Choice of technologies<a class="headerlink" href="#31-choice-of-technologies" title="Permanent link">#</a></h2>
<h3 id="311-a-rationale-for-choosing-xml">3.1.1 A rationale for choosing XML<a class="headerlink" href="#311-a-rationale-for-choosing-xml" title="Permanent link">#</a></h3>
<p>The <a href="https://www.w3.org/TR/xml/">Exensible Markup Language (XML)</a> has evolved from a mere markup language for structuring machine-processable text into a major technological ecosystem with a wide range of auxiliary specifications and tools. This long-term evolution justifies the assumption that XML will not become obsolete in the foreseeable future. Nevetheless, it is fair to say that JSON (initially designed as the Javascript Object Language) is emerging as a contender in many areas hitherto dominated by XML. Moreover, the Linked Data paradigm, briefly described in 3.2 Facilitating interoperability, presents a challenge due to the core differences between the XML and RDF data models. Version 1.1 of the LIDO schema is an attempt at reconciling some of these differences.</p>
<p>Given the maturity of XML and the broad support in the information processing sphere, it is reasonable to expect that this technology is, and will remain, a solid and reliable platform for the LIDO metadata schema.</p>
<h3 id="312-an-xml-schema-for-lido">3.1.2 An XML schema for LIDO<a class="headerlink" href="#312-an-xml-schema-for-lido" title="Permanent link">#</a></h3>
<p>XML itself defines the format of markup elements (often referred to as tags) and how these can be nested to form a tree-like structure. Any XML document that obeys the basic element syntax and has matching start and end tags is said to be well-formed.</p>
<p>In order to allow for predictable processing of the document content, the order and occurrence of markup elements is typically restricted by a schema (sometimes referred to as the document grammar). At least three languages (known as DTD, RelaxNG, and XSD) are available for writing XML schema definitions. The LIDO Schema is written in the <a href="https://www.w3.org/TR/xmlschema-1/">XML Schema Definition (XSD)</a> language, which can be considered the most expressive of these three.</p>
<p>The core specification for LIDO is laid down in an XSD document. This schema document can be used by software tools known as validating parsers in order to verify if a LIDO record complies with all rules and restrictions given in the schema. Such validation is essential wherever the LIDO record is to be processed for usage in databases, web portals, or in transformation chains as used in large-scale aggregation portals.</p>
<h3 id="313-schema-design">3.1.3 Schema design<a class="headerlink" href="#313-schema-design" title="Permanent link">#</a></h3>
<p>The XSD language provides some mechanisms for <em>object-oriented</em> design. This means that schema constructs can be defined on an abstract level, acting as types or templates for actual element or attribute definitions. The LIDO schema definition makes extensive use of type declarations for elements composed of more than one element, or of simple datatypes with attributes. In the XSD language, these type decalations are referred to as <em>complex types</em>. In fact, the entire LIDO record structure is first modelled on the abstract level of complex types, before declaring the derived elements that are actually used in a LIDO record. This design principle offers a clear distinction between the model and its implementation. </p>
<h3 id="314-namespaces">3.1.4 Namespaces<a class="headerlink" href="#314-namespaces" title="Permanent link">#</a></h3>
<p>As explained in <a href="">2.3 Background of LIDO</a>, re-use of existing specifications is an important objective in the design of the LIDO Schema. XML has mechanisms for combining definitions from more than one schema. This allows a schema definition to "borrow" element or attribute definitions from other schemas, typically using a mechanism known as <em>namespaces</em>.</p>
<p>Declaring a namespace for LIDO allows to distinguish between elements or attributes that are part of the LIDO data model, and others that are defined elsewhere. Namespaces determine the scope and origin of an element or attribute name. In other words, a namespace indicates who is responsible for the definition of a particular element or attribute name and where XML processing software can find the corresponding schema. Namespaces also allow to distinguish between elements or attributes that carry identical names.  As an example, the meaning of <em>rdf:type</em> is different from that of <em>lido:type</em>. </p>
<p>A combination of namespace and name is referred to as <em>qualified name</em> (a QName in XML terminology). QNames can be written either in full or in an abbreviated form:</p>
<pre><code>&lt;http://www.lido-schema.org/titleSet&gt; is identical to &lt;lido:titleSet xmns:lido="http://www.lido-schema.org"&gt;
</code></pre>
<p>In practice, the abbreviated namespace (in this case, "lido") is only declared once in the outermost element of an XML document. This allows all subsequent elements and attributes from the LIDO namespace to be written with what is known as a <em>namespace prefix</em>, followed by a colon:</p>
<pre><code>&lt;lido:lidoWrap  xmns:lido="http://www.lido-schema.org" (...)  &gt;
   (...)
     &lt;lido:titleSet&gt;
   (...)
</code></pre>
<p>LIDO XML records prepared using the Schema Version 1.1 can (and should) include the following prefix declarations in their outermost element:</p>
<table>
<thead>
<tr>
<th>Namespace URI</th>
<th>Prefix</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td><a href="http://www.lido-schema.org">http://www.lido-schema.org</a></td>
<td>lido</td>
<td>The LIDO XML schema. A particular version can be selected with the <em>xsi:schemaLocation attribute</em>.</td>
</tr>
<tr>
<td>http://www.opengis.net/gml</td>
<td>gml</td>
<td>The Geography Markup Language (GML) schema, used for location coordinates.</td>
</tr>
<tr>
<td>http://www.w3.org/2002/07/owl#</td>
<td>owl</td>
<td>The Web Ontology Language (OWL) namespace, used for the <em>sameAs</em> identity statement.</td>
</tr>
<tr>
<td>http://www.w3.org/1999/02/22-rdf-syntax-ns#</td>
<td>rdf</td>
<td>The RDF namespace, used within elements from the SKOS namespace.</td>
</tr>
<tr>
<td>http://www.w3.org/2004/02/skos/core#</td>
<td>skos</td>
<td>The SKOS namespace, used for statements about linked vocabulary data.</td>
</tr>
<tr>
<td>http://www.w3.org/XML/1998/namespace</td>
<td>xml</td>
<td>The XML namespace, used for the language (<em>xml:lang</em>) attribute.</td>
</tr>
<tr>
<td>http://www.w3.org/2001/XMLSchema-instance</td>
<td>xsi</td>
<td>An XML utility namespace, used for retrieving schema declarations in cases where the namespace URI does not resolve to the desired schema version.</td>
</tr>
</tbody>
</table>
<h2 id="32-supporting-web-principles">3.2 Supporting Web principles<a class="headerlink" href="#32-supporting-web-principles" title="Permanent link">#</a></h2>
<p>The Web as a platform for cultural heritage information has seen a number of changes since the introduction of LIDO Schema Version 1.0. Arguably the most significant developments are concerned with openness and interconnection of published knowledge. The currrent version of LIDO (1.1) introduces new constructs primarily focussing on requirements from two areas: the <a href="https://www.go-fair.org/">FAIR Princples</a> and the <a href="https://www.w3.org/TR/ldp-bp/">Linked Data Platform Guidelines</a>.</p>
<h3 id="321-fair-principles">3.2.1 FAIR Principles<a class="headerlink" href="#321-fair-principles" title="Permanent link">#</a></h3>
<p>This is a relatively recent initiative from the research data community. It has defined four main principles that should guide the preparation of data and metadata. The FAIR Principles also serve as a checklist for funding agencies. LIDO v1.1 is well suited to play a key role in implementing FAIR principles in the domain of cultural heritage metadata. In particular:</p>
<p><strong>F – Findable:</strong> LIDO mandates the use of publishable identifiers both for the object (the data) [<a href="https://www.go-fair.org/fair-principles/f3-metadata-clearly-explicitly-include-identifier-data-describe/">F3</a>] and the metadata record [<a href="https://www.go-fair.org/fair-principles/f1-meta-data-assigned-globally-unique-persistent-identifiers/">F1</a>]. LIDO implements a rich metadata model [<a href="https://www.go-fair.org/fair-principles/f2-data-described-rich-metadata/">F2</a>] designed for indexing and retrieval [<a href="https://www.go-fair.org/fair-principles/f4-metadata-registered-indexed-searchable-resource/">F4</a>].</p>
<p><strong>A – Accessible:</strong> LIDO metadata is readily distributed via open protocols such as OAI-PMH and REST-based APIs [<a href="https://www.go-fair.org/fair-principles/metadata-retrievable-identifier-standardised-communication-protocol/">A1</a>] and is harvested by aggregators [<a href="https://www.go-fair.org/fair-principles/a2-metadata-accessible-even-data-no-longer-available/">A2</a>] at significant scale.</p>
<p><strong>I – Interoperable:</strong> LIDO builds upon a well-established and extensively documented XML schema definition [<a href="https://www.go-fair.org/fair-principles/i1-metadata-use-formal-accessible-shared-broadly-applicable-language-knowledge-representation/">I1</a>] that expressly supports the use of linked open vocabularies [<a href="https://www.go-fair.org/fair-principles/i2-metadata-use-vocabularies-follow-fair-principles/">I2</a>].</p>
<p><strong>R – Reusable</strong>: LIDO has provisions for detailed licensing information, for both the data resources and the metadata [<a href="https://www.go-fair.org/fair-principles/r1-1-metadata-released-clear-accessible-data-usage-license/">R1.1</a>], for metadata provenance [<a href="https://www.go-fair.org/fair-principles/r1-2-metadata-associated-detailed-provenance/">R1.2</a>], and for community-specific metadata such as events occuring in the object lifecycle [<a href="https://www.go-fair.org/fair-principles/r1-3-metadata-meet-domain-relevant-community-standards/">R1.3</a>]. </p>
<h3 id="322-semantic-web-technologies">3.2.2 Semantic Web technologies<a class="headerlink" href="#322-semantic-web-technologies" title="Permanent link">#</a></h3>
<p>The notion of a Semantic Web originated soon after the "classic" Web had gained acceptance. While ordinary Web pages with hyperlinks support little more than free-text search, a desire was felt to make assertions on the Web that can be acted upon by machines more precisely, for example, for compiling databases of factual statements derived from, and linked to, Web pages. Initially, metadata standards such as LIDO were not primarily designed as building blocks for the Semanic Web, but rather to co-exist with it.</p>
<p>The Semantic Web idea gave rise to a technology known as the <a href="https://www.w3.org/TR/rdf-schema/">Resource Desciption Framework</a> (RDF). RDF is a data model that uses a sentence-like structure (subject–predicate–object) to express statements about things. For example, a painting (the subject) was created (the predicate) by a person (the object) where the person can become the subject for further statements, such as the person's date of birth, place of activity, working field, etc.</p>
<p>LIDO Version 1.1, while strictly rooted in XML technology, permits a limited number of RDF statements to be used as a bridge to Linked Open Data (LOD), and thus the Semantic Web. Moreover, the LIDO Terminology was designed from the ground up to comply with Semantic Web principles.</p>
<p>Initiatives for transforming LIDO data to RDF are underway. However, development of a fully RDF-based metadata schema for cultural heritage is still at an early stage. Examples are the proposed <a href="http://vraweb.org/vra-core-rdf-ontology-available-for-review/">RDF Ontology</a> for the VRA Core standard, and the data model proposed by the <a href="https://linked.art/">Linked Art</a> community.</p>
<p>Progress in this area can be followed by subscribing to the LIDO mailing list at https://www.lists.uni-marburg.de/lists/sympa/info/lido-wg or by sending email to lido-feedback@sub.uni-goettingen.de.</p>
<h3 id="323-linked-data">3.2.3 Linked Data<a class="headerlink" href="#323-linked-data" title="Permanent link">#</a></h3>
<p>Data published in accordance with Semantic Web principles is known as <em>Linked Data</em>. Most Linked Data of interest to the cultural heritage community is freely available as <em>Linked Open Data</em> (LOD).</p>
<p>LIDO strongly suggests the use of controlled vocabularies wherever an index element for a concept can occur, and of shared authority files where a reference to a person, organization, or geographic entity is expected. Very often the relevant concept or entity is described in one of the major LOD sources such as the <a href="https://www.getty.edu/research/tools/vocabularies/aat/index.html">Art &amp; Architecture Thesaurus</a> (AAT), the German <a href="https://www.dnb.de/DE/Home/home_node.html">Integrated Authority File</a> (GND), or in an aggregated Linked Data source such as <a href="https://viaf.org/">VIAF</a> or <a href="https://www.wikidata.org/wiki/Wikidata:Main_Page">Wikidata</a>. Last, but not least, the <a href="">LIDO Terminology</a>, recommended for many of the type attributes in LIDO, is also available as a LOD source.</p>
<p>In order to qualify as Linked Data, a data source must use the RDF data model. Each item must be uniquely and persistently identified by a URI, and the URI must be actionable as a Web resource. The latter means that machines or humans can invoke the URI as a network address and obtain the corresponding resource in the desired RDF syntax, or in a human-readable rendering. A useful add-on to many LOD sources is the option to obtain arbitrary selections or aggregates of the available data by sending queries in the SPARQL RDF query language.</p>
<p>The LIDO Terminology meets all of these requirements and can thererefore claim to be a fully qualified LOD source. LIDO itself can only partly comply with Linked Data requirements in that LIDO records can be published under persistent URIs. Extracting single statements from a LIDO record requires querying the XML syntax tree using a foundational technology that is different from what is used for Linked Data.   </p>
<p>In summary, it can be stated that LIDO supports publishing data as Linked Open Data for the following reasons:</p>
<ul>
<li>LIDO supports the use of LOD-enabled controlled vocabularies and authority files.</li>
<li>The LIDO Terminology is available as a LOD vocabulary, i.e., each LIDO Term is available as a resolvable RDF resource, identified by a persistent Uniform Resource Identifiers (URI), e.g., the URI for the Event Type "Creation" is http://terminology.lido-schema.org/lido00012. All commonly used RDF serializations can be obtained by appending a filename suffix to the URI. For instance, http://terminology.lido-schema.org/lido00012.ttl will yield the concept data in Turtle RDF syntax.</li>
</ul>
<h2 id="33-enabling-adaptability">3.3 Enabling adaptability<a class="headerlink" href="#33-enabling-adaptability" title="Permanent link">#</a></h2>

  <br>
    

    
    
      
    

  <div class="row wm-article-nav-buttons" role="navigation" aria-label="navigation">
    
    <div class="wm-article-nav pull-right">
      <a href="../4_Basic_structure_of_LIDO/" class="btn btn-xs btn-default pull-right">
        Next
        <i class="fa fa-chevron-right" aria-hidden="true"></i>
      </a>
      <a href="../4_Basic_structure_of_LIDO/" class="btn btn-xs btn-link">
        4 Basic structure of LIDO
      </a>
    </div>
    
    <div class="wm-article-nav">
      <a href="../2_About_LIDO/" class="btn btn-xs btn-default pull-left">
        <i class="fa fa-chevron-left" aria-hidden="true"></i>
        Previous</a><a href="../2_About_LIDO/" class="btn btn-xs btn-link">
        2 About LIDO
      </a>
    </div>
    
  </div>

    <br>
</div>

<footer class="container-fluid wm-page-content">
  
  <p>Built with <a href="https://www.mkdocs.org/">MkDocs</a> using <a href="https://github.com/gristlabs/mkdocs-windmill">Windmill</a>.</p>
</footer>

</body>
</html>